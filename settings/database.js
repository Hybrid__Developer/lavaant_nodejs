"use strict";

const dbConfig = require("config").get("db");
const mongoose = require("mongoose");
const webServerConfig = require('config').get('webServer')
const appConfig = require('config').get('app')
global.mongo = mongoose
const configure = async logger => {
  const log = logger.start(`settings:database:configure`);

  try {
    await mongoose.connect(dbConfig.url + "/" + dbConfig.database, {
      useNewUrlParser: true
    });
    console.log(`mongoose default connection is open to ${dbConfig.url}`);
    await require("../models").configure();
    global.db = mongoose.models;
    console.log('db connected')
    process.setMaxListeners(0)
    var port = process.env.PORT || webServerConfig.port
    console.log('port:')
    console.log(`${appConfig.url}:${port}`)
    log.end();
  } catch (err) {
    log.error(`unable to create mongo connection to ${dbConfig.url}`);
    throw new Error(err.message);
    // log.error(err);
    log.end();
  }
};

exports.configure = configure;
