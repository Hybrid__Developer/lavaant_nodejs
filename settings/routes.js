"use strict";

const fs = require("fs");
const api = require("../api");
const specs = require("../specs");
const permit = require("../permit")
const path = require("path");
const validator = require("../validators");
const fileUpload = require('express-fileupload');
var multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, '../', 'assets/images'));
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname);
    }
});


var upload = multer({ storage: storage, limits: { fileSize: 1024 * 1024 * 50 } });

const configure = (app, logger) => {
    const log = logger.start("settings:routes:configure");

    app.get("/specs", function (req, res) {
        fs.readFile("./public/specs.html", function (err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                });
            }
            res.contentType("text/html");
            res.send(data);
        });
    });

    app.get("/api/specs", function (req, res) {
        res.contentType("application/json");
        res.send(specs.get());
    });

    app.use(fileUpload());

    // user routes 
    app.post(
        "/api/users/register",
        permit.context.builder,
        validator.users.create,
        api.users.create
    );

    app.get(
        "/api/users/getCompanyList",
        permit.context.requiresToken,
        api.users.getCompanyList
    );
    app.get(
        "/api/users/searchCompany",
        permit.context.requiresToken,
        api.users.searchCompanyList
    );
    app.put(
        "/api/users/update/companyAdderessList",
        permit.context.requiresToken,
        api.users.updateCompanyListAddress
    );
    app.get(
        "/api/users/getById/:id",
        permit.context.requiresToken,
        validator.users.getById,
        api.users.getById
    );

    app.get(
        "/api/users/list",
        permit.context.builder,
        // validator.users.get, 
        api.users.list
    );
    app.get(
        "/api/users/search",
        permit.context.requiresToken,
        api.users.search
    );
    app.put(
        "/api/users/update/:id",
        permit.context.requiresToken,
        validator.users.update,
        api.users.update
    );
    app.put(
        "/api/users/deviceToken/addOrRemove/:id",
        permit.context.requiresToken,
        api.users.addOrRemoveDeviceToken
    );
    app.post(
        "/api/users/login",
        permit.context.builder,
        validator.users.login,
        api.users.login
    );
    app.post(
        "/api/users/facebook/login",
        permit.context.builder,
        // validator.users.login,
        api.users.facebookLogin
    );
    app.post(
        "/api/users/resetPassword",
        permit.context.requiresToken,
        validator.users.resetPassword,
        api.users.resetPassword
    );

    app.post(
        "/api/users/addAddress",
        permit.context.builder,
        validator.users.addAddress,
        api.users.addAddress
    );

    app.get(
        "/api/users/addressList/:id",
        permit.context.builder,
        // validator.users.getById, 
        api.users.addressList
    );

    app.put(
        "/api/users/addressUpdate/:id",
        permit.context.builder,
        // validator.users.update, 
        api.users.addressUpdate
    );
    app.put(
        "/api/users/forgotPassword",
        permit.context.builder,
        // validator.users.update, 
        api.users.forgotPassword
    );
    app.put(
        "/api/users/otp/verify",
        permit.context.builder,
        // validator.users.update, 
        api.users.otpVerify
    );
    app.put(
        "/api/users/otp",
        permit.context.builder,
        // validator.users.update, 
        api.users.otp
    );
    app.put(
        "/api/users/status/:id",
        permit.context.requiresToken,
        // validator.users.update,
        api.users.status
    );

    // ---------product=========== 
    app.put(
        "/api/products/asignVendor/:id",
        permit.context.builder,
        // validator.users.update, 
        api.products.asignVendor
    );

    app.post(
        "/api/products/addProduct",
        permit.context.builder,
        // validator.users.create, 
        api.products.create
    );

    app.get(
        "/api/products/list",
        permit.context.builder,
        // validator.users.create, 
        api.products.productList
    );

    app.put(
        "/api/products/update/:id",
        permit.context.builder,
        // validator.users.update, 
        api.products.update
    );

    app.get(
        "/api/products/listByVendor",
        permit.context.builder,
        // validator.users.create, 
        api.products.productsByVendor
    );

    app.get(
        "/api/products/bySubCategories",
        permit.context.builder,
        // validator.users.create, 
        api.products.productsBySubCategories
    );

    app.get(
        "/api/products/search",
        permit.context.builder,
        // validator.users.create, 
        api.products.search
    );

    app.put(
        "/api/products/uploadImage/:id",
        permit.context.builder,
        // permit.context.requiresToken, 
        upload.single('image'),
        api.products.uploadProductImage
    );
    app.put(
        "/api/products/uploadPdf/:id",
        permit.context.builder,
        // permit.context.requiresToken, 
        upload.single('pdf'),
        api.products.uploadPdf
    );
    app.put(
        "/api/products/addQuantity/:id",
        permit.context.builder,
        api.products.addQuantity
    );
    app.delete(
        "/api/products/deleteProduct/:id",
        permit.context.requiresToken,
        api.products.deleteProduct
    );

    // ---------------order------------- 
    app.post(
        "/api/orders/placeOrder",
        permit.context.builder,
        api.orders.create
    );

    app.get(
        "/api/orders/list",
        permit.context.builder,
        // validator.users.create, 
        api.orders.orderList
    );

    app.get(
        "/api/orders/byOrderNo/:orderNo",
        permit.context.builder,
        // validator.users.getById, 
        api.orders.getByOrderNo
    );
    app.get(
        "/api/orders/getByStatus",
        permit.context.requiresToken,
        // validator.users.getById, 
        api.orders.getByStatus
    );
    app.put(
        "/api/orders/updateStatus/:id",
        permit.context.requiresToken,
        api.orders.updateStatus
    );
    //==============category================ 

    app.post(
        "/api/categories",
        permit.context.requiresToken,
        // validator.users.getById, 
        api.categories.create
    );
    app.put(
        "/api/categories/:id",
        permit.context.requiresToken,
        api.categories.update
    );
    app.get(
        "/api/categories",
        permit.context.requiresToken,
        // validator.users.getById, 
        api.categories.get
    );

    //==============tasks================ 

    app.post(
        "/api/tasks",
        permit.context.requiresToken,
        validator.tasks.create,
        api.tasks.create
    );
    app.put(
        "/api/tasks/updateStatus/:id",
        permit.context.requiresToken,
        api.tasks.updateStatus
    );
    app.get(
        "/api/tasks",
        permit.context.requiresToken,
        validator.tasks.get,
        api.tasks.get
    );
    app.put(
        "/api/tasks/update/:id",
        permit.context.requiresToken,
        api.tasks.update
    );
    app.delete(
        "/api/tasks/delete/:id",
        permit.context.requiresToken,
        api.tasks.deleteTask
    );
    app.get(
        "/api/tasks/list",
        permit.context.builder,
        api.tasks.taskList
    );

    //==============subcategory================ 

    app.post(
        "/api/subCategories",
        permit.context.requiresToken,
        validator.subCategories.create,
        api.subCategories.create
    );
    app.put(
        "/api/subCategories/:id",
        permit.context.requiresToken,
        api.subCategories.update
    );
    app.get(
        "/api/subCategories/getByCategory",
        permit.context.requiresToken,
        api.subCategories.getByCategory
    );

    //==============quotes================ 

    app.post(
        "/api/quotes",
        permit.context.requiresToken,
        api.quotes.create
    );
    app.get(
        "/api/quotes",
        permit.context.builder,
        api.quotes.get
    );

    //==============favourites================ 

    app.put(
        "/api/favourites/subCategories",
        permit.context.requiresToken,
        api.favourites.updateFavouriteSubCategories
    );

    app.get(
        "/api/favourites/subCategories",
        permit.context.requiresToken,
        api.favourites.getFavouriteSubCategories
    );

    app.put(
        "/api/favourites/stores",
        permit.context.requiresToken,
        api.favourites.updateFavouriteStoreList
    );
    app.get(
        "/api/favourites/stores",
        permit.context.requiresToken,
        api.favourites.getFavouriteStores
    );
    app.get(
        "/api/favourites",
        permit.context.requiresToken,
        api.favourites.get
    );

    //==============features================ 

    app.post(
        "/api/features",
        permit.context.requiresToken,
        api.features.create
    );
    app.put(
        "/api/features/:id",
        permit.context.requiresToken,
        api.features.update
    );
    app.get(
        "/api/features",
        permit.context.requiresToken,
        api.features.get
    )
    app.get(
        "/api/features/getById/:id",
        permit.context.builder,
        api.users.addressList
    );

    //==============plans================ 

    app.post(
        "/api/plans",
        permit.context.requiresToken,
        validator.plans.create,
        api.plans.create
    );
    app.put(
        "/api/plans/:id",
        permit.context.requiresToken,
        api.plans.update
    );
    app.get(
        "/api/plans",
        permit.context.requiresToken,
        api.plans.get
    )
    app.get(
        "/api/plans/getById/:id",
        permit.context.builder,
        api.plans.getById
    );
    app.put(
        "/api/plans/updateStatus/:id",
        permit.context.requiresToken,
        api.plans.updateStatus
    );
    app.post(
        "/api/plans/features/add",
        permit.context.requiresToken,
        api.plans.addFeature
    );
    app.put(
        "/api/plans/features/remove",
        permit.context.requiresToken,
        api.plans.removeFeature
    );

    //==============stores================ 

    app.post(
        "/api/stores",
        permit.context.requiresToken,
        validator.stores.create,
        api.stores.create
    );
    app.put(
        "/api/stores/update/:id",
        permit.context.requiresToken,
        validator.stores.update,
        api.stores.update
    );
    app.put(
        "/api/stores/updateStatus/:id",
        permit.context.requiresToken,
        api.stores.updateStatus
    );
    app.get(
        "/api/stores/list",
        permit.context.builder,
        api.stores.get
    )
    app.get(
        "/api/stores/getById/:id",
        permit.context.builder,
        validator.stores.getById,
        api.stores.getById
    );
    app.get(
        "/api/stores/search",
        permit.context.requiresToken,
        api.stores.search
    );
    //==============uploads================ 
    app.put(
        '/api/uploads/image/:uploadImageOf/:id',
        permit.context.requiresToken,
        api.uploads.imageUploader
    )


    app.put(
        '/api/uploads/pdf/:uploadPdfOf/:id',
        permit.context.requiresToken,
        api.uploads.pdfUploader
    )

    app.post(
        '/api/uploads/excelUploader',
        permit.context.requiresToken,
        api.uploads.excelUploader
    )
    app.post(
        '/api/uploads/productExcelUploader',
        permit.context.requiresToken,
        api.uploads.productExcelUploader
    )
    //================counts================== 
    // app.get( 
    //   "/api/counts/:count", 
    //   permit.context.requiresToken, 
    //   api.counts.count 
    // ); 
    app.get(
        "/api/counts/monthly/users",
        permit.context.requiresToken,
        api.counts.monthlyUserCount
    );

    //================subscription================== 
    app.put(
        "/api/subscriptions",
        permit.context.requiresToken,
        api.subscriptions.buy
    );
    app.get(
        "/api/subscriptions",
        permit.context.requiresToken,
        api.subscriptions.get
    );
    log.end();

    //================My day In-Person================== 

    app.post(
        "/api/inPersons/create",
        permit.context.builder,
        // validator.users.get, 
        api.inPersons.create
    );

    app.get(
        "/api/inPersons/customerList",
        permit.context.builder,
        // validator.users.get, 
        api.inPersons.customerList
    );

    app.put(
        "/api/inPersons/inPersonVisitorUpdate",
        permit.context.requiresToken,
        // validator.users.update, 
        api.inPersons.inPersonVisitorUpdate
    );

    app.get(
        "/api/inPersons/scheduledCustomerList",
        permit.context.builder,
        // validator.users.get, 
        api.inPersons.scheduledCustomerList
    );

    app.get(
        "/api/inPersons/inPersonVisitorsWMY",
        permit.context.builder,
        // validator.users.get, 
        api.inPersons.inPersonVisitorsWMY
    );

    app.get(
        "/api/inPersons/search",
        permit.context.requiresToken,
        api.inPersons.search
    );

    //================My day Phone================== 
    app.post(
        "/api/phone/create",
        permit.context.builder,
        // validator.users.get, 
        api.phone.create
    );

    app.put(
        "/api/phone/PhoneAndSmsUpdate",
        permit.context.requiresToken,
        // validator.users.update, 
        api.phone.PhoneAndSmsUpdate
    );

    //================Payment==================
    app.post(
        "/api/payment/payAmount",
        permit.context.builder,
        // validator.users.get, 
        api.payment.payAmount
    );

    app.post(
        "/api/payment/freeTrial",
        permit.context.builder,
        // validator.users.get, 
        api.payment.freeTrial
    );

    app.get(
        "/api/payment/subscriptionHistory",
        permit.context.requiresToken,
        // validator.users.get, 
        api.payment.subscriptionHistory
    );

    app.post(
        "/api/payment/unsubscribePlan",
        permit.context.builder,
        // validator.users.get, 
        api.payment.unsubscribePlan
    );

    //================Calendar==================
    app.post(
        "/api/calendar/event/create",
        permit.context.builder,
        // validator.users.get, 
        api.calendar.create
    );

    app.put(
        "/api/calendar/event/update",
        permit.context.requiresToken,
        // validator.users.update, 
        api.calendar.updateEvent
    );

    app.get(
        "/api/calendar/getEventsByUserId",
        permit.context.requiresToken,
        // validator.users.get, 
        api.calendar.getEventsByUserId
    );

    //================Team==================
    app.post(
        "/api/team/member/add",
        permit.context.builder,
        // validator.users.get, 
        api.team.create
    );

    app.get(
        "/api/team/getMemberList",
        permit.context.requiresToken,
        // validator.users.get, 
        api.team.getMemberList
    );

    //================assignLeads==================
    app.post(
        "/api/assignLeads/potentialCustomer/add",
        permit.context.builder,
        // validator.users.get, 
        api.assignLeads.create
    );

    app.get(
        "/api/assignLeads/potentialCustomer/list",
        permit.context.builder,
        permit.context.requiresToken,
        api.assignLeads.potentialCustomerList
    );

    app.post(
        "/api/assignLeads/mapAddress/add",
        permit.context.builder,
        // validator.users.get, 
        api.assignLeads.mapAddress
    );

    app.put(
        "/api/assignLeads/mapAddressUpdate/:id",
        permit.context.builder,
        // validator.users.update, 
        api.assignLeads.mapAddressUpdate
    );

    app.get(
        "/api/assignLeads/mapAddressList",
        permit.context.builder,
        permit.context.requiresToken,
        api.assignLeads.mapAddressList
    );

    app.delete(
        "/api/assignLeads/deletePotentialCustomer/:id",
        permit.context.requiresToken,
        api.assignLeads.deletePotentialCustomer
    );

    //================Vendor Payment (subscription)==================
    app.post(
        "/api/vendorPayment/buySubscription",
        permit.context.builder,
        // validator.users.get, 
        api.vendorPayment.buySubscription
    );

    app.post(
        "/api/vendorPayment/unsubscribePlan",
        permit.context.builder,
        // validator.users.get, 
        api.vendorPayment.unsubscribePlan
    );

    //================Chat==================
    app.get(
        '/api/chat/getOldChat',
        permit.context.builder,
        api.chat.getOldChat
    );

    //================Customers==================
    app.post(
        "/api/customers/add",
        permit.context.builder,
        permit.context.requiresToken,
        // validator.customers.create,
        api.customers.create
    );
    app.get(
        "/api/customers/ListByUserId",
        permit.context.builder,
        // permit.context.requiresToken,
        api.customers.ListByUserId
    );


    app.post(
        "/api/customers/delete",
        permit.context.requiresToken,
        api.customers.deleteUser
    );

    app.put(
        "/api/customers/update",
        permit.context.requiresToken,
        api.customers.update
    );

    app.post(
        '/api/customers/customerExcelUploader',
        permit.context.requiresToken,
        api.customers.customerExcelUploader
    )

};


exports.configure = configure;