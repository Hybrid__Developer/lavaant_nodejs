"use strict";
var moment = require('moment');
exports.toModel = entity => {

    const model = {
        id: entity.id,
        orderNo: entity.orderNo,
        totalItem: entity.totalItem,
        totalAmount: entity.totalAmount,
        status: entity.status,
        userId: entity.userId,
        addressId: entity.productId,
        products: entity.products,
        updatedOn: entity.updatedOn,
        createdOn: moment(entity.createdOn).format('L')
    };
    if (entity.addressId && entity.addressId.address) {
        let addressModel = {
            id: entity.addressId.id,
            address: entity.addressId.address,
            city: entity.addressId.city,
            state: entity.addressId.state,
            zipCode: entity.addressId.zipCode,
            area: entity.addressId.area,
            contactName: entity.addressId.contactName,
        }
        model.shipTo = addressModel
    }
    if (entity.userId && entity.userId.firstName) {
        let userModel = {
            firstName: entity.userId.firstName,
            lastName: entity.userId.lastName,
            profilePic: entity.userId.profilePic
        }
        model.user = userModel
    }
    return model;
};

exports.toSearchModel = entities => {
    return entities.map(entity => {
        return exports.toModel(entity);
    });
};
