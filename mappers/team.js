"use strict";

exports.toModel = (entity) => {

    const model = {
        id: entity.id,
        name: entity.name,
        birthdayDate: entity.birthdayDate,
        hireDate: entity.hireDate,
        nickName: entity.nickName,
        phone: entity.phone,
        sales: entity.sales,
        anniversaryDate: entity.anniversaryDate
    };
    return model;
};

exports.toSearchModel = entities => {
    return entities.map((item, key) => {
        return exports.toModel(item, key);
    });
};
