"use strict";

exports.toModel = (entity) => {

    const model = {
        id: entity.id,
        start: entity.start,
        startTime: entity.startTime,
        endTime: entity.endTime,
        name: entity.name,
        description: entity.description,
        reminder: entity.reminder
    };
    return model;
};

exports.toSearchModel = entities => {
    return entities.map((item, key) => {
        return exports.toModel(item, key);
    });
};

exports.toProductCModel = (entity, id) => {

    const model = {
        id: id,
        name: entity.name ? entity.name : entity,
        isShow: entity.name ? true : false,
    };
    return model;
};

exports.toProductCSearchModel = entities => {
    return entities.map((item, key) => {
        return exports.toProductCModel(item, key);
    });
};