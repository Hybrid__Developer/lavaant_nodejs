"use strict";

exports.toModel = (entity) => {

    const model = {
        potentialCusId: entity.potentialCusId,
        lat: entity.lat,
        lng: entity.lng,
        location: entity.location
    };
    return model;
};

exports.toSearchModel = entities => {
    return entities.map((item, key) => {
        return exports.toModel(item, key);
    });
};