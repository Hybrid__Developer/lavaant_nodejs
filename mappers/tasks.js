"use strict";

exports.toModel = (entity) => {

    const model = {
        id: entity.id,
        details: entity.details,
        dueDate: entity.dueDate,
        reminder: entity.reminder,
        status: entity.status
    };
    if (entity.categoryId) {
        if (entity.categoryId._doc) {
            model.category = {
                id: entity.categoryId.id,
                name: entity.categoryId.name
            }
        } else {
            model.category = {
                id: entity.categoryId.toString()
            }
        }
    }
    return model;
};

exports.toSearchModel = entities => {
    return entities.map((item, key) => {
        return exports.toModel(item, key);
    });
};
