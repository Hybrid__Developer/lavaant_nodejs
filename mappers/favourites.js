"use strict";

exports.toModel = (entity) => {

    const model = {
        id: entity.id,
    };
    if (entity.subCategories && entity.subCategories.length) {
        let subCategoriesModel = []
        entity.subCategories.forEach(subcategory => {
            subCategoriesModel.push({
                id: subcategory.id.id,
                name: subcategory.id.name
            })
        });
        model.subCategories =subCategoriesModel
    }
    if (entity.stores && entity.stores.length) {
        let vendorsModel = []
        entity.stores.forEach(vendors => {
            vendorsModel.push({
                id: vendors.id.id,
                name: vendors.id.name,
                logo: vendors.id.logo,
            })
        });
        model.stores =vendorsModel
    }
    return model;
};

exports.toSearchModel = entities => {
    return entities.map((item, key) => {
        return exports.toModel(item, key);
    });
};

exports.toProductCModel = (entity) => {
    const model = {
        id: entity.id,
        name: entity.name,
        isShow: entity.isShow || false,
    };
    return model;
};

exports.toProductCSearchModel = entities => {
    return entities.map((item) => {
        return exports.toProductCModel(item);
    });
};

exports.toStoreModel = (entity) => {
    entity = entity.id
    const model = {
        id: entity.id,
        name: entity.name,
        logo: entity.logo
    };
    return model;
};

exports.toStoreSearchModel = entities => {
    return entities.map((item) => {
        return exports.toStoreModel(item);
    });
};