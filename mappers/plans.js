"use strict";

exports.toModel = (entity) => {

    const model = {
        id: entity.id,
        title: entity.title,
        description: entity.description,
        discountedPrice: entity.discountedPrice,
        price: entity.price,
        price1: entity.price1,
        price2: entity.price2,
        role: entity.role,
        status: entity.status,
        validity: entity.validity
    };
    if (entity.features && entity.features.length !== 0) {
        model.features = entity.features
    }
    return model;
};

exports.toSearchModel = entities => {
    return entities.map((item, key) => {
        return exports.toModel(item, key);
    });
};

exports.toProductCModel = (entity, id) => {

    const model = {
        id: id,
        name: entity.name ? entity.name : entity,
        isShow: entity.name ? true : false,
    };
    return model;
};

exports.toProductCSearchModel = entities => {
    return entities.map((item, key) => {
        return exports.toProductCModel(item, key);
    });
};