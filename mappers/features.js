"use strict";

exports.toModel = (entity) => {

    const model = {
        id: entity.id,
        name: entity.name,
        description: entity.description,
        status: entity.status
    };
    return model;
};

exports.toSearchModel = entities => {
    return entities.map((item, key) => {
        return exports.toModel(item, key);
    });
};
