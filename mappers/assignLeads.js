"use strict";

exports.toModel = (entity) => {

    const model = {
        id: entity.id,
        company: entity.company,
        customerName: entity.customerName,
        address: entity.address,
        phone: entity.phone,
        email: entity.email,
        note1: entity.note1,
        note2: entity.note2
    };
    return model;
};

exports.toSearchModel = entities => {
    return entities.map((item, key) => {
        return exports.toModel(item, key);
    });
};