"use strict";

exports.toModel = entity => {

    const model = {
        id: entity.id,
        name: entity.name,
        description: entity.description,
        quantity: entity.quantity,
        overAllPrice: entity.overAllPrice,
        status: entity.status,
        image: entity.image,
        note: entity.note,
        height: entity.height,
        width: entity.width,
        length: entity.length,
        pdf: entity.pdf,
        manufacturer: entity.manufacturer,
        rvDescription: entity.rvDescription,
        sku: entity.sku,
        uom: entity.uom,
        assignedVendors: entity.assignedVendors,
        costPerEach: entity.costPerEach,
        subCategory: entity.subCategory,
        variation: entity.variation,
        updatedOn: entity.updatedOn,
        createdOn: entity.createdOn,
    };
    if (entity.category) {
        let categoryModel = {
            id: entity.category.id,
            name: entity.category.name
        }
        model.category = categoryModel
    }
    if (entity.subCategory) {
        let subCategory = {
            id: entity.subCategory.id,
            name: entity.subCategory.name
        }
        model.subCategory = subCategory
    }
    return model;
};

exports.toSearchModel = entities => {
    return entities.map(entity => {
        return exports.toModel(entity);
    });
};
