"use strict";

exports.toModel = entity => {
    const model = {
        id: entity.id,
        plan: entity.plan,
        amount: entity.amount,
        userId: entity.userId,
        planId: entity.planId,
        expirePlan: entity.expirePlan
    };
    return model;
};

exports.toSearchModel = entities => {
    return entities.map(entity => {
        return exports.toModel(entity);
    });
};