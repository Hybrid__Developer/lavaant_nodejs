"use strict";

exports.toModel = entity => {
  const model = {
    id: entity.id,
    firstName: entity.firstName,
    lastName: entity.lastName,
    mobileNo: entity.mobileNo,
    alternateMobileNo: entity.alternateMobileNo,
    email: entity.email,
    address: entity.address,
    anniversary: entity.anniversary,
    amount: entity.amount,
    currentPlan: entity.currentPlan,
    subscriptionId: entity.subscriptionId,
    profilePic: entity.profilePic,
    dob: entity.dob,
    role: entity.role,
    status: entity.status,
    anniversary: entity.anniversary,
    currentPlan: entity.currentPlan,
    isShow: entity.isShow,
    country: entity.country,
    deviceToken: entity.deviceToken,
    workDetails: entity.workDetails,
    otherInformation: entity.otherInformation
  };
  if (entity.customerGroup) {
    model.customerGroup = {
      name: entity.customerGroup.name,
      id: entity.customerGroup.id
    }
  }
  if (entity.addressId) {
    model.currentAddress = entity.addressId
  }
  return model;
};
exports.toLoginModel = entity => {
  let login = exports.toModel(entity);
  login.token = entity.token
  return login
};
exports.toSearchModel = entities => {
  return entities.map(entity => {
    return exports.toModel(entity);
  });
};
