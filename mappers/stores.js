"use strict";

exports.toModel = entity => {
    const model = {
        id: entity.id,
        name: entity.name,
        owner: entity.owner,
        address: entity.address,
        email: entity.email,
        city: entity.city,
        state: entity.state,
        zipCode: entity.zipCode,
        description: entity.description,
        contactName: entity.contactName,
        contactNumber: entity.contactNumber,
        status: entity.status,
        alternateContactNumber: entity.alternateContactNumber,
        area: entity.area,
        logo: entity.logo
    };
    return model;
};

exports.toSearchModel = entities => {
    return entities.map(entity => {
        return exports.toModel(entity);
    });
};
