"use strict";

exports.toModel = (entity) => {

    const model = {
        id: entity.id,
        paymentId: entity.paymentId,
        plan: entity.plan,
        user: entity.user,
        expiryDate: entity.expiryDate,
        status: entity.status
    };
    return model;
};

exports.toSearchModel = entities => {
    return entities.map((item, key) => {
        return exports.toModel(item, key);
    });
};
