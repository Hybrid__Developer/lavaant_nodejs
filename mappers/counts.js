'use strict'
const removeAndSetElement = (data, item) => {
    data.pop();
    let insertAtIndex = (item._id.month - 1)
    let stringToBeInserted = item.count
    data.splice(insertAtIndex, 0, stringToBeInserted);
    return data
}
exports.toModel = (entity) => {
    const model = {}
    model.labels = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    let res = []
    if (entity.customer && entity.customer.length) {
        let data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        entity.customer.forEach(customer => {
            removeAndSetElement(data, customer)
        });
        res.push({
            data: data,
            label: 'Customers'
        })
    }
    if (entity.vendor && entity.vendor.length) {
        let data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        entity.vendor.forEach(vendor => {
            removeAndSetElement(data, vendor)
        });
        res.push({
            data: data,
            label: 'Vendors'
        })
    }
    model.barChartData = res
    return model
}

exports.toSearchModel = (entities) => {
    return entities.map((entity) => {
        return exports.toModel(entity)
    })
}