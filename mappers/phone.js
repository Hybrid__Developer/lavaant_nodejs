"use strict";

exports.toModel = (entity) => {

    const model = {
        id: entity.id,
        name: entity.name,
    };

    return model;
};