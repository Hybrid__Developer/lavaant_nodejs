'use strict'
const cron = require('cron').CronJob
var moment = require("moment");
const pushNotificationService = require("../services/push-notification");

const start = (startOn, logger) => {
    const log = logger.start(`settings:database:configure`);
    let job = new cron({
        cronTime: startOn,
        onTick: () => {
            console.log('running a task at 5 am daily.');
            sendTaskNotification(logger)
        },
        start: true
    })
    log.end();
}
console.log('After job instantiation');
exports.schedule = async logger => {
    start(`* * 5 * * *`, logger)
}

const sendTaskNotification = async (logger) => {
    const log = logger.start(`jobs:taskNotification:sendTaskNotification`);
    let tasks = await db.tasks.find({
        status: {
            $ne: 'completed'
        },
        // $and: [{
        // fromDate: {
        //     '$lte': moment(new Date()).startOf('day').toDate()
        // },
        toDate: {
            '$gte': moment(new Date()).startOf('day').toDate()
        },
        //   }],

    }).populate('categoryId userId')

    for (const task of tasks) {
        let msg = {
            data: `task reminder for ${task.details} with in category '${task.categoryId.name} has duedate ${task.dueDate}.`,
            taskId: task.id
        }
        let title = `task reminder for ${task.details}`
        pushNotificationService.pushNotification(task.userId, title, msg, logger)

    }
    log.end();
    console.log('task', tasks)
}