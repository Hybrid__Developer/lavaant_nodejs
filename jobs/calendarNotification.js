'use strict'
const cron = require('cron').CronJob
var moment = require("moment");
const pushNotificationService = require("../services/push-notification");

const start = (startOn, logger) => {
    const log = logger.start(`settings:database:configure`);
    let job = new cron({
        cronTime: startOn,
        onTick: () => {
            console.log('running a task after every minute.');
            sendTaskNotification(logger)
        },
        start: true
    })
    log.end();
}
console.log('After job instantiationn calendar module');
exports.schedule = async logger => {
    start(`* * * * *`, logger)
}

const sendTaskNotification = async (logger) => {
    const log = logger.start(`jobs:taskNotification:sendTaskNotification`);
    let events = await db.calendar.find({
        reminder: true,
        date: {
            '$gte': moment(new Date()).startOf('day').toDate()
        },
        time: {
            $gte: moment().startOf("minute").format('hh:mm'),
            $lt: moment().endOf("minute").format('hh:mm')
        }
    })


    for (const event of events) {
        let msg = {
            data: `event reminder for ${event.description} with date '${event.date} and time ${event.time}.`,
            eventName: event.eventName
        }
        pushNotificationService.pushNotification(msg, logger);
    }
    log.end();
}
