
const set = async (model, feature, context) => {
  const log = context.logger.start("services:feature:set");
  if (model.name !== "string" && model.name !== undefined) {
    feature.name = model.name;
  }
  if (model.description !== "string" && model.description !== undefined) {
    feature.description = model.description;
  }
  if (model.status !== "string" && model.status !== undefined) {
    feature.status = model.status;
  }
  if (model.role !== "string" && model.role !== undefined) {
    feature.role = model.role;
  }
  log.end();
  await feature.save();
  return feature;
};

const build = async (model, context) => {
  const log = context.logger.start(`services:feature:build${model}`);
  let buildModel = {
    name: model.name.toLowerCase(),
    description: model.description.toLowerCase(),
    role: model.role.toLowerCase(),
    status: 'active',
    createdOn: new Date(),
    updatedOn: new Date()
  }
  const feature = await new db.features(buildModel).save();
  log.end();
  return feature;
};

const create = async (model, context) => {
  const log = context.logger.start("services:feature:create");
  let findfeatureModel = {
    name: model.name.toLowerCase(),
    role: model.role
  }

  const isfeatureExists = await db.features.findOne(findfeatureModel);
  if (isfeatureExists) {
    return {
      err: `feature already exists for role ${model.role}`
    }
  }
  const feature = await build(model, context);
  log.end();
  return feature;
};


const getById = async (id, context) => {
  const log = context.logger.start(`services:feature:getById:${id}`);
  const feature = await db.features.findById(id);
  log.end();
  return feature;
};

const get = async (query, context) => {
  const log = context.logger.start(`services/feature/get:${query}`)
  let feature
  if (query === "") {
    feature = await db.features.find({ status: 'active' }).sort({ 'name': 1 })
    return feature
  }
  if (typeof query === 'string') {
    if (global.mongo.Types.ObjectId(query)) {
      feature = await db.features.findById(query)
      return feature
    }
  }
  if (query.role) {
    feature = await db.features.find({
      role: query.role,
      status: query.status || 'active'
    })
    return feature
  }
  if (query.status) {
    feature = await db.features.find({
      status: query.status || 'active'
    })
    return feature
  }
  log.end()
  return null
}

const update = async (id, model, context) => {
  const log = context.logger.start(`services:feature:update`);
  let entity = await db.features.findById(id);
  if (!entity) {
    throw new Error("invalid feature Id");
  }
  let role = entity.role
  if (model.role !== null && model.role !== undefined && model.role !== 'string') {
    if (role !== model.role) {
      role = model.role
    }

  }
  const isfeatureExists = await db.features.findOne({
    name: model.name.toLowerCase(),
    role: role
  });
  if (isfeatureExists) {
    if (isfeatureExists.id !== entity.id) {
      return {
        err: 'feature already exists'
      }
    }
  }
  const feature = await set(model, entity, context);
  log.end();
  return feature
};

exports.create = create;
exports.get = get;
exports.update = update;
exports.getById = getById;
