const moment = require("moment");

const build = async (model, context) => {
    const log = context.logger.start(`services:calendar:build${model}`);
    const { userId, start, name, description, time, durationHours, durationMinutes } = model;

    let duration = await moment(durationHours, 'HH:mm').add(durationMinutes, 'minutes').format('HH:mm');
    console.log('duration =>', duration);
    let endTime = await moment(time, 'HH:mm').add(durationHours, 'hours').add(durationMinutes, 'minutes').format('HH:mm');

    let eventModel = {
        userId: userId,
        start: start,
        name: name,
        description: description,
        startTime: time,
        endTime: endTime,
        duration: duration,
        createdOn: new Date(),
        updatedOn: new Date()
    }
    console.log('event model', eventModel);
    const event = await new db.calendar(eventModel).save();
    console.log('event', event);
    log.end();
    return event;
};

const create = async (model, context) => {
    const log = context.logger.start("services:calendar:create");

    const event = await build(model, context);
    log.end();
    return event;
};


const updateEvent = async (model, context) => {
    const { eventId, start, name, description, time, durationHours, durationMinutes, reminder } = model;
    const log = context.logger.start("services:calendar:updateEvent");

    let endTime = await moment(time, 'HH:mm').add(durationHours, 'hours').add(durationMinutes, 'minutes').format('HH:mm');

    let event = await db.calendar.findOne({ _id: eventId });
    if (!event) {
        throw new Error("event not exist");
    }

    if (start !== "" && start !== "string" && start !== undefined) {
        event.start = start
    }
    if (time !== "" && time !== "string" && time !== undefined) {
        event.startTime = time
    }
    if (endTime !== "" && endTime !== "string" && endTime !== undefined && endTime !== 'Invalid date') {
        event.endTime = endTime
    }
    if (name !== "" && name !== "string" && name !== undefined) {
        event.name = name
    }
    if (description !== "" && description !== "string" && description !== undefined) {
        event.description = description
    }
    if (reminder !== "" && reminder !== "string" && reminder !== undefined) {
        event.reminder = reminder
    }
    await event.save()


    log.end();
    return event;
};


const getEventsByUserId = async (query, context) => {
    const log = context.logger.start(`services:calendar:getEventsByUserId:${query.userId}`);
    const events = await db.calendar.find({ userId: query.userId });
    log.end();
    return events;
};

// markedDate: moment(new Date()).format("YYYY-MM-DD")

// HH = 24 hour time
// moment().startOf('month').format('YYYY-MM-DD HH:mm:ss');
// "2019-07-01 00:00:00"

// hh is 12 hour time, adding "a" to the end will show am/pm
// moment().startOf('minute').format('YYYY-MM-DD hh:mm:ss a');
// "2019-07-01 12:00:00 am"




exports.create = create;
exports.updateEvent = updateEvent;
exports.getEventsByUserId = getEventsByUserId;
