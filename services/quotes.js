var moment = require('moment');
const build = async (model, context) => {
  const log = context.logger.start(`services:quote:build${model}`);
  let buildModel = {
    title: model.title,
    writter: (model.writter === '' && model.writter === 'string' && model.writter === undefined) ? model.writter.toLowerCase() : '',
    createdOn: new Date()
  }
  const quote = await new db.quotes(buildModel).save();
  log.end();
  return quote;
};

const create = async (model, context) => {
  const log = context.logger.start("services:quote:create");
  const quote = await build(model, context);
  log.end();
  return quote;
};

const get = async (query, context) => {
  const log = context.logger.start(`services/quote/get:${query}`)
  let quote
  let config
  quote = await db.quotes.find({ status: 'active' })
  if (quote.length) {
    let randomNo = Math.floor(Math.random() * (quote.length - 0));
     config = await db.config.findOne()
    if (config == null || config.length == 0) {
      config = await new db.config({
        quoteOfDay: quote[randomNo].title,
        createdOn: new Date(),
        updatedOn: new Date(),
        quoteUpdatedOn: new Date()
      }).save()
    } else if(!(moment(moment(config.quoteUpdatedOn).format('MM-DD-YYYY')).isSame(moment(new Date()).format('MM-DD-YYYY')))){
      config.quoteOfDay = quote[randomNo].title
      config.quoteUpdatedOn = new Date()
      await config.save()
    }
  }
  log.end()
  return {title: config.quoteOfDay}
}

exports.create = create;
exports.get = get;
