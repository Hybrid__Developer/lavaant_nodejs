const mongoose = require("mongoose");
const ObjectId = require("mongodb").ObjectID;
const moment = require('moment');
const { Router } = require("express");
// const set = (model, inPerson, context) => {
//   const log = context.logger.start("services:inPerson:set");
//   if (model.name !== "string" && model.name !== undefined) {
//     inPerson.name = model.name;
//   }
//   log.end();
//   inPerson.save();
//   return inPerson;
// };



const date = () => {
  let dat = new Date();

  const d = {
    '$gte': moment(dat, "DD-MM-YYYY").startOf('day').toDate(),
    '$lt': moment(dat, "DD-MM-YYYY").endOf('day').toDate()
  }
  return d;
}

const build = async (model, context) => {
  const log = context.logger.start(`services:inPerson:build${model}`);
  let buildModel = {
    userId: model.userId,
    type: model.type,
    toVisit: model.toVisit,
    createdOn: new Date(),
    updatedOn: new Date()
  }
  const inPerson = await new db.inPersons(buildModel).save();
  log.end();
  return inPerson;
};


const create = async (model, context) => {
  const log = context.logger.start("services:inPerson:create");

  const d = date();
  const isinPersonScheduleExists = await db.inPersons.findOne({ userId: model.userId, type: model.type, createdOn: d });
  if (isinPersonScheduleExists) await db.inPersons.findOneAndDelete({ userId: model.userId, type: model.type, createdOn: d }, function (err, docs) {
    console.log('err', err);
  })

  const inPerson = await build(model, context);
  log.end();
  return inPerson;
};


const getCustomerList = async (query, context) => {
  const log = context.logger.start(`services:inPerson:getCustomerList`);
  const { userId, type } = query;

  let users = await db.user.find();
  users.count = await db.user.find({}).countDocuments();
  let inPersonUsers = await db.inPersons.find({ userId: userId, type: type, createdOn: date() });
  if (inPersonUsers == '') {
    log.end();
    return users;
  }

  let personsCus = inPersonUsers[0].toVisit;
  users.forEach((user, index) => {
    personsCus.forEach((customer) => {
      if (user.id === customer.id) {
        let objUser = user;
        users.splice(index, 1);

        objUser.isShow = true
        users.splice(index, 0,
          objUser
        );
      }
    })
  })
  log.end();
  return users;
};


const inPersonVisitorUpdate = async (model, context) => {
  const { userId, customerId, type, note1, note2, contact, recordNotes, firstName, lastName, phoneNo, recordTaskReminder } = model;
  const log = context.logger.start("services:inPerson:inPersonVisitorUpdate");
  if (type === "string" || type === undefined || type === "") {
    throw new Error("type is required");
  }
  let inPersonUsers = await db.inPersons.findOne({ userId: userId, type: type, createdOn: date() });
  if (!inPersonUsers) {
    throw new Error("invalid inPerson");
  }
  for (const visitor of inPersonUsers.toVisit) {
    if (visitor.id === customerId) {
      if (note1 !== "" && note1 !== "string" && note1 !== undefined) {
        visitor.note1 = note1
      }
      if (note2 !== "" && note2 !== "string" && note2 !== undefined) {
        visitor.note2 = note2
      }
      if (contact !== "" && contact !== "string" && contact !== undefined) {
        visitor.contact = contact
      }
      if (recordNotes !== "" && recordNotes !== "string" && recordNotes !== undefined) {
        visitor.recordNotes = recordNotes
      }
      if (firstName !== "" && firstName !== "string" && firstName !== undefined) {
        visitor.recordContact.firstName = firstName
      }
      if (lastName !== "" && lastName !== "string" && lastName !== undefined) {
        visitor.recordContact.lastName = lastName
      }
      if (phoneNo !== "" && phoneNo !== "string" && phoneNo !== undefined) {
        visitor.recordContact.phoneNo = phoneNo
      }
      if (recordTaskReminder.reminderText !== "" && recordTaskReminder.reminderText !== "string" && recordTaskReminder.reminderText !== undefined) {
        visitor.recordTaskReminder.reminderText = recordTaskReminder.reminderText
      }
      if (recordTaskReminder.reminderNote !== "" && recordTaskReminder.reminderNote !== "string" && recordTaskReminder.reminderNote !== undefined) {
        visitor.recordTaskReminder.reminderNote = recordTaskReminder.reminderNote
      }
      if (recordTaskReminder.fromDate !== "" && recordTaskReminder.fromDate !== "string" && recordTaskReminder.fromDate !== undefined) {
        visitor.recordTaskReminder.fromDate = recordTaskReminder.fromDate
      }
      if (recordTaskReminder.toDate !== "" && recordTaskReminder.toDate !== "string" && recordTaskReminder.toDate !== undefined) {
        visitor.recordTaskReminder.toDate = recordTaskReminder.toDate
      }
      await inPersonUsers.save()
    }
  }
  log.end();
  return inPersonUsers;
};


// {
//   $gte: new Date('2020-07-06T00:00:00.000Z'),
//     $lt: new Date('2020-07-21T18:29:59.999Z')
// }


const scheduledCustomerList = async (query, context) => {
  const { fromDate, toDate, getByType, type } = query;
  const log = context.logger.start(`services:inPerson:scheduledCustomerList`);
  const dat = {
    '$gte': moment(fromDate, "DD-MM-YYYY").startOf('day').toDate(),
    '$lt': moment(toDate, "DD-MM-YYYY").endOf('day').toDate()
  }

  const startOfTime = moment().startOf(getByType).format('YYYY-MM-DD hh:mm');
  const endOfTime = moment().endOf(getByType).format('YYYY-MM-DD hh:mm');

  let inPersonUsers
  if (type == 'all' && !getByType && !fromDate && !toDate) {
    inPersonUsers = await db.inPersons.find({ userId: query.userId });
    if (inPersonUsers) {
      console.log('ALL', inPersonUsers);
      return inPersonUsers;
    }
    else {
      throw new Error("something went wrong");
    }
  }
  if (type == 'all' && !getByType && dat) {
    inPersonUsers = await db.inPersons.find({
      userId: query.userId, createdOn: dat
    });
    console.log('all  and formDate toDate', inPersonUsers);
    return inPersonUsers;
  }
  if (type && !getByType && dat) {
    inPersonUsers = await db.inPersons.find({
      userId: query.userId, type: type, createdOn: dat
    });
    console.log('type and formDate toDate', inPersonUsers);
    return inPersonUsers;
  }

  let inPersonsVisitorData;
  if (getByType == 'week') {
    if (query.type == 'all') {
      inPersonsVisitorData = await db.inPersons.find({
        userId: query.userId,
        createdOn: {
          $gte: startOfTime,
          $lt: endOfTime
        }
      });
      console.log('week, all');
    } else if (query.type) {
      inPersonsVisitorData = await db.inPersons.find({
        userId: query.userId, type: query.type,
        createdOn: {
          $gte: startOfTime,
          $lt: endOfTime
        }
      });
      console.log('week, type');
    }
  }
  if (getByType == 'month') {
    if (query.type == 'all') {
      inPersonsVisitorData = await db.inPersons.find({
        userId: query.userId,
        createdOn: {
          $gte: startOfTime,
          $lt: endOfTime
        }
      });
      console.log('month, all');
    } else if (query.type) {
      inPersonsVisitorData = await db.inPersons.find({
        userId: query.userId, type: query.type,
        createdOn: {
          $gte: startOfTime,
          $lt: endOfTime
        }
      });
      console.log('month, type');
    }
  }
  if (getByType == 'year') {
    if (query.type == 'all') {
      inPersonsVisitorData = await db.inPersons.find({
        userId: query.userId,
        createdOn: {
          $gte: startOfTime,
          $lt: endOfTime
        }
      });
      console.log('year, all');
    } else if (query.type) {
      inPersonsVisitorData = await db.inPersons.find({
        userId: query.userId, type: query.type,
        createdOn: {
          $gte: startOfTime,
          $lt: endOfTime
        }
      });
      console.log('year, type');
    }
  }

  log.end();
  return inPersonsVisitorData;
};



// let areaBasedCustomer = []
//   for (const user of inPersonUsers) {
//     for (let i = 0; i < user.toVisit.length; i++) {
//       let cusId = user.toVisit[i].id;
//       // console.log('cusid', cusId);
//       let customer = await db.user.findById(cusId).populate('addressId');
//       if (customer.addressId) {
//         if (customer.addressId.area == query.area) {
//           areaBasedCustomer.push(customer.addressId);
//         }
//       }
//     }
//   }

//   let duplicatecustomerRemoved = areaBasedCustomer.filter((v, i, a) => a.findIndex(t => (t.id === v.id)) === i)

//   if (duplicatecustomerRemoved) {
//     return duplicatecustomerRemoved;
//   }
//   else {
//     throw new Error("something went wrong");
//   }










// const inPersonVisitorsWMY = async (query, context) => {
//   const log = context.logger.start(`services:inPerson:inPersonVisitorsWMY`);

// }


// const search = async (query, context) => {
//   const log = context.logger.start(`services:inPerson:search`);
//   const { userId, type, searchString } = query;

//   let user = await db.user.find({ $or: [{ 'firstName': searchString }, { 'email': searchString }] });
//   let inPersonUsers = await db.inPersons.findOne({ userId: userId, type: type, createdOn: date() });
//   if (inPersonUsers == '' || inPersonUsers == null) {
//     log.end();
//     console.log('if not inperson then it is running', inPersonUsers);
//     return user;
//   }
//   else if (inPersonUsers) {
//     inPersonUsers.toVisit.forEach((customer, index) => {
//       if (user.id === customer.id) {
//         user.isShow = true
//       }
//     })
//   }
//   log.end();
//   return user
// };

const search = async (query, context) => {
  const log = context.logger.start(`services:inPerson:search`);
  let searchModel = {}
  let sortModel = {}
  searchModel = {
    $or: [
      {
        email: {
          $regex: '^' + query.searchString,
          $options: 'i'
        }
      },
      {
        firstName: {
          $regex: '^' + query.searchString,
          $options: 'i'
        }
      }],
    role: query.role || 'customer'
  }
  if (Object.keys(sortModel).length == 0) {
    sortModel.firstName = 1
  }
  let searchedUser = await db.user.find(searchModel).sort(sortModel);
  let inPersonUsers = await db.inPersons.findOne({ userId: query.userId, type: query.type, createdOn: date() });
  if (inPersonUsers == '' || inPersonUsers == null) {
    log.end();
    console.log('if not inperson then it is running', inPersonUsers);
    return searchedUser;
  }
  else if (inPersonUsers) {
    inPersonUsers.toVisit.forEach((customer, index) => {
      searchedUser.forEach((user, index) => {
        if (user.id === customer.id) {
          let objUser = user;
          searchedUser.splice(index, 1);

          objUser.isShow = true
          searchedUser.splice(index, 0,
            objUser
          );
        }
      })
    })
  }
  log.end();
  return searchedUser
};



exports.create = create;
// exports.update = update;
exports.getCustomerList = getCustomerList;
exports.inPersonVisitorUpdate = inPersonVisitorUpdate;
exports.scheduledCustomerList = scheduledCustomerList;
exports.search = search;