
const set = (model, category, context) => {
  const log = context.logger.start("services:category:set");
  if (model.name !== "string" && model.name !== undefined) {
    category.name = model.name;
  }
  log.end();
  category.save();
  return category;
};

const build = async (model, context) => {
  const log = context.logger.start(`services:category:build${model}`);
  let buildModel = {
    name: model.name.toLowerCase(),
    categoryOf: (model.categoryOf === '' && model.categoryOf === 'string' && model.categoryOf === undefined) ? '' : model.categoryOf.toLowerCase(),
    createdOn: new Date(),
    updatedOn: new Date()
  }
  if (buildModel.categoryOf === 'task') {
    buildModel.userId = context.user.id
  }
  const category = await new db.categories(buildModel).save();
  log.end();
  return category;
};

const create = async (model, context) => {
  const log = context.logger.start("services:category:create");
  let findCategoryModel = {
    name: model.name.toLowerCase(),
    categoryOf: (model.categoryOf === '' || model.categoryOf === 'string' || model.categoryOf === undefined) ? '' : model.categoryOf.toLowerCase()
  }
  if (model.name.toLowerCase() === 'task') {
    findCategoryModel.userId = context.user.id
  }
  const isCategoryExists = await db.categories.findOne(findCategoryModel);
  if (isCategoryExists) {
    return {
      err: 'Category already exists'
    }
  }
  const category = await build(model, context);
  log.end();
  return category;
};


const getById = async (id, context) => {
  const log = context.logger.start(`services:category:getById:${id}`);
  const category = await db.categories.findById(id);
  log.end();
  return category;
};

const get = async (query, context) => {
  const log = context.logger.start(`services/category/get:${query}`)
  let category
  if (query === "") {
      category = await db.categories.find({categoryOf:'',status:'active'}).sort({'name':1})
      return category
  }
  if (typeof query === 'string') {
    if (global.mongo.Types.ObjectId(query)) {
      category = await db.categories.findById(query)
      return category
    }
  }
  if (query.userId) {
    category = await db.categories.find({
      userId: query.userId
    })
    return category
  }
  log.end()
  return null
}

const update = async (id, model, context) => {
  const log = context.logger.start(`services:category:update`);

  let entity = await db.categories.findById(id);
  if (!entity) {
    throw new Error("invalid category");
  }
  const isCategoryExists = await db.categories.findOne({
    name: model.name.toLowerCase(),
    userId: context.user.id,
    categoryOf: entity.categoryOf
  });
  if (isCategoryExists) {
    return {
      err: 'Category already exists'
    }
  }
  const category = await set(model, entity, context);
  log.end();
  return category
};

exports.create = create;
exports.get = get;
exports.update = update;
exports.getById = getById;
