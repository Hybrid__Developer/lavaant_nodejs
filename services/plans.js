const ObjectId = require("mongodb").ObjectID;

const set = async (model, plan, context) => {
  const log = context.logger.start("services:plan:set");
  if (model.title !== "string" && model.title !== undefined) {
    plan.title = model.title;
  }
  if (model.description !== "string" && model.description !== undefined) {
    plan.description = model.description;
  }
  if (model.status !== "string" && model.status !== undefined) {
    plan.status = model.status;
  }
  if (model.price !== "string" && model.price !== undefined) {
    plan.price = model.price;
  }
  if (model.price1 !== "string" && model.price1 !== undefined) {
    plan.price1 = model.price1;
  }
  if (model.price2 !== "string" && model.price2 !== undefined) {
    plan.price2 = model.price2;
  }
  if (model.discountedPrice !== "string" && model.discountedPrice !== undefined) {
    plan.discountedPrice = model.discountedPrice;
  }
  if (model.validity) {
    let validity = {}
    validity.type = (model.validity.type !== 'string' && model.validity.type !== '' && model.validity.type !== undefined) ? model.validity.type : 'days'
    validity.timePeriod = (model.validity.timePeriod !== undefined && model.validity.timePeriod === 0) ? model.validity.timePeriod : 1
    plan.validity = validity
  }
  if (model.features && model.features.length !== 0) {
    if (model.features[0].id !== 'string' && model.features[0].id !== '' && model.features[0].id !== undefined) {
      plan.features = model.features
    }
  }
  log.end();
  await plan.save();
  return plan;
};

const build = async (model, context) => {
  const log = context.logger.start(`services:plan:build${model}`);
  let buildModel = {
    title: model.title.toLowerCase(),
    description: model.description,
    status: 'active',
    role: model.role || 'customer',
    price: model.price,
    price1: model.price1,
    price2: model.price2,
    discountedPrice: model.discountedPrice || 0,
    createdOn: new Date(),
    updatedOn: new Date()
  }
  if (model.validity) {
    let validity = {}
    validity.type = (model.validity.type !== 'string' && model.validity.type !== '' && model.validity.type !== undefined) ? model.validity.type : 'days'
    validity.timePeriod = (model.validity.timePeriod !== undefined && model.validity.timePeriod === 0) ? model.validity.timePeriod : 1
    buildModel.validity = validity
  }
  if (model.features && model.features.length !== 0) {
    if (model.features[0].id !== 'string' && model.features[0].id !== '' && model.features[0].id !== undefined) {
      buildModel.features = model.features
    }
  }
  const plan = await new db.plans(buildModel).save();
  log.end();
  return plan;
};

const create = async (model, context) => {
  const log = context.logger.start("services:plan:create");
  let findplanModel = {
    name: model.title.toLowerCase(),
    role: model.role
  }

  const isplanExists = await db.plans.findOne(findplanModel);
  if (isplanExists) {
    return {
      err: 'plan already exists'
    }
  }
  const plan = await build(model, context);
  log.end();
  return plan;
};


const getById = async (id, context) => {
  const log = context.logger.start(`services:plan:getById:${id}`);
  const plan = await db.plans.findById(id).populate('features.id');
  log.end();
  return plan;
};

const get = async (query, context) => {
  const log = context.logger.start(`services/plan/get:${query}`)
  let plan
  if (query === "") {
    plan = await db.plans.find({ status: 'active' }).sort({ 'name': 1 })
    return plan
  }
  if (typeof query === 'string') {
    if (global.mongo.Types.ObjectId(query)) {
      plan = await db.plans.findById(query)
      return plan
    }
  }
  if (query.role) {
    plan = await db.plans.find({
      role: query.role,
      status: query.status || 'active'
    })
    return plan
  }
  if (query.status) {
    plan = await db.plans.find({
      status: query.status || 'active'
    })
    return plan
  }
  log.end()
  return null
}

const update = async (id, model, context) => {
  const log = context.logger.start(`services:plan:update`);

  let entity = await db.plans.findById(id);
  if (!entity) {
    throw new Error("invalid plan");
  }
  if (model.title !== "string" && model.title !== undefined) {
    const isPlanExists = await db.plans.findOne({
      title: model.title.toLowerCase(),
      role: entity.role,
      _id: {
        $ne: ObjectId(id)
      }
    });
    if (isPlanExists) {
      return {
        err: 'Plan already exists'
      }
    }
  }
  const plan = await set(model, entity, context);
  log.end();
  return plan
};
const updateStatus = async (id, query, context) => {
  const log = context.logger.start(`services:tasks:update`);

  let isPlanExists = await db.plans.findById(id);
  if (!isPlanExists) {
    throw new Error("invalid plan");
  }
  isPlanExists.status = query.status
  await isPlanExists.save()
  log.end();
  return isPlanExists
};

const addFeature = async (model, context) => {
  const log = context.logger.start(`services:tasks:addFeature`);
  let features = []
  let isPlanExists = await db.plans.findById(model.planId);
  if (!isPlanExists) {
    throw new Error("invalid plan");
  }
  for (const feature of model.features) {
    let isFeatureExists = await db.features.findById(feature.id);
    if (isFeatureExists) {
      features.push({
        id: isFeatureExists.id
      })
    }
  }
  if (!isPlanExists.features.length) {
    isPlanExists.features = model.features
    await isPlanExists.save()
  } else {
    if (model.features.length) {
      isPlanExists.features.forEach((planFeatures, index) => {
        features.forEach(feature => {
          if (ObjectId(feature.id).equals(planFeatures.id)) {
            isPlanExists.features.splice(index, 1);
            isPlanExists.features.splice(index, 0, {
              "id": planFeatures.id
            });
          }
        })
      })
    }
    await isPlanExists.save()
  }
  log.end();
  return isPlanExists
};

const removeFeature = async (query, context) => {
  const log = context.logger.start(`services:tasks:removeFeature`);

  let isPlanFeatureExists = await db.features.findById(query.featureId);
  if (!isPlanFeatureExists) {
    throw new Error("invalid plan");
  }
  let isPlanExists = await db.plans.findById(query.planId);
  isPlanExists.features.forEach((plan, index) => {
    if (ObjectId(isPlanFeatureExists.id).equals(plan.id)) {
      isPlanExists.features.splice(index, 1);
    }
  })
  await isPlanExists.save()
  log.end();
  return isPlanExists
};
exports.create = create;
exports.get = get;
exports.update = update;
exports.getById = getById;
exports.updateStatus = updateStatus;
exports.addFeature = addFeature;
exports.removeFeature = removeFeature;