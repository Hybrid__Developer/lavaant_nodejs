const subCategoryService = require("./subCategories");
const ObjectId = require("mongodb").ObjectID;

const update = async (model, updateType, context) => {
  const log = context.logger.start(`services:favourites:update`);
  let user = await db.user.findById(context.user.id);
  if (!user) {
    throw new Error("invalid user");
  }
  let favouriteExist = await db.favourites.findOne({ userId: user.id })
  if (!favouriteExist) {
    let favourites = await new db.favourites({
      userId: user.id,
      stores: [{
        id: '5ed0adfa1016710bb8801f23'
      }],
      subCategories: [],
      createdOn: new Date(),
      updatedOn: new Date()
    }).save();
    user.favourites = favourites.id
    await user.save()
  }
  if (updateType === 'subCategory') {
    favouriteExist.subCategories = model.favouriteSubCategories
    await favouriteExist.save()
  }
  if (updateType === 'store') {
    let isStoreExist = await db.stores.findById(model.id)
    if (!isStoreExist) {
      throw new Error('vendor not found')
    }
    if (!favouriteExist.stores.length) {
      favouriteExist.stores = [{
        id: '5ed0adfa1016710bb8801f23'
      }]
      favouriteExist.stores.push(model)
    } else {
      let isAlreadyExist = false
      favouriteExist.stores.forEach((favouriteItem, index) => {
        if (ObjectId(favouriteItem.id.id).equals(model.id)) {
          isAlreadyExist = true
        }
      });
      if (!isAlreadyExist) {
        favouriteExist.stores.push(model)
      }
    }
    await favouriteExist.save()
  }
  favouriteExist = await get('', context)
  log.end()
  return favouriteExist;
};
const get = async (categoryId, context) => {
  const log = context.logger.start(`services:favourites:update`);
  let user = await db.favourites.findOne({ userId: context.user.id }).populate('subCategories.id').populate('stores.id');
  if (!user) {
    throw new Error("invalid user");
  }
  log.end();
  return user;
};
const getFavouriteSubCategories = async (categoryId, context) => {
  const log = context.logger.start(`services:favourites:getFavouriteSubCategories`);
  let subCategory = await subCategoryService.get({ categoryId: categoryId }, context);

  let favourites = await db.favourites.findOne({ userId: context.user.id }).populate('subCategories.id')
  let favouritesCount = 0

  if (favourites.subCategories.length) {
    subCategory.forEach((categoryItem, index) => {
      favourites.subCategories.forEach(favouriteItem => {
        if (favouriteItem.id.id === categoryItem.id) {
          favouritesCount = favouritesCount + 1
          subCategory.splice(index, 1);
          subCategory.splice(index, 0, {
            "id": categoryItem.id,
            "isShow": true,
            "name": categoryItem.name
          });
        }
      })
    })

  }
  log.end();
  return { subCategory: subCategory, favouritesCount: favouritesCount };
};
const getFavouriteStores = async (context) => {
  const log = context.logger.start(`services:favourites:getFavouriteSubCategories`);

  let favourites = await db.favourites.findOne({ userId: context.user.id }).populate('stores.id')
  log.end();
  return favourites.stores;
};
exports.get = get;
exports.updateFavouriteSubCategories = update;
exports.getFavouriteSubCategories = getFavouriteSubCategories
exports.getFavouriteStores = getFavouriteStores
exports.update = update;