const ObjectId = require("mongodb").ObjectID;
const userService = require("../services/users");
var moment = require("moment");

const setTask = async (model, task, context) => {
    const log = context.logger.start("services:tasks:set");
    if (model.details !== "string" && model.details !== undefined) {
        task.details = model.details;
    }
    if (model.categoryId !== "string" && model.categoryId !== undefined) {
        task.categoryId = model.categoryId;
    }
    if (model.reminder !== "string" && model.reminder !== undefined) {
        task.reminder = model.reminder;
    }
    if (model.dueDate !== "string" && model.dueDate !== undefined) {
        task.dueDate = new Date(model.dueDate);
    }
    log.end();
    await task.save();
    return task;
};

const build = async (model, context) => {
    const log = context.logger.start(`services:task:build${model}`);
    let buildModel = {
        details: model.details,
        categoryId: model.categoryId,
        reminder: (model.reminder !== '' && model.reminder !== 'string' && model.reminder !== undefined) ? model.reminder.toLowerCase() : '1day',
        dueDate: new Date(model.dueDate).toUTCString(),
        fromDate: model.fromDate,
        toDate: model.toDate,
        userId: context.user.id,
        status: model.status || 'pending',
        createdOn: new Date(),
        updatedOn: new Date()
    }
    const task = await new db.tasks(buildModel).save();
    log.end();
    return task;
};

const create = async (model, context) => {
    const log = context.logger.start("services:task:create");
    const isCategoryExists = await db.categories.findById(model.categoryId);
    if (!isCategoryExists) {
        throw new Error('Category not found')
    }
    if (model.reminder === '1day') {
        model.fromDate = moment(new Date(model.dueDate)).startOf('day').toDate()
        model.toDate = moment(new Date(model.dueDate)).endOf('day').toDate()
    }
    if (model.reminder === '2day') {
        model.fromDate = moment(new Date(model.dueDate)).startOf('day').toDate()
        model.toDate = moment(new Date(model.dueDate)).add(1, 'day').endOf('day').toDate()
    }
    if (model.reminder === '1week') {
        model.fromDate = moment(new Date(model.dueDate)).startOf('day').toDate()
        model.toDate = moment(new Date(model.dueDate)).add(1, 'week').subtract(1,'day').endOf('day').toDate()
    }
    if (model.reminder === '1month') {
        model.fromDate = moment(new Date(model.dueDate)).startOf('day').toDate()
        model.toDate = moment(new Date(model.dueDate)).add(1, 'month').subtract(1,'day').endOf('day').toDate()
    }
    const task = await build(model, context);
    log.end();
    return task;
};

const update = async (id, model, context) => {
    const log = context.logger.start(`services:task:update`);

    let entity = await db.tasks.findById(id);
    if (!entity) {
        throw new Error("invalid task id");
    }

    const task = await setTask(model, entity, context);

    log.end();
    return task
};
// const getById = async (id, context) => {
//   const log = context.logger.start(`services:task:getById:${id}`);
//   const task = await db.categories.findById(id);
//   log.end();
//   return task;
// };

const get = async (query, context) => {
    const log = context.logger.start(`services/task/get:${query}`)
    let task
    let sortModel = {}

    if (query.sortByName !== '' && query.sortByName !== 'string' && query.sortByName !== undefined) {
        sortModel.details = await userService.checkSortingOrder(query.sortByName)
    }
    if (query.sortByDate !== '' && query.sortByDate !== 'string' && query.sortByDate !== undefined) {
        sortModel.dueDate = await userService.checkSortingOrder(query.sortByDate)
    }
    if (query.sortByCompletedFirst !== '' && query.sortByCompletedFirst !== 'string' && query.sortByCompletedFirst !== undefined) {
        sortModel.taskCompletedOn = await userService.checkSortingOrder(query.sortByCompletedFirst)
    }
    if (Object.keys(sortModel).length == 0) {
        sortModel.dueDate = 1
    }
    if (query.category !== 'all') {
        let isCategoryExists = await db.categories.findById(query.category);
        if (!isCategoryExists) {
            throw new Error('invalid category id')
        }
        task = await db.tasks.find({
            userId: query.user,
            categoryId: query.category
        }).sort(sortModel).populate('categoryId')
    } else {
        task = await db.tasks.find({
            userId: query.user
        }).sort(sortModel).populate('categoryId')
    }

    log.end()
    return task
}

const updateStatus = async (id, query, context) => {
    const log = context.logger.start(`services:tasks:update`);

    let istaskExists = await db.tasks.findById(id);
    if (!istaskExists) {
        throw new Error("invalid task");
    }
    if (query.status !== '' && query.status !== 'string' && query.status !== undefined) {
        istaskExists.status = query.status
        if (query.status === 'completed') {
            istaskExists.taskCompletedOn = new Date()
        }
        await istaskExists.save()
    } else {
        throw new Error('task status required')
    }

    log.end();
    return istaskExists
};
const deleteTask = async (id, context) => {
    const log = context.logger.start(`services:tasks:deleteTask`);

    let istaskExists = await db.tasks.findById(id);
    if (!istaskExists) {
        throw new Error("invalid task");
    }
    if (!ObjectId(istaskExists.userId).equals(context.user.id)) {
        throw new Error('Access denied')
    }
    await db.tasks.remove({ '_id': id });
    log.end();
    return istaskExists
};

const taskList = async (query, context) => {

    const log = context.logger.start(`services:tasks:taskList`);
    let sortByDate = query.sortByDate || 'desc'
    let sortByName = query.sortByName || 'desc'
    let queryModel = {
        "userId": query.userId,
    }
    if (query.fromDate !== 'string' && query.fromDate !== undefined) {
        if (query.toDate !== 'string' && query.toDate !== undefined) {
            queryModel.dueDate = {
                '$gte': moment(query.fromDate).startOf('day').toDate(),
                '$lt': moment(query.toDate).endOf('day').toDate()
            }
        } else {
            queryModel.dueDate = {
                '$gte': moment(query.fromDate).startOf('day').toDate()
            }
        }
    }
    if (query.categories !== '' && query.categories !== undefined && query.categories !== null) {
        let categories = [];
        query.categories = query.categories.split(",");
        query.categories.forEach(function (opt) {
            categories.push(ObjectId(opt));
        });
        queryModel['categoryId'] = {
            "$in": categories
        }
    }

    const tasks = await db.tasks.find(queryModel).sort({ createdOn: sortByDate, details: sortByName })
    log.end();
    return tasks;
};

exports.taskList = taskList;
exports.create = create;
exports.get = get;
exports.updateStatus = updateStatus;
exports.update = update;
exports.deleteTask = deleteTask;