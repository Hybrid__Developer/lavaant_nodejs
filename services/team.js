const buildMember = async (model, context) => {
    const { userId, name, title, birthdayDate, hireDate, nickName, phone, sales, anniversaryDate } = model;
    const log = context.logger.start(`services:team:build${model}`);

    let memberModel = {
        userId,
        name,
        title,
        birthdayDate,
        hireDate,
        nickName,
        phone,
        sales,
        anniversaryDate,
        createdOn: new Date(),
        updatedOn: new Date()
    }
    const member = await new db.team(memberModel).save();
    log.end();
    return member;
};


const create = async (model, context) => {
    const log = context.logger.start("services:team:create");

    const member = await buildMember(model, context);

    log.end();
    return member;
};

const getMemberList = async (query, context) => {
    const { userId } = query;
    const log = context.logger.start(`services:team:getMemberList`);

    let teams = await db.team.find({ userId: userId });

    log.end();
    return teams;
};



exports.create = create;
exports.getMemberList = getMemberList;
