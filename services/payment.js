
const mongoose = require("mongoose");
const moment = require("moment");
const uuid = require("uuid");
// var Publishable_Key = 'pk_test_MzEz6Ixn8ZKY8WTEPLQUu3oz00WnBQ4UkY';
// var Secret_Key = 'sk_test_cAj3lKBjWadb14CyhifPeCdb00y2Evjoah';

// let Publishable_Key = 'pk_test_51HWGuZAsGGcPOYa87l6oVAErRu9owDYypYiVYzjPdFNqTr7dh5VJmFzfMONb2ZmvMsceSAOELDvHdR3KxJ0KQXAa00v8YKRxoP'
// let Secret_Key = 'sk_test_51HWGuZAsGGcPOYa8kOciABpD21bWnJLjRlRnNf6mpBcbrrmWWOrkuxLXnehpTuuuJDKv4Kt6McHgaD7im1Xgrms100f4y7God1'


// let Secret_Key = 'sk_test_51HgstJJR8H5wGClYm5aguK2uAtlacMXbpWbSoGcQmiLbP9iSVLaCSO4TnvwgalxsTfabzvsQeWUOuWXuhJnRrt4s00riTE7v25';
// let Secret_Key = 'sk_test_cAj3lKBjWadb14CyhifPeCdb00y2Evjoah'
let Secret_Key = 'sk_test_51HWGuZAsGGcPOYa8kOciABpD21bWnJLjRlRnNf6mpBcbrrmWWOrkuxLXnehpTuuuJDKv4Kt6McHgaD7im1Xgrms100f4y7God1';

const stripe = require('stripe')(Secret_Key);

// create product
const createProduct = async () => {
    const PRODUCT_NAME = "Monthly Subscription";
    const PRODUCT_TYPE = 'service'

    const product = await stripe.products.create({
        name: PRODUCT_NAME,
        type: PRODUCT_TYPE,
    });
    console.log('product, product ====>>>>>>>>>', product);
    return product.id;
}

//create plan
const createPlan = async (productId, model) => {
    const PLAN_NICKNAME = model.plan;
    const PLAN_INTERVAL = "month";
    const CURRENCY = "usd";
    const PLAN_PRICE = model.amount * 100;

    const plan = await stripe.plans.create({
        product: productId,
        nickname: PLAN_NICKNAME,
        currency: CURRENCY,
        interval: PLAN_INTERVAL,
        amount: PLAN_PRICE,
    });

    console.log('plan plan ===>>>>>', plan);
    return plan.id;
}

const payAmount = async (model, context) => {
    const log = context.logger.start("services:payment:payAmount");
    console.log('payment api running', model);


    var fortnightAway = new Date(Date.now() + 12096e5);
    let timestamp = Date.parse(fortnightAway) / 1000;
    // const token
    const product = await createProduct();
    const planId = await createPlan(product, model);
    return stripe.customers.create({
        email: model.customerEmail,
        source: model.stripeToken,
        name: model.userName,
        address: {
            line1: '',
            postal_code: '',
            city: '',
            state: '',
            country: 'USA',
        }
    })

        .then((customer) => {
            return stripe.subscriptions.create({
                customer: customer.id,
                items: [{ plan: planId }],
                trial_end: timestamp,
            });

        })
        .then(async (charge) => {
            console.log('charge==============>>>>>>>>>>>>>>>>>>>>>>', charge);
            // res.send("Success");  // If no error occurs.
            // const paymentDone = await build(charge.id, model, context);
            await db.user.findByIdAndUpdate(model.userId, {
                $set: {
                    amount: model.amount,
                    currentPlan: model.plan,
                    subscriptionId: charge.id
                }
            })

            let buildModel = {
                userId: model.userId,
                userName: model.userName,
                planId: model.planId,
                plan: model.plan,
                amount: model.amount,
                createdOn: new Date(),
                updatedOn: new Date()
            }
            const amountPaid = await new db.payment(buildModel).save();
            console.log('amount paid ===>>', amountPaid);
            log.end();
            return amountPaid;
        })
        .catch((err) => {
            // res.send(err);       // If some error occurs. 
            log.end();
            return err
        });
};


const trialBuild = async (model, context) => {
    const { userId, plan, } = model;
    const log = context.logger.start(`services:plan:build${model}`);
    let expirePlan = moment(new Date()).add(14, 'days').toDate();
    let trialModel = {
        userId: userId,
        plan: plan,
        expirePlan: expirePlan,
        createdOn: new Date(),
        updatedOn: new Date()
    }
    const trialPlan = await new db.payment(trialModel).save();
    log.end();
    return trialPlan;
};

const freeTrial = async (model, context) => {
    const log = context.logger.start("services:payment:freeTrial");
    const plan = await trialBuild(model, context);
    await db.user.findByIdAndUpdate(model.userId, {
        $set: {
            currentPlan: model.plan
        }
    }).then((done) => { console.log('done') })
        .catch((err) => { console.log(err) });
    log.end();
    return plan;
};


const subscriptionHistory = async (query, context) => {
    const { role } = query;
    const log = context.logger.start(`services:payment:subscriptionHistory`);

    let paymentHistory;
    if (role === 'customer') {
        paymentHistory = await db.payment.find();
        log.end();
        return paymentHistory
    }
    if (role === 'vendor') {
        paymentHistory = await db.vendorPayment.find();
        log.end();
        return paymentHistory
    }
    else {
        log.end()
        throw new Error("error in fetching subscription history");
    }
};


// const unsubscribePlan = async (model, context) => {
//     const log = context.logger.start("services:payment:unsubscribePlan");

//     let customerId = model.customerId;
//     let subscriptionId = model.subscriptionId;

//     stripe.customers.cancelSubscription(
//         customerId,
//         subscriptionId,
//     )
//         .then((unsubscribe) => {
//             // res.send("Success");  // If no error occurs.
//             log.end();
//             return 'plan_unsubscribed';
//         })
//         .catch((err) => {
//             // res.send(err);       // If some error occurs. 
//             log.end();
//             return err
//         });
// };


const unsubscribePlan = async (model, context) => {
    const log = context.logger.start("services:payment:unsubscribePlan");

    let subscription_id = model.subscriptionId;

    const subscriptionCancl = await stripe.subscriptions.update(
        subscription_id,
        { cancel_at_period_end: true }
    )
    if (!subscriptionCancl) {
        throw new Error('occured error in cancel subscription')
    }
    else {
        setPlan(model.userId, model, context);
        return subscriptionCancl;
    }
};

exports.payAmount = payAmount;
exports.freeTrial = freeTrial;
exports.subscriptionHistory = subscriptionHistory;
exports.unsubscribePlan = unsubscribePlan;






// const payAmount = async (model, context) => {
//     const log = context.logger.start("services:payment:payAmount");


//     const idempontencyKey = uuid()
//     // Moreover you can take more details from user 
//     // like Address, Name, etc from form 
//     stripe.customers.create({
//         email: model.stripeEmail,
//         source: model.stripeToken,
//     })
//         .then((customer) => {

//             return stripe.charges.create({
//                 amount: model.amount * 100,     // Charging amount 
//                 description: 'Lavaant Machinery Product',
//                 currency: 'USD',         // Currency type
//                 customer: customer.id
//             }, {idempontencyKey});
//         })
//         .then((charge) => {
//             // const amount = build(model, context);
//             console.log('charge===>>>', charge)
//             res.send("Success");  // If no error occurs
//         })
//         .catch((err) => {
//             res.send(err);       // If some error occurs 
//         });
//     log.end();

// };