const mongoose = require("mongoose");
const moment = require('moment');

const date = () => {
    let dat = new Date();

    const d = {
        '$gte': moment(dat, "DD-MM-YYYY").startOf('day').toDate(),
        '$lt': moment(dat, "DD-MM-YYYY").endOf('day').toDate()
    }
    return d;
}

const build = async (model, context) => {
    const log = context.logger.start(`services:phone:build${model}`);
    let buildModel = {
        userId: model.userId,
        type: model.type,
        toPhone: model.toPhone,
        createdOn: new Date(),
        updateOn: new Date()
    }
    const phone = await new db.phone(buildModel).save();
    log.end();
    return phone;
};

const update = async (model, context, d) => {
    const log = context.logger.start(`services:phone:update${model}`);
    const obj = model.toPhone[0];
    const phone = await db.phone.update(
        { userId: model.userId, type: model.type, createdOn: d },
        { $push: { toPhone: obj } },
    );
    log.end();
    return phone;
}

const create = async (model, context) => {
    const log = context.logger.start("services:phone:create");

    const isPhoneScheduleExists = await db.phone.find({ userId: model.userId, type: model.type, createdOn: date() });

    if (isPhoneScheduleExists == null || isPhoneScheduleExists == '') {
        const phone = await build(model, context);
        log.end();
        return phone;
    }
    else {
        const phone = await update(model, context, date());
        log.end();
        return phone;
    }
};


const PhoneAndSmsUpdate = async (model, context) => {
    const { userId, customerId, type, contact, sms } = model;
    const log = context.logger.start("services:phone:PhoneAndSmsUpdate");
    if (type === "string" || type === undefined || type === "") {
        throw new Error("type is required");
    }
    let phoneUsers = await db.phone.findOne({ userId: userId, type: type, createdOn: date() });
    if (!phoneUsers) {
        throw new Error("invalid inPerson");
    }
    for (const phone of phoneUsers.toPhone) {
        if (phone.id === customerId) {
            if (contact !== "" && contact !== "string" && contact !== undefined) {
                phone.contact = contact
            }
            if (sms !== "" && sms !== "string" && sms !== undefined) {
                phone.sms = sms
            }
            await phoneUsers.save()
        }
    }
    log.end();
    return phoneUsers;
};



exports.create = create;
exports.PhoneAndSmsUpdate = PhoneAndSmsUpdate;




