const moment = require('moment');
const build = async (model, context) => {
    const { company, customerName, address, phone, email, note1, note2 } = model;
    const log = context.logger.start(`services:assignLeads:build${model}`);

    let Model = {
        company,
        customerName,
        address,
        phone,
        email,
        note1,
        note2,
        createdOn: new Date(),
        updatedOn: new Date()
    }
    const activity = await new db.assignlead(Model).save();
    log.end();
    return activity;
};


const setAddress = (model, address, context) => {
    const log = context.logger.start("services:assignLeads:set", model);
    if (model.lat !== "string" && model.lat !== undefined && model.lat !== '') {
        address.lat = model.lat;
    }
    if (model.lng !== "string" && model.lng !== undefined && model.lng !== '') {
        address.lng = model.lng;
    }
    if (model.location !== "string" && model.location !== undefined && model.location !== '') {
        address.location = model.location;
    }
    address.updatedOn = new Date();
    log.end();
    address.save();
    return address;
};


const create = async (model, context) => {
    const log = context.logger.start("services:assignLeads:create");

    const activity = await build(model, context);

    log.end();
    return activity;
};


const mapAddress = async (model, context) => {
    const { potentialCusId, lat, lng, location } = model;
    const log = context.logger.start("services:assignLeads:mapAddress");

    let Model = {
        potentialCusId,
        lat,
        lng,
        location,

        createdOn: new Date(),
        updatedOn: new Date()
    }
    const activity = await new db.mapAddress(Model).save();

    log.end();
    return activity;
};


const potentialCustomerList = async (req, context) => {
    const log = context.logger.start("services:assignLeads:potentialCustomerList");
    let customers = await db.assignlead.find({});
    log.end();
    return customers;
}

const deletePotentialCustomer = async (id, context) => {
    const log = context.logger.start(`services:assignLeads:deletePotentialCustomer`);

    let isPotentialCustomerExists = await db.assignlead.findById(id);
    await db.assignlead.remove({ '_id': id });
    log.end();
    return isPotentialCustomerExists
};


const mapAddressUpdate = async (customerId, model, context) => {
    const log = context.logger.start(`services:assignLeads:update`);

    let entity = await db.mapAddress.findOne({ potentialCusId: customerId });
    if (!entity) {
        throw new Error("invalid  address id");
    }
    const address = await setAddress(model, entity, context);
    log.end();
    return address
};

const mapAddressList = async (req, context) => {
    const log = context.logger.start("services:assignLeads:mapAddressList");
    let mapAddresses = await db.mapAddress.find({});
    log.end();
    return mapAddresses;
}

exports.create = create;
exports.mapAddress = mapAddress;
exports.potentialCustomerList = potentialCustomerList;
exports.mapAddressUpdate = mapAddressUpdate;
exports.mapAddressList = mapAddressList;
exports.deletePotentialCustomer = deletePotentialCustomer;