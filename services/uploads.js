const url = require('config').get('image').url
const urlPdf = require('config').get('pdf').url

const encrypt = require("../permit/crypto.js");




const fs = require('fs')
const imageUploader = async (model, params, context) => {
    let log = context.logger.start('services:uploads:uploadImage')

    let uploadImageOf = params.uploadImageOf || 'user'
    let entity
    let picUrl
    if (uploadImageOf === 'user') {
        entity = await db.user.findById(params.id);
    }
    if (uploadImageOf === 'product') {
        entity = await db.products.findById(params.id);
    }
    if (uploadImageOf === 'store') {
        entity = await db.stores.findById(params.id);
    }
    if (!entity) {
        throw new Error(`invalid ${uploadImageOf} Id`);
    } else {
        if (uploadImageOf === 'user') {
            picUrl = entity.profilePic
        } else if (uploadImageOf === 'product') {
            picUrl = entity.image
        } else if (uploadImageOf === 'store') {
            picUrl = entity.logo
        }
    }

    let date = new Date()
    date = date.getTime()
    var filePath = 'assets/images/' + date
    if (picUrl.includes(`${url}`)) {
        picUrl = picUrl.replace(`${url}`, '');
        try {
            fs.unlinkSync(`${picUrl}`)
            console.log('File unlinked!');
        } catch (err) {
            console.log(err)
        }
    }
    if (model.files) {
        if (Object.keys(model.files).length == 0) {
            log.end()
            throw new Error('No files were uploaded.');
        }

        filePath = filePath + model.files.file.name.replace(/\s/g, '')

        model.files.file.mv(filePath, function (err) {
            if (err) {
                log.end()
                throw new Error('No files were uploaded.');
            }
            console.log('File uploaded!');
        });
    }
    var completeURl = url + filePath
    if (uploadImageOf === 'user') {
        entity.profilePic = completeURl
    } else if (uploadImageOf === 'product') {
        entity.image = completeURl
    } else if (uploadImageOf === 'store') {
        entity.logo = completeURl
    }
    await entity.save()
    log.end()
    return entity
}



const pdfUploader = async (model, params, context) => {
    let log = context.logger.start('services:uploads:pdfUploader')

    let uploadPdfOf = params.uploadPdfOf;
    let entity
    let picUrl
    if (uploadPdfOf === 'user') {
        entity = await db.user.findById(params.id);
    }
    if (uploadPdfOf === 'product') {
        entity = await db.products.findById(params.id);
    }
    if (uploadPdfOf === 'store') {
        entity = await db.stores.findById(params.id);
    }
    if (!entity) {
        throw new Error(`invalid ${uploadPdfOf} Id`);
    } else {
        if (uploadPdfOf === 'user') {
            picUrl = entity.pdf;
        } else if (uploadPdfOf === 'product') {
            picUrl = entity.pdf
        } else if (uploadPdfOf === 'store') {
            picUrl = entity.pdf
        }
    }
    let date = new Date()
    date = date.getTime()
    var filePath = 'assets/pdf/' + date
    if (picUrl.includes(`${urlPdf}`)) {
        picUrl = picUrl.replace(`${urlPdf}`, '');
        try {
            fs.unlinkSync(`${picUrl}`)
            console.log('File unlinked!');
        } catch (err) {
            console.log(err)
        }
    }
    if (model.files) {
        if (Object.keys(model.files).length == 0) {
            log.end()
            throw new Error('No files were uploaded.');
        }

        // filePath = filePath + model.files.file.name.replace(/\s/g, '')
        filePath = filePath + model.files.file.name;


        model.files.file.mv(filePath, function (err) {
            if (err) {
                log.end()
                throw new Error('No files were uploaded.');
            }
            console.log('File uploaded!');
        });
    }
    var completeURl = urlPdf + filePath
    if (uploadPdfOf === 'user') {
        entity.pdf = completeURl
    } else if (uploadPdfOf === 'product') {
        entity.pdf = completeURl
    } else if (uploadPdfOf === 'store') {
        entity.pdf = completeURl
    }
    await entity.save()
    log.end()
    return entity
}


const excelUploader = async (model, context) => {
    let log = context.logger.start('services:uploads:excelUploader');
    let date = new Date()
    date = date.getTime()
    let excel = model.excel;
    for (var v = 0; v < excel.length; v++) {
        if (excel[v].firstName == "" || excel[v].lastName == "" || excel[v].email == "" || excel[v].mobileNo == "" || excel[v].password == ""
            || excel[v].role == "" || excel[v].dob == "" || excel[v].anniversary == "") {
            console.log('fields are empty');
            // throw new Error("selected columns should not be empty in excel file");;

        }
        else {
            const isEmail = await db.user.findOne({ email: excel[v].email.toLowerCase() });
            if (isEmail) {
                console.log('is email exist');
            } else {
                const shoper = {};
                shoper.firstName = excel[v].firstName;
                shoper.lastName = excel[v].lastName;
                shoper.email = excel[v].email;
                shoper.mobileNo = excel[v].mobileNo;
                shoper.password = excel[v].password;
                // shoper.password = encrypt.getHash(excel[v].password, context);
                shoper.role = excel[v].role;
                shoper.dob = excel[v].dob;
                shoper.anniversary = excel[v].anniversary;

                const savedUser = await new db.user(shoper).save();
                // console.log('saved user', savedUser);
            }
        }
    }
    log.end()
    return date
}

const productExcelUploader = async (model, context) => {
    let log = context.logger.start('services:uploads:productExcelUploader');
    let date = new Date()
    date = date.getTime()
    let excel = model.excel;
    for (var v = 0; v < excel.length; v++) {
        if (excel[v].name == "" || excel[v].description == "" || excel[v].quantity == "" || excel[v].costPerEach == "" || excel[v].overAllPrice == ""
            || excel[v].note == "" || excel[v].category == "" || excel[v].height == "" || excel[v].width == "" || excel[v].weight == ""
            || excel[v].length == "" || excel[v].subCategory == "" || excel[v].rvDescription == "" || excel[v].manufacturer == "" || excel[v].sku == ""
            || excel[v].uom == "") {
            console.log('fields are empty');
            // throw new Error("selected columns should not be empty in excel file");;
        }
        else {
            const product = {};
            product.name = excel[v].name;
            product.description = excel[v].description;
            product.quantity = excel[v].quantity;
            product.costPerEach = excel[v].costPerEach;
            product.overAllPrice = excel[v].overAllPrice;
            product.note = excel[v].note;
            product.category = excel[v].category;
            product.height = excel[v].height;
            product.width = excel[v].width;
            product.weight = excel[v].weight;
            product.length = excel[v].length;
            product.subCategory = excel[v].subCategory;
            product.rvDescription = excel[v].rvDescription;
            product.manufacturer = excel[v].manufacturer;
            product.sku = excel[v].sku;
            product.uom = excel[v].uom;

            const savedproduct = await new db.products(product).save();
            console.log('saved product', savedproduct);
        }
    }
    log.end()
    return date
}


exports.imageUploader = imageUploader;
exports.pdfUploader = pdfUploader;
exports.excelUploader = excelUploader;
exports.productExcelUploader = productExcelUploader;
