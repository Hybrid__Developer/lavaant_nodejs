const encrypt = require("../permit/crypto.js");
const ObjectId = require("mongodb").ObjectID;

const set = (model, store, context) => {
  const log = context.logger.start("services:store:set");
  if (model.name !== "string" && model.name !== undefined) {
    store.name = model.name;
  }
  if (model.description !== "string" && model.description !== undefined) {
    store.description = model.description;
  }
  if (model.status !== "string" && model.status !== undefined) {
    store.status = model.status;
  }
  if (model.owner !== "string" && model.owner !== undefined) {
    store.owner = model.owner;
  }
  if (model.address !== "string" && model.address !== undefined) {
    store.address = model.address;
  }
  if (model.city !== "string" && model.city !== undefined) {
    store.city = model.city;
  }
  if (model.state !== "string" && model.state !== undefined) {
    store.state = model.state;
  }
  if (model.zipCode !== "string" && model.zipCode !== undefined) {
    store.zipCode = model.zipCode;
  }
  if (model.contactName !== "string" && model.contactName !== undefined) {
    store.contactName = model.contactName;
  }
  if (model.contactNumber !== "string" && model.contactNumber !== undefined) {
    store.contactNumber = model.contactNumber;
  }
  if (model.alternateContactNumber !== "string" && model.alternateContactNumber !== undefined) {
    store.alternateContactNumber = model.alternateContactNumber;
  }
  if (model.area !== "string" && model.area !== undefined) {
    store.area = model.area;
  }
  store.updatedOn = new Date()

  log.end();
  store.save();
  return store;
};

const build = async (model, context) => {
  const log = context.logger.start(`services:store:build${model}`);
  let buildModel = {
    name: model.name.toLowerCase(),
    owner: model.owner,
    email: model.email.toLowerCase(),
    password: model.password,
    address: model.address,
    city: model.city,
    state: model.state,
    zipCode: model.zipCode,
    area: model.area,
    description: model.description,
    contactName: model.contactName,
    contactNumber: model.contactNumber,
    alternateContactNumber: model.alternateContactNumber,
    status: 'active',
    createdOn: new Date(),
    updatedOn: new Date()
  }
  const store = await new db.stores(buildModel).save();
  log.end();
  return store;
};

const create = async (model, context) => {
  const log = context.logger.start("services:store:create");

  const isStoreExists = await db.stores.findOne({
    email: model.email.toLowerCase(),
    status: 'active'
  });
  if (isStoreExists) {
    return {
      err: 'Store already exists'
    }
  }
  model.password = (model.password && model.password !== undefined && model.password !== null && model.password !== 'string') ? encrypt.getHash(
    model.password, context) : encrypt.getHash(model.name.toLowerCase().replace(/\s+/g, '') + '@1234', context)
  const store = await build(model, context);
  log.end();
  return store;
};


const getById = async (id, context) => {
  const log = context.logger.start(`services:store:getById:${id}`);
  const store = await db.stores.findById(id)
  log.end();
  return store;
};

const get = async (query, context) => {
  const log = context.logger.start(`services/store/get:${query}`)
  let store
  if (query === "") {
    store = await db.stores.find({ status: 'active' }).sort({ 'name': 1 })
    return store
  }
  if (typeof query === 'string') {
    if (global.mongo.Types.ObjectId(query)) {
      store = await db.stores.findById(query)
      return store
    }
  }
  if (query.searchString) {
    store = await db.stores.find({
      $or: [
        {
          email: {
            $regex: '^' + query.searchString,
            $options: 'i'
          }
        },
        {
          name: {
            $regex: '^' + query.searchString,
            $options: 'i'
          }
        }], status: query.status || 'active'
    }).sort({ 'name': 1 })
    return store
  }
  if (query.status) {
    store = await db.stores.find({
      status: query.status || 'active'
    }).sort({ 'name': 1 })
    return store
  }
  log.end()
  return null
}

const update = async (id, model, context) => {
  const log = context.logger.start(`services:store:update`);

  let entity = await db.stores.findById(id);
  if (!entity) {
    throw new Error("invalid store");
  }
 
  const store = await set(model, entity, context);
  log.end();
  return store
};
const updateStatus = async (id, query, context) => {
  const log = context.logger.start(`services:tasks:update`);

  let isstoreExists = await db.stores.findById(id);
  if (!isstoreExists) {
    throw new Error("invalid store");
  }
  isstoreExists.status = query.status
  await isstoreExists.save()
  log.end();
  return isstoreExists
};

exports.create = create;
exports.get = get;
exports.update = update;
exports.getById = getById;
exports.updateStatus = updateStatus;