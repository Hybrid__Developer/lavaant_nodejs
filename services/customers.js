const encrypt = require("../permit/crypto.js");
const auth = require("../permit/auth");
const imageUrl = require('config').get('image').url
var nodemailer = require('nodemailer')
var moment = require('moment');

const setUser = async (model, user, context) => {
    const log = context.logger.start("services:users:set");
    if (model.firstName !== "string" && model.firstName !== undefined) {
        user.firstName = model.firstName;
    }
    if (model.lastName !== "string" && model.lastName !== undefined) {
        user.lastName = model.lastName;
    }
    if (model.mobileNo !== "string" && model.mobileNo !== undefined) {
        user.mobileNo = model.mobileNo;
    }
    if (model.alternateMobileNo !== "string" && model.alternateMobileNo !== undefined) {
        user.alternateMobileNo = model.alternateMobileNo;
    }
    // if (model.country !== "string" && model.country !== undefined) {
    //   user.country = model.country;
    // }
    if (model.address !== "string" && model.address !== undefined) {
        user.address = model.address;
    }
    if (model.dob !== "string" && model.dob !== undefined) {
        user.dob = model.dob;
    }
    if (model.customerGroup !== "string" && model.customerGroup !== undefined) {
        user.customerGroup = model.customerGroup;
    }
    if (model.role !== "string" && model.role !== undefined) {
        user.role = model.role;
    }
    if (model.anniversary !== "string" && model.anniversary !== undefined) {
        user.anniversary = model.anniversary;
    }
    if (model.profiePic !== "string" && model.profiePic !== undefined) {
        user.profiePic = model.profiePic;
    }
    if (model.workDetails) {
        let workDetails = model.workDetails
        if (workDetails.companyAddress !== "string" && workDetails.companyAddress !== undefined) {
            user.workDetails.companyAddress = workDetails.companyAddress.toLowerCase();
        }
        if (workDetails.companyName !== "string" && workDetails.companyName !== undefined) {
            user.workDetails.companyName = workDetails.companyName.toLowerCase();
        }
        if (workDetails.companyPhone !== "string" && workDetails.companyPhone !== undefined) {
            user.workDetails.companyPhone = workDetails.companyPhone;
        }
    }
    if (model.otherInformation) {
        let otherInformation = model.otherInformation
        if (otherInformation.likes !== "string" && otherInformation.likes !== undefined) {
            user.otherInformation.likes = otherInformation.likes;
        }
        if (otherInformation.dislikes !== "string" && otherInformation.dislikes !== undefined) {
            user.otherInformation.dislikes = otherInformation.dislikes;
        }
        if (otherInformation.hobbies !== "string" && otherInformation.hobbies !== undefined) {
            user.otherInformation.hobbies = otherInformation.hobbies;
        }
        if (otherInformation.interests !== "string" && otherInformation.interests !== undefined) {
            user.otherInformation.interests = otherInformation.interests;
        }
        if (otherInformation.companionName !== "string" && otherInformation.companionName !== undefined) {
            user.otherInformation.companionName = otherInformation.companionName;
            user.otherInformation.isMarried = true
        }
        if (otherInformation.kids && otherInformation.kids.length) {
            if (otherInformation.kids[0].name !== "string" && otherInformation.kids[0].name !== undefined) {
                user.otherInformation.kids = otherInformation.kids;
                user.otherInformation.hasKids = true
            }
        }
    }
    log.end();
    await user.save();
    return user;
};

const buildUser = async (model, context) => {
    const { userId, firstName, lastName, mobileNo, email, password, anniversary } = model;
    const log = context.logger.start(`services:customers:build${model}`);
    let userModel = {
        userId: userId,
        firstName: firstName.toLowerCase(),
        lastName: lastName.toLowerCase(),
        mobileNo: mobileNo,
        alternateMobileNo: model.alternateMobileNo,
        email: email.toLowerCase(),
        role: 'customer',
        password: password,
        // address: model.address,
        dob: model.dob,
        anniversary: anniversary,
        // country: model.country,
        workDetails: {
            companyName: model.workDetails && model.workDetails.companyName ? model.workDetails.companyName.toLowerCase() : '',
            companyPhone: model.workDetails && model.workDetails.companyPhone ? model.workDetails.companyPhone : '',
            companyAddress: model.workDetails && model.workDetails.companyAddress ? model.workDetails.companyAddress.toLowerCase() : ''
        },
        otherInformation: {
            hobbies: model.otherInformation && model.otherInformation.hobbies ? model.otherInformation.hobbies : '',
            interests: model.otherInformation && model.otherInformation.interests ? model.otherInformation.interests : '',
            isMarried: model.otherInformation && model.otherInformation.isMarried ? model.otherInformation.isMarried : false,
            companionName: model.otherInformation && model.otherInformation.companionName ? model.otherInformation.companionName : '',
            anniversary: model.otherInformation && model.otherInformation.anniversary ? model.otherInformation.anniversary : '',
            hasKids: model.otherInformation && model.otherInformation.hasKids ? model.otherInformation.hasKids : false,
            kids: model.otherInformation.kids
        },
        createdBy: model.createdBy !== "string" && model.createdBy !== undefined ? model.createdBy : 'customer',
        createdOn: new Date(),
        updatedOn: new Date()
    }
    if (model.customerGroup !== 'string' && model.customerGroup !== undefined) {
        userModel.customerGroup = model.customerGroup
    }
    const user = await new db.user(userModel).save();
    log.end();
    return user;
};



const create = async (model, context) => {
    const log = context.logger.start("services:customers:create");
    const isEmail = await db.user.findOne({ email: model.email.toLowerCase() });
    if (isEmail) {
        return {
            err: "Email already resgister"
        }
    }
    model.password = encrypt.getHash(model.password, context);
    model.password = (model.password && model.password !== undefined && model.password !== null && model.password !== 'string') ? model.password : encrypt.getHash(model.firstName.toLowerCase().replace(/\s+/g, '') + '@1234', context)

    const user = await buildUser(model, context);
    if (user.role === 'customer') {
        const favourites = await new db.favourites({
            userId: user.id,
            stores: [
                {
                    id: '5ed0adfa1016710bb8801f23'
                }
            ],
            subCategories: [],
            createdOn: new Date(),
            updatedOn: new Date()
        }).save();
        user.favourites = favourites.id
        user.save()
    }

    log.end();
    return user;
};

const ListByUserId = async (query, context) => {

    const log = context.logger.start(`services:users:ListByUserId`);
    let pageNo = Number(query.pageNo) || 1;
    let pageSize = Number(query.pageSize) || 10;
    let skipCount = pageSize * (pageNo - 1);

    let users = await db.user
        .find({ userId: query.userId }).populate('customerGroup addressId')
        .skip(skipCount)
        .limit(pageSize);
    users.count = await db.user.find({ userId: query.userId }).countDocuments();
    log.end();
    return users;
};

const update = async (model, context) => {
    const log = context.logger.start(`services:users:update`);
    if (!model.userId) {
        throw new Error("user id is required");
    }
    if (!model.customerId) {
        throw new Error("customer id is required");
    }
    let entity = await db.user.findOne({ _id: model.customerId, userId: model.userId, });
    if (!entity) {
        throw new Error("invalid user");
    }
    const user = await setUser(model, entity, context);
    log.end();
    return user
};

const deleteUser = async (model, context) => {
    const log = context.logger.start(`services:users:deleteUser`);
    if (!model.userId) {
        throw new Error("user id is required");
    }
    if (!model.customerId) {
        throw new Error("customer id is required");
    }
    let user = await db.user.findOne({ _id: model.customerId, userId: model.userId, });
    if (!user) {
        throw new Error("user not found");
    }
    await db.user.deleteOne({ _id: model.customerId, userId: model.userId, });

    return 'User Deleted Successfully'

};


const customerExcelUploader = async (model, context) => {
    let log = context.logger.start(`services:users:customerExcelUploader`);
    let date = new Date()
    date = date.getTime()
    let excel = model.excel;
    for (var v = 0; v < excel.length; v++) {
        if (excel[v].firstName == "" || excel[v].lastName == "" || excel[v].email == "" || excel[v].mobileNo == "" || excel[v].password == ""
            || excel[v].role == "" || excel[v].dob == "" || excel[v].anniversary == "") {
            console.log('fields are empty');
            // throw new Error("selected columns should not be empty in excel file");;

        }
        else {
            const isEmail = await db.user.findOne({ email: excel[v].email.toLowerCase() });
            if (isEmail) {
                console.log('is email exist');
            } else {
                const shoper = {};
                shoper.userId = model.userId;
                shoper.firstName = excel[v].firstName;
                shoper.lastName = excel[v].lastName;
                shoper.email = excel[v].email;
                shoper.mobileNo = excel[v].mobileNo;
                shoper.password = excel[v].password;
                shoper.role = 'customer',
                    shoper.dob = excel[v].dob;
                shoper.anniversary = excel[v].anniversary;

                const savedUser = await new db.user(shoper).save();
                // console.log('saved user', savedUser);
            }
        }
    }
    log.end()
    return date
}

exports.create = create;
exports.ListByUserId = ListByUserId;
exports.update = update;
exports.deleteUser = deleteUser;
exports.customerExcelUploader = customerExcelUploader;