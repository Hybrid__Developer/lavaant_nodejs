const ObjectId = require("mongodb").ObjectID;
var moment = require('moment');

const build = async (model, context) => {
  const log = context.logger.start(`services:subscription:build${model}`);
  let buildModel = {
    plan: model.plan,
    user: model.user,
    status: model.status,
    paymentId: model.paymentId,
    expiryDate: model.expiryDate,
    createdOn: new Date(),
    updatedOn: new Date()
  }

  const subscription = await new db.subscriptions(buildModel).save();
  log.end();
  return subscription;
};

const create = async (model, context) => {
  const log = context.logger.start("services:subscription:create");
  const isPlanExists = await db.plans.getById(model.plan);
  const user = await db.user.getById(model.user);
  if (!isPlanExists) {
    throw new Error('Plan not exist')
  }
  if (isPlanExists) {
    throw new Error('Plan is not active for now.')
  }
  if (user.subscription) {
    if (ObjectId(user.subscription).equals(model.plan)) {
      throw new Error('You already bought this plan')
    }
  }
  let expiry
  if (isPlanExists.validity.type === 'days') {
    expiry = moment().add(sPlanExists.validity.timePeriod, 'days');
  }
  if (isPlanExists.validity.type === 'months') {
    expiry = moment().add(sPlanExists.validity.timePeriod, 'months');
  }
  if (isPlanExists.validity.type === 'years') {
    expiry = moment().add(sPlanExists.validity.timePeriod, 'years');
  }
  model.expiryDate = expiry
  const subscription = await build(model, context);
  user.subscription = subscription.id
  await user.save()
  log.end();
  return subscription;
};


const get = async (query, context) => {
  const log = context.logger.start(`services/subscription/get:${query}`)
  let subscription
  if (query === "") {
    subscription = await db.subscriptions.find({ status: 'active' }).sort({ 'createdOn': 1 })
    return subscription
  }
  if (typeof query === 'string') {
    if (global.mongo.Types.ObjectId(query)) {
      subscription = await db.subscriptions.findById(query)
      return subscription
    }
  }
  log.end()
  return null
}

exports.create = create;
exports.get = get;
