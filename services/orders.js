const auth = require("../permit/auth");
const ObjectId = require("mongodb").ObjectID;
var moment = require('moment');

const set = (model, orders, context) => {

    const log = context.logger.start("services:orders:set");


    if (model.firstName) {
        orders.firstName = model.firstName;
    }
    if (model.lastName) {
        orders.lastName = model.lastName;
    }
    if (model.mobileNo) {
        orders.mobileNo = model.mobileNo;
    }
    if (model.country) {
        orders.country = model.country;
    }
    if (model.address) {
        orders.address = model.address;
    }
    if (model.dob) {
        orders.dob = model.dob;
    }
    if (model.anniversary) {
        orders.anniversary = model.anniversary;
    }
    if (model.profiePic) {
        orders.profiePic = model.profiePic;
    }
    log.end();
    order.save();
    return orders;
};

const build = async (model, context) => {

    const log = context.logger.start(`services:orders:build${model}`);
    let orderNo = Date.now()
    let productModel = {
        orderNo: orderNo,
        totalItem: model.totalItem,
        totalAmount: model.totalAmount,
        addressId: model.addressId,
        // products: model.products,
        status: model.status,
        userId: model.userId,
        createdOn: moment(),
        updatedOn: new Date()
    }
    if (model.products.length) {
        productModel.products = model.products
    } else {
        throw new Error('please Add products')
    }
    const order = await new db.order(productModel).save();
    log.end();
    return order;
};


const create = async (model, context) => {

    const log = context.logger.start("services:orders:create");
    const orders = build(model, context);
    log.end();
    return orders;

};

const getByOrderNo = async (orderNo, context) => {
    const log = context.logger.start(`services:orders:getById:${orderNo}`);
    const orders = await db.order.find({ order: orderNo });
    log.end();
    return orders;
};

const orderList = async (query, context) => {

    const log = context.logger.start(`services:orders:get`);
    // let pageNo = Number(query.pageNo) || 1;
    // let pageSize = Number(query.pageSize) || 10;
    // let skipCount = pageSize * (pageNo - 1);
    let sortByDate = query.sortByDate || 'desc'
    let sortByName = query.sortByName || 'desc'
    let queryModel = {
        "userId": query.userId,
    }
    if (query.fromDate !== 'string' && query.fromDate !== undefined) {
        if (query.toDate !== 'string' && query.toDate !== undefined) {
            queryModel.createdOn = {
                '$gte': new Date(query.fromDate),
                '$lte': new Date(query.toDate)
            }
        } else {
            queryModel.createdOn = {
                '$gte': new Date(query.fromDate)
            }
        }
    }
    if (query.subCategories !== '' && query.subCategories !== undefined && query.subCategories !== null) {
        let subCategories = [];
        query.subCategories = query.subCategories.split(",");
        query.subCategories.forEach(function (opt) {
            subCategories.push(new RegExp(opt, "i"));
        });
        queryModel['products.subCategory'] =
        {
            "$in": subCategories
        }
    }
    if (query.products !== '' && query.products !== undefined && query.products !== null) {
        let productName = [];
        query.products = query.products.split(",");
        query.products.forEach(function (opt) {
            productName.push(new RegExp(opt, "i"));
        });
        queryModel['products.name'] =
        {
            "$in": productName
        }
    }
    // const orders = await db.order.aggregate([
    //     {
    //         "$match": queryModel
    //     }
    // ])
    const orders = await db.order
        .find(queryModel).sort({ createdOn: sortByDate, name: sortByName })
    // .skip(skipCount)
    // .limit(pageSize);
    // orders.count = await db.order.find({}).count();
    log.end();
    return orders;
};

const ordersByCategories = async (query, context) => {
    var categories = query.categories.split(",");
    const log = context.logger.start(`services:ordersByCategories:get`);
    let pageNo = Number(query.pageNo) || 1;
    let pageSize = Number(query.pageSize) || 10;
    let skipCount = pageSize * (pageNo - 1);
    const orders = await db.orders
        .find({ category: { "$in": categories } })
        .skip(skipCount)
        .limit(pageSize);
    orders.count = await db.order.find({}).countDocuments();
    log.end();
    return orders;
};

const search = async (query, context) => {
    const log = context.logger.start(`services:ordersByCategories:get`);
    let categories = query.categories.split(",");
    const orders = await db.orders
        .find({ "$and": [{ name: { "$regex": '.*' + query.name + '.*', "$options": 'i' } }, { category: { "$in": categories } }] }).limit(5);
    log.end();
    return orders;
};

const getByStatus = async (query, context) => {
    const log = context.logger.start(`services:orders:getByStatus`);
    let orders = []
    let statusModel = []
    if (query.status === "string" || query.status === undefined || query.status === '') {
        query.status = 'new'
    }
    if (query.status.toLowerCase() === 'new') {
        statusModel = ['waiting for approval']
        orders = await db.order.find({
            status:
                { $in: statusModel }
        }).populate('addressId userId').sort({createdOn: 1});
    } else {
        if (query.status.toLowerCase() === 'current') {
            statusModel = ['approved', 'packed', "ready to ship", 'onhold',]
        }
        if (query.status.toLowerCase() === 'past') {
            statusModel = ['delivered']
        }
        orders = await db.order.find({
            assignedVendor: context.user.id,
            status:
                { $in: statusModel }
        }).populate('addressId userId').sort({createdOn: 1});
    }

    log.end();
    return orders;
};
const updateStatus = async (id, query, context) => {
    const log = context.logger.start(`services:orders:updateStatus`);
    if (query.status === "string" || query.status === undefined || query.status === '') {
        throw new Error('Status is compulsory')
    }
    if (context.user.role === 'customer') {
        throw new Error('Access denied')
    }
    let isOrderExists = await db.order.findById(id);
    if (!isOrderExists) {
        throw new Error("invalid order id");
    }
    if (isOrderExists.status !== 'waiting for approval' && query.status === 'approved') {
        throw new Error("order is already approved and assigned");
    }  
    if (isOrderExists.status === 'packed' && (query.status === 'waiting for approval' || query.status === 'approved')) {
        throw new Error(`order status is ${isOrderExists.status} you cannot change it to ${query.status}`);
    }
    if (isOrderExists.status === 'ready to ship' && (query.status === 'packed' || query.status === 'waiting for approval' || query.status === 'approved')) {
        throw new Error(`order status is ${isOrderExists.status} you cannot change it to ${query.status}`);
    }
    if (isOrderExists.status === 'onhold' && (query.status === 'ready to ship' || query.status === 'packed' || query.status === 'waiting for approval' || query.status === 'approved')) {
        throw new Error(`order status is ${isOrderExists.status} you cannot change it to ${query.status}`);
    }
    if (isOrderExists.status === 'delivered' && (isOrderExists.status === 'onhold' || query.status === 'ready to ship' || query.status === 'packed' || query.status === 'waiting for approval' || query.status === 'approved')) {
        throw new Error(`order status is ${isOrderExists.status} you cannot change it to ${query.status}`);
    }
    isOrderExists.status = query.status
    if (query.status === 'approved') {
        isOrderExists.assignedVendor = context.user.id
    }
    await isOrderExists.save()
    log.end();
    return isOrderExists
};
exports.create = create;
exports.orderList = orderList;
exports.getByOrderNo = getByOrderNo;
exports.ordersByCategories = ordersByCategories;
exports.search = search;
exports.getByStatus = getByStatus;
exports.updateStatus = updateStatus;
