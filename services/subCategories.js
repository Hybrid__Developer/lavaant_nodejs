
const categoryService = require("../services/categories");
const set = (model, subCategory, context) => {
  const log = context.logger.start("services:subCategory:set");
  if (model.name !== "string" && model.name !== undefined) {
    subCategory.name = model.name;
  }
  log.end();
  subCategory.save();
  return subCategory;
};

const build = async (model, context) => {
  const log = context.logger.start(`services:subCategory:build${model}`);
  let buildModel = {
    name: model.name.toLowerCase(),
    categoryId: model.categoryId,
    createdOn: new Date(),
    updatedOn: new Date()
  }

  const subCategory = await new db.subCategories(buildModel).save();
  log.end();
  return subCategory;
};

const create = async (model, context) => {
  const log = context.logger.start("services:subCategory:create");
  let isCategoryExists = await categoryService.get(model.categoryId, context)
  if (!isCategoryExists) {
    throw new Error('categoryId not exist')
  }
  let findCategoryModel = {
    name: model.name.toLowerCase(),
    categoryId: model.categoryId
  }
  const isSubCategoryExists = await db.subCategories.findOne(findCategoryModel);
  if (isSubCategoryExists) {
    return {
      err: 'SubCategory already exists'
    }
  }
  const subCategory = await build(model, context);
  log.end();
  return subCategory;
};


const getById = async (id, context) => {
  const log = context.logger.start(`services:subCategory:getById:${id}`);
  const subCategory = await db.categories.findById(id);
  log.end();
  return subCategory;
};

const get = async (query, context) => {
  const log = context.logger.start(`services/subCategory/get:${query}`)
  let subCategory
  if (query === "") {
    subCategory = await db.categories.find({ categoryOf: '', status: 'active' })
    return subCategory
  }
  if (typeof query === 'string') {
    if (global.mongo.Types.ObjectId(query)) {
      subCategory = await db.categories.findById(query)
      return subCategory
    }
  }
  if (query.categoryId) {
    if (query.categoryId === 'all') {
      subCategory = await db.subCategories.find()
      return subCategory
    }
    else {
      subCategory = await db.subCategories.find({
        categoryId: query.categoryId
      })
      return subCategory
    }
  }
  log.end()
    return null
  }

  const update = async (id, model, context) => {
    const log = context.logger.start(`services:subCategory:update`);

    let entity = await db.subCategories.findById(id);
    if (!entity) {
      throw new Error("invalid subCategory");
    }
    const isCategoryExists = await db.subCategories.findOne({
      name: model.name.toLowerCase(),
      categoryId: entity.categoryId
    });
    if (isCategoryExists) {
      return {
        err: 'Category already exists'
      }
    }
    const subCategory = await set(model, entity, context);
    log.end();
    return subCategory
  };

  exports.create = create;
  exports.get = get;
  exports.update = update;
  exports.getById = getById;
