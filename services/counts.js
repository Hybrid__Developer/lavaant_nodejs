var moment = require('moment');
const getUser = async (role,context) => {
    let log = context.logger.start('services/counts:filter')
    let currentYear = parseInt(moment(new Date()).format('YYYY'))
    let existUser = await db.user.aggregate(
        [{
            $match:
            {
                role: role
            }
        },
        {
            $project: {
                year: { $year: "$createdOn" },
                month: { $month: "$createdOn" },
            }
        },
        {
            $match:
            {
                year: currentYear
            }
        },
        {
            $group: {
                _id: {
                    "year": "$year",
                    "month": "$month"
                },
                count: { $sum: 1 }
            }
        }, {
            $sort: {
                "_id.month": 1,
            }
        }])
    log.end()
    return existUser
}

const userCountByMonth = async (context) => {
    const log = context.logger.start(`services/counts/userCountByMonth`)
    let existVendor = await getUser('customer',context)
    let existCustomer = await getUser('vendor',context)
    if (existVendor.length) {
        log.end()
        return {
            vendor: existVendor,
            customer: existCustomer,
        }
    }
}

exports.userCountByMonth = userCountByMonth
exports.getUser = getUser