const mongoose = require("mongoose");
const moment = require("moment");
const uuid = require("uuid");
// var Publishable_Key = 'pk_test_MzEz6Ixn8ZKY8WTEPLQUu3oz00WnBQ4UkY';
// var Secret_Key = 'sk_test_cAj3lKBjWadb14CyhifPeCdb00y2Evjoah';

// let Publishable_Key = 'pk_test_51HWGuZAsGGcPOYa87l6oVAErRu9owDYypYiVYzjPdFNqTr7dh5VJmFzfMONb2ZmvMsceSAOELDvHdR3KxJ0KQXAa00v8YKRxoP'
// let Secret_Key = 'sk_test_51HWGuZAsGGcPOYa8kOciABpD21bWnJLjRlRnNf6mpBcbrrmWWOrkuxLXnehpTuuuJDKv4Kt6McHgaD7im1Xgrms100f4y7God1'

let Secret_Key = 'sk_test_51HWGuZAsGGcPOYa8kOciABpD21bWnJLjRlRnNf6mpBcbrrmWWOrkuxLXnehpTuuuJDKv4Kt6McHgaD7im1Xgrms100f4y7God1';
// let Secret_Key = 'sk_test_cAj3lKBjWadb14CyhifPeCdb00y2Evjoah'
const stripe = require('stripe')(Secret_Key);

const setPlan = async (cusId, model, context) => {
    const log = context.logger.start(`services:inPerson:build${model}`);
    await db.user.findByIdAndUpdate(model.userId, {
        $set: {
            currentPlan: 'none'
        }
    })
}


// create product
const createProduct = async () => {
    const PRODUCT_NAME = "Monthly Subscription";
    const PRODUCT_TYPE = 'service'

    const product = await stripe.products.create({
        name: PRODUCT_NAME,
        type: PRODUCT_TYPE,
    });
    return product.id;
}

//create plan
const createPlan = async (productId, model) => {
    const PLAN_NICKNAME = model.plan;
    const PLAN_INTERVAL = "month";
    const CURRENCY = "usd";
    const PLAN_PRICE = model.amount * 100;

    const plan = await stripe.plans.create({
        product: productId,
        nickname: PLAN_NICKNAME,
        currency: CURRENCY,
        interval: PLAN_INTERVAL,
        amount: PLAN_PRICE,
    });

    return plan.id;
}

const buySubscription = async (model, context) => {
    console.log('model', model);
    const log = context.logger.start("services:vendorPayment:buySubscription");

    var fortnightAway = new Date(Date.now() + 12096e5);
    let timestamp = Date.parse(fortnightAway) / 1000;
    // const token
    const product = await createProduct();
    const planId = await createPlan(product, model);

    return stripe.customers.create({
        email: model.customerEmail,
        source: model.stripeToken,
        name: model.userName,
        address: {
            line1: '',
            postal_code: '',
            city: '',
            state: '',
            country: 'USA',
        }
    })

        .then((customer) => {
            // const cusId = setCusId(customer.id, model, context);
            return stripe.subscriptions.create({
                customer: customer.id,
                items: [{ plan: planId }],
                trial_end: timestamp,
            });
        })
        .then(async (charge) => {
            // res.send("Success");  // If no error occurs.
            console.log('charge id', charge.id);
            // const paymentDone = build(charge.id, model, context);
            await db.user.findByIdAndUpdate(model.userId, {
                $set: {
                    amount: model.amount,
                    currentPlan: model.plan,
                    subscriptionId: charge.id
                }
            })
            let buildModel = {
                userId: model.userId,
                userName: model.userName,
                planId: model.planId,
                plan: model.plan,
                amount: model.amount,
                createdOn: new Date(),
                updatedOn: new Date()
            }
            const amountPaid = await new db.vendorPayment(buildModel).save();
            log.end();
            return amountPaid;
        })
        .catch((err) => {
            // res.send(err);       // If some error occurs. 
            log.end();
            return err
        });
};


const unsubscribePlan = async (model, context) => {
    const log = context.logger.start("services:vendorPayment:unsubscribePlan");

    let subscription_id = model.subscriptionId;

    const subscriptionCancl = await stripe.subscriptions.update(
        subscription_id,
        { cancel_at_period_end: true }
    )
    if (!subscriptionCancl) {
        throw new Error('occured error in cancel subscription')
    }
    else {
        setPlan(model.userId, model, context);
        return subscriptionCancl;
    }
};

exports.buySubscription = buySubscription;
exports.unsubscribePlan = unsubscribePlan;
