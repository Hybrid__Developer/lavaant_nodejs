const encrypt = require("../permit/crypto.js");
const auth = require("../permit/auth");
const imageUrl = require('config').get('image').url
var nodemailer = require('nodemailer')
var moment = require('moment');

const setUser = async (model, user, context) => {
  const log = context.logger.start("services:users:set");
  if (model.firstName !== "string" && model.firstName !== undefined) {
    user.firstName = model.firstName;
  }
  if (model.lastName !== "string" && model.lastName !== undefined) {
    user.lastName = model.lastName;
  }
  if (model.mobileNo !== "string" && model.mobileNo !== undefined) {
    user.mobileNo = model.mobileNo;
  }
  if (model.alternateMobileNo !== "string" && model.alternateMobileNo !== undefined) {
    user.alternateMobileNo = model.alternateMobileNo;
  }
  // if (model.country !== "string" && model.country !== undefined) {
  //   user.country = model.country;
  // }
  if (model.address !== "string" && model.address !== undefined) {
    user.address = model.address;
  }
  if (model.dob !== "string" && model.dob !== undefined) {
    user.dob = model.dob;
  }
  if (model.customerGroup !== "string" && model.customerGroup !== undefined) {
    user.customerGroup = model.customerGroup;
  }
  if (model.role !== "string" && model.role !== undefined) {
    user.role = model.role;
  }
  if (model.anniversary !== "string" && model.anniversary !== undefined) {
    user.anniversary = model.anniversary;
  }
  if (model.profiePic !== "string" && model.profiePic !== undefined) {
    user.profiePic = model.profiePic;
  }
  if (model.workDetails) {
    let workDetails = model.workDetails
    if (workDetails.companyAddress !== "string" && workDetails.companyAddress !== undefined) {
      user.workDetails.companyAddress = workDetails.companyAddress.toLowerCase();
    }
    if (workDetails.companyName !== "string" && workDetails.companyName !== undefined) {
      user.workDetails.companyName = workDetails.companyName.toLowerCase();
    }
    if (workDetails.companyPhone !== "string" && workDetails.companyPhone !== undefined) {
      user.workDetails.companyPhone = workDetails.companyPhone;
    }
  }
  if (model.otherInformation) {
    let otherInformation = model.otherInformation
    if (otherInformation.likes !== "string" && otherInformation.likes !== undefined) {
      user.otherInformation.likes = otherInformation.likes;
    }
    if (otherInformation.dislikes !== "string" && otherInformation.dislikes !== undefined) {
      user.otherInformation.dislikes = otherInformation.dislikes;
    }
    if (otherInformation.hobbies !== "string" && otherInformation.hobbies !== undefined) {
      user.otherInformation.hobbies = otherInformation.hobbies;
    }
    if (otherInformation.interests !== "string" && otherInformation.interests !== undefined) {
      user.otherInformation.interests = otherInformation.interests;
    }
    if (otherInformation.companionName !== "string" && otherInformation.companionName !== undefined) {
      user.otherInformation.companionName = otherInformation.companionName;
      user.otherInformation.isMarried = true
    }
    if (otherInformation.kids && otherInformation.kids.length) {
      if (otherInformation.kids[0].name !== "string" && otherInformation.kids[0].name !== undefined) {
        user.otherInformation.kids = otherInformation.kids;
        user.otherInformation.hasKids = true
      }
    }
  }
  log.end();
  await user.save();
  return user;
};

const setAddress = (model, address, context) => {
  const log = context.logger.start("services:users:set", model);
  if (model.name !== "string" && model.name !== undefined) {
    address.name = model.name;
  }
  if (model.address !== "string" && model.address !== undefined) {
    address.address = model.address;
  }
  if (model.city !== "string" && model.city !== undefined) {
    address.city = model.city;
  }
  if (model.state !== "string" && model.state !== undefined) {
    address.state = model.state;
  }
  if (model.zipCode !== "string" && model.zipCode !== undefined) {
    address.zipCode = model.zipCode;
  }
  if (model.specialInstruction !== "string" && model.specialInstruction !== undefined) {
    address.specialInstruction = model.specialInstruction;
  }
  if (model.contactName !== "string" && model.contactName !== undefined) {
    address.contactName = model.contactName;
  }
  if (model.contactNumber !== "string" && model.contactNumber !== undefined) {
    address.contactNumber = model.contactNumber;
  }
  if (model.alternateContactNumber !== "string" && model.alternateContactNumber !== undefined) {
    address.alternateContactNumber = model.alternateContactNumber;
  }
  if (model.area !== "string" && model.area !== undefined) {
    address.area = model.area;
  }
  address.updatedOn = new Date()
  log.end();
  address.save();
  return address;
};

const buildUser = async (model, context) => {
  const { firstName, lastName, mobileNo, email, password, role, anniversary } = model;
  const log = context.logger.start(`services:users:build${model}`);
  let userModel = {
    firstName: firstName.toLowerCase(),
    lastName: lastName.toLowerCase(),
    mobileNo: mobileNo,
    alternateMobileNo: model.alternateMobileNo,
    email: email.toLowerCase(),
    role: role || 'customer',
    password: password,
    // address: model.address,
    dob: model.dob,
    anniversary: anniversary,
    // country: model.country,
    workDetails: {
      companyName: model.workDetails && model.workDetails.companyName ? model.workDetails.companyName.toLowerCase() : '',
      companyPhone: model.workDetails && model.workDetails.companyPhone ? model.workDetails.companyPhone : '',
      companyAddress: model.workDetails && model.workDetails.companyAddress ? model.workDetails.companyAddress.toLowerCase() : ''
    },
    otherInformation: {
      hobbies: model.otherInformation && model.otherInformation.hobbies ? model.otherInformation.hobbies : '',
      interests: model.otherInformation && model.otherInformation.interests ? model.otherInformation.interests : '',
      isMarried: model.otherInformation && model.otherInformation.isMarried ? model.otherInformation.isMarried : false,
      companionName: model.otherInformation && model.otherInformation.companionName ? model.otherInformation.companionName : '',
      anniversary: model.otherInformation && model.otherInformation.anniversary ? model.otherInformation.anniversary : '',
      hasKids: model.otherInformation && model.otherInformation.hasKids ? model.otherInformation.hasKids : false,
      kids: model.otherInformation.kids
    },
    createdBy: model.createdBy !== "string" && model.createdBy !== undefined ? model.createdBy : 'customer',
    createdOn: new Date(),
    updatedOn: new Date()
  }
  if (model.customerGroup !== 'string' && model.customerGroup !== undefined) {
    userModel.customerGroup = model.customerGroup
  }
  const user = await new db.user(userModel).save();
  log.end();
  return user;
};

const buildAddress = async (model, context) => {
  const { name, address, city, state, zipCode, specialInstruction, contactName, contactNumber, alternateContactNumber, area, status, userId } = model;
  const log = context.logger.start(`services:users:build${model}`);
  const userAddress = await new db.address({
    name: name,
    address: address,
    city: city,
    state: state,
    zipCode: zipCode,
    specialInstruction: specialInstruction,
    contactName: contactName,
    contactNumber: contactNumber,
    alternateContactNumber: alternateContactNumber,
    area: area,
    userId: userId,
    favouriteSubCategories: model.favouriteSubCategories,
    createdOn: new Date(),
    updatedOn: new Date(),
  }).save();
  log.end();
  return userAddress;
};



const buildTaskUser = async (model, context) => {
  const { email } = model;
  const log = context.logger.start(`services:users:build${model}`);
  let password = encrypt.getHash(model.password, context);
  let userModel = {
    email: email.toLowerCase(),
    role: 'vendor',
    password: password,
    createdOn: new Date(),
    updatedOn: new Date()
  }
  const user = await new db.user(userModel).save();
  log.end();
  return user;
};



const create = async (model, context) => {
  const log = context.logger.start("services:users:create");
  const isEmail = await db.user.findOne({ email: model.email.toLowerCase() });
  if (isEmail) {
    return {
      err: "Email already resgister"
    }
  }
  model.password = encrypt.getHash(model.password, context);
  model.password = (model.password && model.password !== undefined && model.password !== null && model.password !== 'string') ? model.password : encrypt.getHash(model.firstName.toLowerCase().replace(/\s+/g, '') + '@1234', context)

  const user = await buildUser(model, context);
  if (user.role === 'customer') {
    const favourites = await new db.favourites({
      userId: user.id,
      stores: [
        {
          id: '5ed0adfa1016710bb8801f23'
        }
      ],
      subCategories: [],
      createdOn: new Date(),
      updatedOn: new Date()
    }).save();
    user.favourites = favourites.id
    user.save()
  }
  if (model.createdBy === 'vendor') {
    var mailOptions = {
      from: 'Lavaant',
      to: user.email,
      subject: "Welcome Email form Lavaant",
      html: `<body style="background-color: #199bf1"><p style="text-align:center"><em><font size="+1">Welcome... You are registered by Lavaant Sales person.</font></em></p></body>`
    };
    await sendMail(mailOptions)
  }
  log.end();
  return user;
};

const addAddress = async (model, context) => {
  const log = context.logger.start("services:users:addAddress");
  let user = await db.user.findById(model.userId)
  const address = await buildAddress(model, context);
  log.end();
  user.addressId = address.id
  await user.save()
  return address;

};
const addOrRemoveDeviceToken = async (id, model, context) => {
  const log = context.logger.start("services:users:addOrRemoveDeviceToken");
  let entity = await db.user.findById(id);
  if (!entity) {
    throw new Error("invalid user");
  }
  if (entity.id !== context.user.id) {
    throw new Error('Access denied')
  }

  entity.deviceToken = model.deviceToken
  await entity.save()
  log.end();
  return entity;

};
const getAddressById = async (id, context) => {
  const log = context.logger.start(`services:users:getAddressById:${id}`);
  const address = await db.address.find({ userId: id });
  log.end();
  return address;
};

const getById = async (id, context) => {
  const log = context.logger.start(`services:users:getById:${id}`);
  const user = await db.user.findById(id).populate('customerGroup addressId');
  log.end();
  return user;
};

const get = async (query, context) => {
  const log = context.logger.start(`services:users:get`);
  let pageNo = Number(query.pageNo) || 1;
  let pageSize = Number(query.pageSize) || 10;
  let skipCount = pageSize * (pageNo - 1);
  let users
  if (query.role == 'all') {
    users = await db.user
      .find({})
      .skip(skipCount)
      .limit(pageSize);
    users.count = await db.user.find({}).countDocuments();
  }
  else if (query.role) {
    users = await db.user
      .find({ role: query.role }).populate('customerGroup addressId').sort({ 'firstName': 1 })
      .skip(skipCount)
      .limit(pageSize);
    users.count = await db.user.find({ role: query.role }).countDocuments();
  }

  if (query.email) {
    const user = await db.user.findOne({
      email: query.email
    })
    log.end()
    return user
  }
  if (query.otpVerifyToken) {
    const user = await db.user.findOne({
      otpVerifyToken: query.otpVerifyToken,
    })
    log.end()
    return user
  }
  log.end();
  return users;
};

const resetPassword = async (model, context) => {
  const log = context.logger.start(`service/users/resetPassword: ${model}`);
  const user = context.user;
  const isMatched = encrypt.compareHash(
    model.oldPassword,
    user.password,
    context
  );
  if (isMatched) {
    const newPassword = encrypt.getHash(model.newPassword, context);
    user.password = newPassword;
    user.updatedOn = new Date();
    await user.save();
    log.end();
    return "Password Updated Successfully";
  } else {
    log.end();
    throw new Error("Old Password Not Match");
  }
};

const adddressUpdate = async (id, model, context) => {
  const log = context.logger.start(`services:users:update`);

  let entity = await db.address.findById(id);
  if (!entity) {
    throw new Error("invalid  address id");
  }
  let user = await db.user.findById(entity.userId)
  const address = await setAddress(model, entity, context);
  log.end();
  user.addressId = address.id
  await user.save()
  log.end();
  return address
};

const update = async (id, model, context) => {
  const log = context.logger.start(`services:users:update`);
  let populate = context.user.role === 'vendor' ? '' : 'customerGroup addressId'
  let entity = await db.user.findById(id).populate(populate);
  if (!entity) {
    throw new Error("invalid user");
  }

  await setUser(model, entity, context);
  let user = await db.user.findById(id).populate(populate);

  log.end();
  return user
};


const login = async (model, context) => {
  const log = context.logger.start("services:users:login");

  if (model.app == "task") {
    let user = await db.user.findOne({ email: model.email })
    if (!user) {
      const createdUser = await buildTaskUser(model, context);
      const token = auth.getToken(createdUser.id, false, context);
      createdUser.token = token;
      log.end();
      return createdUser;
    }
  }

  const query = {};

  if (model.email) {
    query.email = model.email;
  }

  let user = await db.user.findOne(query).populate('customerGroup addressId');

  if (user) {
    if (user.status == 'inactive') {
      log.end();
      throw new Error("Your account is inactive");
    }
  }

  if (!user) {
    log.end();
    throw new Error("user not found");
  }
  const isMatched = encrypt.compareHash(model.password, user.password, context);

  if (!isMatched) {
    log.end();
    throw new Error("password mismatch");
  }

  const token = auth.getToken(user.id, false, context);
  user.token = token;
  user.updatedOn = new Date();
  user.save();
  log.end();
  return user;
};

const logout = async (model, context) => {
  const log = context.logger.start("services:users:logout");
  await context.user.save();
  log.end();
  return "logout successfully";
};



const buildTaskFBUser = async (model, context) => {
  const { facebookId, firstName, lastName, email } = model;
  const log = context.logger.start(`services:users:build${model}`);

  let emailId
  if (email) {
    emailId = email.toLowerCase();
  }
  let userModel = {
    email: emailId,
    role: 'vendor',
    facebookId: facebookId,
    firstName: firstName,
    lastName: lastName,
    createdOn: new Date(),
    updatedOn: new Date()
  }
  const user = await new db.user(userModel).save();
  log.end();
  return user;
};


const facebookLogin = async (model, context) => {
  const log = context.logger.start("services:users:facebookLogin");

  let user = await db.user.findOne({ facebookId: model.facebookId });
  if (!user) {
    const createdFBUser = await buildTaskFBUser(model, context);
    const token = auth.getToken(createdFBUser.id, false, context);
    createdFBUser.token = token;
    log.end();
    return createdFBUser;
  }
  const token = auth.getToken(user.id, false, context);
  user.token = token;
  user.updatedOn = new Date();
  user.save();
  log.end();
  return user;
};

const forgotPassword = async (model, context) => {
  const log = context.logger.start('services/users/forgotPassword')
  if (!model.otpVerifyToken) {
    log.end()
    throw new Error("otpVerifyToken is required.")
  }
  let user = await get({ otpVerifyToken: model.otpVerifyToken }, context)
  if (!user) {
    log.end()
    throw new Error("otpVerifyToken is wrong or expired.")
  }
  user.password = encrypt.getHash(model.newPassword, context)
  let token = auth.getToken(user.id, false, context)
  user.otpVerifyToken = null
  user.otp = null
  user.otpExpires = null
  user.token = token
  await user.save()
  log.end()
  return "Password changed Succesfully"
}

const otp = async (user, context) => {
  const log = context.logger.start('services/users/otp')
  // first email verifies, i.e email is "registered email" or not
  let otpExpires = moment().add(3, 'm').format("YYYY-MM-DDTHH:mm:ss")
  // four digit otp genration
  var digits = '0123456789';
  let OTP = '';
  for (let i = 0; i < 4; i++) {
    OTP += digits[Math.floor(Math.random() * 10)];
  }

  // email send to registered email
  var mailOptions = {
    from: 'Lavaant',
    to: user.email,
    subject: "One Time Password",
    html: `Your 4 digit One Time Password:<br>${OTP}`
  };

  await sendMail(mailOptions)
  // otp and otpExpires time save to user table
  let otpVerifyToken = auth.getToken(user.id, false, context)
  user.otpVerifyToken = otpVerifyToken
  user.otp = auth.getToken(OTP, false, context)
  user.otpExpires = otpExpires
  user.save()
  log.end()
  return user.otpVerifyToken
}

const otpVerify = async (query, context) => {
  // verify otp
  const log = context.logger.start('services/users/otpVerified')
  let user = await get({ otpVerifyToken: query.otpVerifyToken }, context)
  if (!user) {
    throw new Error("otpVerifyToken is wrong.")
  }
  let date = moment()
  let data
  let isotpExpires = true
  if (query.otp === auth.extractToken(user.otp, context).id) {  // moment(date).diff(user.otpExpires, 'minutes')
    if ((moment(date).diff(user.otpExpires, 'minutes') < 3)) {
      data = {
        isotpExpires: false,
        message: "OTP verified"
      }
      log.end()
      return data
    } else {
      data = {
        isotpExpires: isotpExpires,
        message: "OTP Expire"
      }
      log.end()
      return data
    }
  } else {
    data = {
      isotpExpires: isotpExpires,
      message: "OTP did not match"
    }
    log.end()
    return data
  }
}

const search = async (query, context) => {
  const log = context.logger.start(`services:users:search`);
  let searchModel = {}
  let sortModel = {}
  searchModel = {
    $or: [
      {
        email: {
          $regex: '^' + query.searchString,
          $options: 'i'
        }
      },
      {
        firstName: {
          $regex: '^' + query.searchString,
          $options: 'i'
        }
      }],
    role: query.role || 'customer'
  }
  if (query.sortByName !== '' && query.sortByName !== 'string' && query.sortByName !== undefined) {
    sortModel.firstName = await checkSortingOrder(query.sortByName)
  }
  if (query.sortByShipTo !== '' && query.sortByShipTo !== 'string' && query.sortByShipTo !== undefined) {
    sortModel.address = await checkSortingOrder(query.sortByShipTo)
  }
  if (query.sortByCompany !== '' && query.sortByCompany !== 'string' && query.sortByCompany !== undefined) {
    sortModel.companyName = await checkSortingOrder(query.sortByCompany)
  }
  if (query.sortByArea !== '' && query.sortByArea !== 'string' && query.sortByArea !== undefined) {
    sortModel.area = await checkSortingOrder(query.sortByArea)
  }
  if (Object.keys(sortModel).length == 0) {
    sortModel.firstName = 1
  }
  let user = await db.user.find(searchModel).sort(sortModel).populate('customerGroup addressId');
  log.end();
  return user
};

const sendMail = async (mailOptions) => {
  // smtpTrans
  var smtpTrans = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: `javascript.mspl@gmail.com`,
      pass: `showmydev#$!45`
    }, tls: {
      rejectUnauthorized: false
    }
  });

  let mailSent = await smtpTrans.sendMail(mailOptions)
  if (mailSent) {
    console.log("Message sent: %s", mailSent.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(mailSent));
    return mailSent
  } else {
    log.end()
    return throwFailed(error, 'Unable to send email.');
  }
}
const checkSortingOrder = async (value) => {
  if (value === 'asce') {
    return 1
  }
  if (value === 'desc') {
    return -1
  }
}
const getCompanyList = async (context) => {

  const log = context.logger.start(`services:users:getCompanyList`);
  let companyList = await db.user.aggregate([
    {
      $match: {
        role: {
          $in: ['customer', 'vendor']
        },
        "workDetails.companyName": {
          "$exists": true,
          "$ne": ''
        }
      }
    },
    {
      $group: {
        _id: "$workDetails.companyName",
        companyList: {
          $addToSet: {
            companyName: "$workDetails.companyName",
            companyPhone: "$workDetails.companyPhone",
            companyAddress: "$workDetails.companyAddress",
          }
        },
        users: { $push: "$$ROOT" }
      }
    },
    { $sort: { "_id": 1 } }
  ])

  let res = await companyAddressRes(companyList)
  log.end();
  return res
};
const searchCompanyList = async (query, context) => {

  const log = context.logger.start(`services:users:searchCompanyList`);
  let companyList = await db.user.aggregate([
    {
      $match: {
        role: {
          $in: ['customer', 'vendor']
        },
        'workDetails.companyName': {
          $regex: '^' + query.companyName,
          $options: 'i'
        }
      }
    },
    {
      $group: {
        _id: "$workDetails.companyName",
        companyList: {
          $addToSet: {
            companyName: "$workDetails.companyName",
            companyPhone: "$workDetails.companyPhone",
            companyAddress: "$workDetails.companyAddress",
          }
        },
        users: { $push: "$$ROOT" }
      }
    },
    { $sort: { "_id": 1 } }
  ])

  let res = await companyAddressRes(companyList)
  log.end();
  return res
};
const updateCompanyListAddress = async (query, model, context) => {
  const log = context.logger.start(`services:users:updateCompanyListAddress`);
  let companionName = query.companyName
  let updateAddressModel = {}
  if (model.companyName !== "" && model.companyName !== "string" && model.companyName !== undefined) {
    updateAddressModel['workDetails.companyName'] = model.companyName;
    companionName = model.companyName
  }
  if (model.companyPhone !== "" && model.companyPhone !== "string" && model.companyPhone !== undefined) {
    updateAddressModel['workDetails.companyPhone'] = model.companyPhone;
  }
  if (model.companyAddress !== "" && model.companyAddress !== "string" && model.companyAddress !== undefined) {
    updateAddressModel['workDetails.companyAddress'] = model.companyAddress;
  }
  let a = await db.user.update(
    { 'workDetails.companyName': query.companyName },
    {
      $set: updateAddressModel
    },
    { multi: true })
  let res = await searchCompanyList({
    'companyName': companionName
  }, context)
  log.end();
  return res
};
let companyAddressRes = async (companyList) => {
  let index = 1
  let res = []

  for (const company of companyList) {

    let user = []
    company.users.forEach(entity => {
      user.push({
        id: entity.id,
        firstName: entity.firstName,
        lastName: entity.lastName,
        mobileNo: entity.mobileNo,
        address: entity.address,
        profilePic: entity.profilePic,
      })
    });
    res.push({
      id: index,
      companyName: company.companyList[0].companyName,
      companyPhone: company.companyList[0].companyPhone,
      companyAddress: company.companyList[0].companyAddress,
      users: user
    })
    index++
  }
  return res
}
const status = async (id, model, context) => {
  const log = context.logger.start(`services:users:status`);

  let entity = await db.user.findById(id);
  if (!entity) {
    throw new Error("invalid user");
  }

  db.user.update({ _id: id }, { $set: { status: model.status } }, { new: true }, function (err, result) {
    if (err) {
      return err
    } else {
      log.end();
      return "status is inactive"
    }
  })
};
exports.create = create;
exports.get = get;
exports.login = login;
exports.resetPassword = resetPassword;
exports.update = update;
exports.getById = getById;
exports.logout = logout;
exports.facebookLogin = facebookLogin;
exports.getCompanyList = getCompanyList;
exports.searchCompanyList = searchCompanyList;
exports.addAddress = addAddress;
exports.getAddressById = getAddressById;
exports.addressUpdate = adddressUpdate;
exports.forgotPassword = forgotPassword
exports.otp = otp
exports.otpVerify = otpVerify
exports.search = search
exports.checkSortingOrder = checkSortingOrder
exports.updateCompanyListAddress = updateCompanyListAddress;
exports.companyAddressRes = companyAddressRes;
exports.addOrRemoveDeviceToken = addOrRemoveDeviceToken;
exports.status = status;
