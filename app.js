"use strict";

const express = require("express");
const appConfig = require("config").get("app");
const logger = require("@open-age/logger")("server");
const Http = require("http");
const port = process.env.PORT || appConfig.port || 3000;
var admin = require("firebase-admin");
var serviceAccount = require("./lavaant-firebase.json");
const cors = require('cors');
const multer = require('multer');
var xlsxtojson = require("xlsx-to-json");
// const user = require('./models/user');
var bodyParser = require('body-parser')

const app = express();
var server = Http.createServer(app);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

require('./communication/chat.js').sockets(server);


app.use(bodyParser.json());
//upload excel for users
global.__basedir = __dirname;

// -> Multer Upload Storage
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, __basedir + '/uploads/')
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
  }
});

const upload = multer({ storage: storage });


//
app.use(cors());
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
  // res.header(
  //     'Access-Control-Allow-Headers',
  //     'Origin, X-Requested-With, Content-Type, Accept, x-auth-token'
  // );
  //     Header add Access-Control-Allow-Origin "*"
  // Header add Access-Control-Allow-Methods: "GET,POST,OPTIONS,DELETE,PUT"
  next();
});



// -> Express Upload RestAPIs
app.post('/api/uploadfile', upload.single("uploadfile"), (req, res) => {

  importExcelData2MongoDB(__basedir + '/uploads/' + req.file.filename);
  res.json({
    'msg': 'File uploaded/import successfully!', 'file': req.file
  });
});


async function importExcelData2MongoDB(filePath) {
  console.log('file path', filePath);
  xlsxtojson({
    input: filePath,  // input xls 
    output: "output.json", // output json 
    lowerCaseHeaders: true
  }, async function (err, result) {
    if (err) {
      console.log('error', err);
    }
    if (result) {
      console.log('result =>', result);
      // for (var v = 0; v < result.length; v++) {
      //   const shoper = {};
      //   shoper.firstName = result[v].firstName;
      //   shoper.lastName = result[v].lastName;
      //   shoper.email = result[v].email;
      //   shoper.mobileNo = result[v].mobileNo;
      //   shoper.password = result[v].password;

      //   shoper.dob = result[v].dob;
      //   shoper.anniversary = result[v].anniversary;

      //   const savedUser = await new db.user(shoper).save();
      //   console.log('saved user', savedUser);
      // }
    }
  });
}








const boot = () => {
  const log = logger.start("app:boot");
  log.info(`environment:  ${process.env.NODE_ENV}`);
  log.info("starting server");
  server.listen(port, () => {
    log.info(`listening on port: ${port}`);
    log.end();
  });
};

const init = async () => {
  await require("./settings/database").configure(logger);
  await require("./settings/express").configure(app, logger);
  await require("./settings/routes").configure(app, logger);
  await require('./jobs/taskNotification').schedule(logger)
  app.get('/term&policies', function (req, res) {
    res.sendFile(__dirname + '/templates/term&policies.html');
  });
  boot();
};
init();
