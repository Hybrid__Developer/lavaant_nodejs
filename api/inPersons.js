"use strict";
const inPersonService = require("../services/inPersons");
const response = require("../exchange/response");
const userMapper = require("../mappers/user");
const inPersonMapper = require("../mappers/inPersons");

const create = async (req, res) => {
  const log = req.context.logger.start(`api:inPerson:create`);
  try {
    const inPerson = await inPersonService.create(req.body, req.context);
    if (inPerson.err === null || inPerson.err === undefined) {
      const message = "inPerson Schedule updated Successfully";
      log.end();
      return response.success(res, message, inPersonMapper.toModel(inPerson));
    } else {
      log.end();
      return response.seeother(res, inPerson.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const customerList = async (req, res) => {
  const log = req.context.logger.start(`api:inPerson:getCustomerList`);
  try {
    const users = await inPersonService.getCustomerList(req.query, req.context);
    let message = users.count ? users.count : 0 + " " + "user Got";
    log.end();
    return response.page(
      message,
      res,
      userMapper.toSearchModel(users),
      users.count
    );
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const inPersonVisitorUpdate = async (req, res) => {
  const log = req.context.logger.start(`api:inPerson:inPersonVisitorUpdate`);
  try {
    const inPerson = await inPersonService.inPersonVisitorUpdate(req.body, req.context);
    if (inPerson.err === null || inPerson.err === undefined) {
      const message = "InPerson visitor information Added or updated Successfully";
      log.end();
      return response.success(res, message, inPersonMapper.toModel(inPerson));
    } else {
      log.end();
      return response.seeother(res, inPerson.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};


const scheduledCustomerList = async (req, res) => {
  const log = req.context.logger.start(`api:inPerson:scheduledCustomerList`);
  try {
    const users = await inPersonService.scheduledCustomerList(req.query, req.context);
    // let message = users.count ? users.count : 0 + " " + "user Got";
    let message = "users Got";
    log.end();
    return response.page(
      message,
      res,
      inPersonMapper.toSearchModel(users),
      // users.count
    );
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};


const inPersonVisitorsWMY = async (req, res) => {
  const log = req.context.logger.start(`api:inPerson:inPersonVisitorsWMY`);
  try {
    const users = await inPersonService.inPersonVisitorsWMY(req.query, req.context);
    let message = users.count ? users.count : 0 + " " + "user Got";
    log.end();
    return response.page(
      message,
      res,
      inPersonMapper.toSearchModel(users),
      users.count
    );
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const search = async (req, res) => {
  const log = req.context.logger.start(`api:inPerson:search`)
  try {
    const user = await inPersonService.search(req.query, req.context)
    log.end()
    return response.data(res, userMapper.toSearchModel(user))
  } catch (err) {
    log.error(err)
    log.end()
    return response.failure(res, err.message)
  }
}


exports.create = create;
exports.customerList = customerList;
exports.inPersonVisitorUpdate = inPersonVisitorUpdate;
exports.scheduledCustomerList = scheduledCustomerList;
exports.search = search;
exports.inPersonVisitorsWMY = inPersonVisitorsWMY;

