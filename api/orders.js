"use strict";
const service = require("../services/orders");
const response = require("../exchange/response");
const mapper = require("../mappers/orders");

const create = async (req, res) => {
    const log = req.context.logger.start(`api:orders:create`);
    try {
        const order = await service.create(req.body, req.context);
        const message = "order  Successfully";
        log.end();
        return response.success(res, message, order);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const getByOrderNo = async (req, res) => {
    const log = req.context.logger.start(`api:orders:getById:${req.params.id}`);
    try {
        const order = await service.getByOrderNo(req.params.id, req.context);
        log.end();
        return response.data(res, mapper.toSearchModel(orders));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const orderList = async (req, res) => {
    const log = req.context.logger.start(`api:orders:orderList`);
    try {
        const orders = await service.orderList(req.query, req.context);
        let message = orders.count ? orders.count : 0 + " " + "orders Got";
        log.end();
        return response.page(
            message,
            res,
            mapper.toSearchModel(orders),
            Number(req.query.pageNo) || 1,
            Number(req.query.pageSize) || 10,
            orders.count
        );
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const ordersByCategories = async (req, res) => {
    const log = req.context.logger.start(`api:orders:orderList`);
    try {
        const orders = await service.ordersByCategories(req.query, req.context);
        let message = orders.count ? orders.count : 0 + " " + "orders Got";
        log.end();
        return response.page(
            message,
            res,
            mapper.toSearchModel(orders),
            Number(req.query.pageNo) || 1,
            Number(req.query.pageSize) || 10,
            orders.count
        );
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const search = async (req, res) => {
    const log = req.context.logger.start(`api:orders:search`);
    try {
        const orders = await service.search(req.query, req.context);
        // let message = orders.count ? orders.count : 0 + " " + "orders Got";
        log.end();
        return response.data(
            res,
            mapper.toSearchModel(orders),
        );
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};
const getByStatus = async (req, res) => {
    const log = req.context.logger.start(`api:orders:getByStatus`);
    try {
        const orders = await service.getByStatus(req.query, req.context);
        log.end();
        return response.data(res, mapper.toSearchModel(orders));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};
const updateStatus = async (req, res) => {
    const log = req.context.logger.start(`api:orders:updateStatus:${req.params.id}`);
    try {
      const task = await service.updateStatus(req.params.id, req.query, req.context);
      log.end();
      let message = "Order Status Updated Successfully";
      log.end();
      return response.success(res, message, mapper.toModel(task));
    } catch (err) {
      log.error(err);
      log.end();
      return response.failure(res, err.message);
    }
  };
exports.create = create;
exports.orderList = orderList;
exports.getByStatus = getByStatus;
exports.getByOrderNo = getByOrderNo;
exports.ordersByCategories = ordersByCategories;
exports.search = search
exports.updateStatus = updateStatus;


