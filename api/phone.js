"use strict";
const phoneService = require("../services/phone");
const response = require("../exchange/response");
const userMapper = require("../mappers/user");
const phoneMapper = require("../mappers/phone");


const create = async (req, res) => {
    const log = req.context.logger.start(`api:phone:create`);
    try {
        const phone = await phoneService.create(req.body, req.context);
        if (phone.err === null || phone.err === undefined) {
            const message = "Phone Schedule updated Successfully";
            log.end();
            return response.success(res, message, phoneMapper.toModel(phone));
        } else {
            log.end();
            return response.seeother(res, phone.err, {});
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const PhoneAndSmsUpdate = async (req, res) => {
    const log = req.context.logger.start(`api:phone:PhoneAndSmsUpdate`);
    try {
        const phone = await phoneService.PhoneAndSmsUpdate(req.body, req.context);
        if (phone.err === null || phone.err === undefined) {
            const message = "Phone and sms Added or updated Successfully";
            log.end();
            return response.success(res, message, phoneMapper.toModel(phone));
        } else {
            log.end();
            return response.seeother(res, phone.err, {});
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


exports.create = create;
exports.PhoneAndSmsUpdate = PhoneAndSmsUpdate;
