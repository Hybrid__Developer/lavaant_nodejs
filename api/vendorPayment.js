"use strict";
const vendorService = require("../services/vendorPayment");
const response = require("../exchange/response");
const vendorMapper = require("../mappers/vendorPayment");

const buySubscription = async (req, res) => {
    console.log('buyS')
    const log = req.context.logger.start(`api:vendorPayment:buySubscription`);
    try {
        const payment = await vendorService.buySubscription(req.body, req.context);
        if (payment.err === null || payment.err === undefined) {
            const message = "Subscription is done Successfully";
            log.end();
            return response.success(res, message, vendorMapper.toModel(payment));
        } else {
            log.end();
            return response.seeother(res, payment.err, {});
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


const unsubscribePlan = async (req, res) => {
    const log = req.context.logger.start(`api:payment:unsubscribePlan`);
    try {
        const payment = await vendorService.unsubscribePlan(req.body, req.context);
        if (payment.err === null || payment.err === undefined) {
            const message = "UnSubscription is done Successfully";
            log.end();
            return response.success(res, message);
        } else {
            log.end();
            return response.seeother(res, payment.err, {});
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

exports.buySubscription = buySubscription;
exports.unsubscribePlan = unsubscribePlan;
