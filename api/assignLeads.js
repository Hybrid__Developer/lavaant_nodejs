"use strict";
const leadsService = require("../services/assignLeads");
const response = require("../exchange/response");
const leadsMapper = require("../mappers/assignLeads");
const mapAddressMapper = require("../mappers/mapAddress");


const create = async (req, res) => {
    const log = req.context.logger.start(`api:assignLeads:create`);
    try {
        const leads = await leadsService.create(req.body, req.context);
        const message = "Potential customer added Successfully";
        log.end();
        return response.success(res, message, leadsMapper.toModel(leads));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


const mapAddress = async (req, res) => {
    const log = req.context.logger.start(`api:assignLeads:mapAddress`);
    try {
        const leads = await leadsService.mapAddress(req.body, req.context);
        const message = "Map address is added Successfully";
        log.end();
        return response.success(res, message, mapAddressMapper.toModel(leads));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const potentialCustomerList = async (req, res) => {
    const log = req.context.logger.start(`api:assignLeads:potentialCustomerList`);
    try {
        const customers = await leadsService.potentialCustomerList(req.body, req.context);
        if (customers.err === null || customers.err === undefined) {
            log.end();
            return response.data(res, leadsMapper.toSearchModel(customers));
        } else {
            log.end();
            return response.seeother(res, customers.err, {});
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const deletePotentialCustomer = async (req, res) => {
    const log = req.context.logger.start(`api:assignLeads:deletePotentialCustomer:${req.params.id}`);
    try {
        const task = await leadsService.deletePotentialCustomer(req.params.id, req.context);
        log.end();
        let message = "potential customer deleted Successfully";
        log.end();
        return response.success(res, message,);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const mapAddressUpdate = async (req, res) => {
    const log = req.context.logger.start(`api:assignLeads:mapAddressUpdate:${req.params.id}`);
    try {
        const customer = await leadsService.mapAddressUpdate(req.params.id, req.body, req.context);
        log.end();
        return response.data(res, mapAddressMapper.toModel(customer));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const mapAddressList = async (req, res) => {
    const log = req.context.logger.start(`api:assignLeads:mapAddressList`);
    try {
        const mapAddresses = await leadsService.mapAddressList(req.body, req.context);
        if (mapAddresses.err === null || mapAddresses.err === undefined) {
            log.end();
            return response.data(res, mapAddressMapper.toSearchModel(mapAddresses));
        } else {
            log.end();
            return response.seeother(res, mapAddresses.err, {});
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

exports.create = create;
exports.mapAddress = mapAddress;
exports.potentialCustomerList = potentialCustomerList;
exports.mapAddressUpdate = mapAddressUpdate;
exports.mapAddressList = mapAddressList;
exports.deletePotentialCustomer = deletePotentialCustomer;