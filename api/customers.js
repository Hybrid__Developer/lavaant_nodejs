"use strict";
const customerService = require("../services/customers");
const response = require("../exchange/response");
const userMapper = require("../mappers/user");


const create = async (req, res) => {
    const log = req.context.logger.start(`api:customers:create`);
    try {
        if (!req.body.userId || req.body.userId === "string") {
            const message = "user id is required";
            log.end();
            return response.failure(res, message);
        }
        const user = await customerService.create(req.body, req.context);
        if (user.err === null || user.err === undefined) {
            const message = "customer added Successfully";
            log.end();
            return response.success(res, message, user);
        } else {
            log.end();
            return response.seeother(res, user.err, {});
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


const ListByUserId = async (req, res) => {
    const log = req.context.logger.start(`api:customers:get`);
    try {
        const users = await customerService.ListByUserId(req.query, req.context);
        let message = users.count ? users.count : 0 + " " + "user Got";
        log.end();
        return response.page(
            message,
            res,
            userMapper.toSearchModel(users),
            Number(req.query.pageNo) || 1,
            Number(req.query.pageSize) || 10,
            users.count
        );
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const update = async (req, res) => {
    const log = req.context.logger.start('api:customers:update');
    try {
        const user = await customerService.update(req.body, req.context);
        log.end();
        return response.data(res, userMapper.toModel(user));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const deleteUser = async (req, res) => {
    const log = req.context.logger.start('api:customers:deleteUser');
    try {
        const user = await customerService.deleteUser(req.body, req.context);
        log.end();
        return response.data(res, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const customerExcelUploader = async (req, res) => {
    let log = req.context.logger.start(`api:uploads:customerExcelUploader`)
    try {
        const responseData = await customerService.customerExcelUploader(req.body, req.context)
        const message = "excel File Uploaded Successfully";
        log.end()
        return response.data(res, message, responseData)
    } catch (err) {
        log.error(err)
        log.end()
        response.failure(res, err.message)
    }
}

exports.create = create;
exports.ListByUserId = ListByUserId;
exports.update = update;
exports.deleteUser = deleteUser;
exports.customerExcelUploader = customerExcelUploader;