"use strict";
const paymentService = require("../services/payment");
const response = require("../exchange/response");
const paymentMapper = require("../mappers/payment");

const payAmount = async (req, res) => {
    const log = req.context.logger.start(`api:payment:payAmount`);
    try {
        const payment = await paymentService.payAmount(req.body, req.context);
        if (payment.err === null || payment.err === undefined) {
            const message = "Subscription is done Successfully";
            log.end();
            return response.success(res, message, paymentMapper.toModel(payment));
        } else {
            log.end();
            return response.seeother(res, payment.err, {});
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


const freeTrial = async (req, res) => {
    const log = req.context.logger.start(`api:payment:freeTrial`);
    try {
        const payment = await paymentService.freeTrial(req.body, req.context);
        if (payment.err === null || payment.err === undefined) {
            const message = "Free trial is done Successfully";
            log.end();
            return response.success(res, message, paymentMapper.toModel(payment));
        } else {
            log.end();
            return response.seeother(res, payment.err, {});
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


const subscriptionHistory = async (req, res) => {
    const log = req.context.logger.start(`api:payment:subscriptionHistory`);
    try {
        const subHistory = await paymentService.subscriptionHistory(req.query, req.context);
        log.end();
        return response.success(res, paymentMapper.toSearchModel(subHistory));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


const unsubscribePlan = async (req, res) => {
    const log = req.context.logger.start(`api:payment:unsubscribePlan`);
    try {
        const payment = await paymentService.payAmount(req.body, req.context);
        if (payment.err === null || payment.err === undefined) {
            const message = "UnSubscription is done Successfully";
            log.end();
            return response.success(res, message, paymentMapper.toModel(payment));
        } else {
            log.end();
            return response.seeother(res, payment.err, {});
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

exports.payAmount = payAmount;
exports.freeTrial = freeTrial;
exports.subscriptionHistory = subscriptionHistory;
exports.unsubscribePlan = unsubscribePlan;
