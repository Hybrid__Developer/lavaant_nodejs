"use strict";
const calendarService = require("../services/calendar");
const response = require("../exchange/response");
const calendarMapper = require("../mappers/calendar");


const create = async (req, res) => {
    const log = req.context.logger.start(`api:calendar:create`);
    try {
        const calendar = await calendarService.create(req.body, req.context);
        if (calendar.err === null || calendar.err === undefined) {
            const message = "Event added Successfully";
            log.end();
            return response.success(res, message, calendarMapper.toModel(calendar));
        } else {
            log.end();
            return response.seeother(res, calendar.err, {});
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


const updateEvent = async (req, res) => {
    const log = req.context.logger.start(`api:calendar:updateEvent`);
    try {
        const calendar = await calendarService.updateEvent(req.body, req.context);
        if (calendar.err === null || calendar.err === undefined) {
            const message = "Event updated Successfully";
            log.end();
            return response.success(res, message, calendarMapper.toModel(calendar));
        } else {
            log.end();
            return response.seeother(res, calendar.err, {});
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


const getEventsByUserId = async (req, res) => {

    const log = req.context.logger.start(`api:calendar:getEventsByUserId`);
    try {
        const eventList = await calendarService.getEventsByUserId(req.query, req.context);
        log.end();
        return response.data(res, eventList)
        // return response.success(res, calendarMapper.toModel(eventList));

    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

exports.create = create;
exports.updateEvent = updateEvent;
exports.getEventsByUserId = getEventsByUserId;

