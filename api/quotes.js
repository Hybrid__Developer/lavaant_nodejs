"use strict";
const quoteService = require("../services/quotes");
const response = require("../exchange/response");

const create = async (req, res) => {
  const log = req.context.logger.start(`api:quote:create`);
  try {
    const quote = await quoteService.create(req.body, req.context);
    const message = "quote Added Successfully";
    log.end();
    return response.success(res, message,quote);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const get = async (req, res) => {
  const log = req.context.logger.start(`api:quote:get:userId${req.query.id}`);
  try {
    let quote = await quoteService.get('', req.context);
    log.end();
    return response.data(res, quote);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

exports.create = create;
exports.get = get;

