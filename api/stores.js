"use strict";
const storeService = require("../services/stores");
const response = require("../exchange/response");
const storeMapper = require("../mappers/stores");


const create = async (req, res) => {
  const log = req.context.logger.start(`api:store:create`);
  try {
    if (req.context.user.role !== "admin") {
      throw new Error('Access denied')
    }
    const store = await storeService.create(req.body, req.context);
    if (store.err === null || store.err === undefined) {
      const message = "Store Added Successfully";
      log.end();
      return response.success(res, message, storeMapper.toModel(store));
    } else {
      log.end();
      return response.seeother(res, store.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const update = async (req, res) => {
  const log = req.context.logger.start(`api:store:update:${req.params.id}`);
  try {
    if (req.context.user.role !== "admin") {
      throw new Error('Access denied')
    }
    const store = await storeService.update(req.params.id, req.body, req.context);
    log.end();
    if (store.err === null || store.err === undefined) {
      let message = "store Updated Successfully";
      log.end();
      return response.success(res, message, storeMapper.toModel(store));
    } else {
      log.end();
      return response.seeother(res, store.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const updateStatus = async (req, res) => {
  const log = req.context.logger.start(`api:store:updateStatus:${req.params.id}`);
  try {
    if (req.context.user.role !== "admin") {
      throw new Error('Access denied')
    }
    const store = await storeService.updateStatus(req.params.id, req.query, req.context);
    log.end();
    let message = "Store Status Updated Successfully";
    log.end();
    return response.success(res, message, {});
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const get = async (req, res) => {
  const log = req.context.logger.start(`api:store:get:userId${req.query.id}`);
  try {
    let status = req.query.status !== 'string' && req.query.status !== undefined ? req.query.status : 'active'
    const store = await storeService.get({ status: status }, req.context);
    log.end();
    return response.data(res, storeMapper.toSearchModel(store));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const getById = async (req, res) => {
  const log = req.context.logger.start(`api:store:getById:${req.params.id}`);
  try {
    const store = await storeService.getById(req.params.id, req.context);
    log.end();
    return response.data(res, storeMapper.toModel(store));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const search = async (req, res) => {
  const log = req.context.logger.start(`api:users:search`)
  try {
    let status = req.query.status !== 'string' && req.query.status !== undefined ? req.query.status : 'active'
    const stores = await storeService.get({ searchString: req.query.searchString,status:status }, req.context)
    log.end()
    return response.data(res, storeMapper.toSearchModel(stores))
  } catch (err) {
    log.error(err)
    log.end()
    return response.failure(res, err.message)
  }
}

exports.create = create;
exports.update = update;
exports.get = get;
exports.getById = getById;
exports.updateStatus = updateStatus;
exports.search = search;

