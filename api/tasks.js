"use strict";
const taskService = require("../services/tasks");
const response = require("../exchange/response");
const taskMapper = require("../mappers/tasks");


const create = async(req, res) => {
    const log = req.context.logger.start(`api:task:create`);
    try {
        const task = await taskService.create(req.body, req.context);
        const message = "task Created Successfully";
        log.end();
        return response.success(res, message, taskMapper.toModel(task));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const get = async(req, res) => {
    const log = req.context.logger.start(`api:task:get:userId${req.query.id}`);
    try {
        const task = await taskService.get(req.query, req.context);
        log.end();
        return response.data(res, taskMapper.toSearchModel(task));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};
const update = async(req, res) => {
    const log = req.context.logger.start(`api:task:getById:${req.params.id}`);
    try {
        const task = await taskService.update(req.params.id, req.body, req.context);
        log.end();
        return response.data(res, taskMapper.toModel(task));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};
const updateStatus = async(req, res) => {
    const log = req.context.logger.start(`api:task:update:${req.params.id}`);
    try {
        const task = await taskService.updateStatus(req.params.id, req.query, req.context);
        log.end();
        let message = "task Updated Successfully";
        log.end();
        return response.success(res, message, taskMapper.toModel(task));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};
const deleteTask = async(req, res) => {
    const log = req.context.logger.start(`api:task:deleteTask:${req.params.id}`);
    try {
        const task = await taskService.deleteTask(req.params.id, req.context);
        log.end();
        let message = "task deleted Successfully";
        log.end();
        return response.success(res, message, {});
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const taskList = async(req, res) => {
    const log = req.context.logger.start(`api:tasks:taskList`);
    try {
        const tasks = await taskService.taskList(req.query, req.context);
        let message = tasks.count ? tasks.count : 0 + " " + "tasks Got";
        log.end();
        return response.page(
            message,   
            res,
            taskMapper.toSearchModel(tasks),
            Number(req.query.pageNo) || 1,
            Number(req.query.pageSize) || 10,
            tasks.count
        );
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

exports.taskList = taskList;
exports.create = create;
exports.updateStatus = updateStatus;
exports.get = get;
exports.update = update;
exports.deleteTask = deleteTask;