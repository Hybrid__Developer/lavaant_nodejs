"use strict";
const subCategoryService = require("../services/subCategories");
const response = require("../exchange/response");
const subCategoryMapper = require("../mappers/categories");


const create = async (req, res) => {
  const log = req.context.logger.start(`api:subCategory:create`);
  try {
    const subCategory = await subCategoryService.create(req.body, req.context);
    if (subCategory.err === null || subCategory.err === undefined) {
      const message = "Category Added Successfully";
      log.end();
      return response.success(res, message, subCategoryMapper.toModel(subCategory));
    } else {
      log.end();
      return response.seeother(res, subCategory.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const getByCategory = async (req, res) => {
  const log = req.context.logger.start(`api:subCategory:getByCategory:categoryId${req.query.id}`);
  try {
    let subCategory = []
      subCategory = await subCategoryService.get({
        categoryId: req.query.id
      }, req.context);
    log.end();
    return response.data(res, subCategoryMapper.toSearchModel(subCategory));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
// const getById = async (req, res) => {
//   const log = req.context.logger.start(`api:subCategory:getById:${req.params.id}`);
//   try {
//     const subCategory = await categoryService.get(req.params.id, req.context);
//     log.end();
//     return response.data(res, categoryMapper.toModel(subCategory));
//   } catch (err) {
//     log.error(err);
//     log.end();
//     return response.failure(res, err.message);
//   }
// };
const update = async (req, res) => {
  const log = req.context.logger.start(`api:subCategory:update:${req.params.id}`);
  try {
    const subCategory = await subCategoryService.update(req.params.id, req.body, req.context);
    log.end();
    if (subCategory.err === null || subCategory.err === undefined) {
      let message = "Category Updated Successfully";
      log.end();
      return response.success(res, message, subCategoryMapper.toModel(subCategory));
    } else {
      log.end();
      return response.seeother(res, subCategory.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

exports.create = create;
exports.update = update;
exports.getByCategory = getByCategory;
// exports.getById = getById;

