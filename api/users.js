"use strict";
const userService = require("../services/users");
const response = require("../exchange/response");
const userMapper = require("../mappers/user");
const addressMapper = require("../mappers/address");

const create = async (req, res) => {
  const log = req.context.logger.start(`api:users:create`);
  try {
    const user = await userService.create(req.body, req.context);
    if (user.err === null || user.err === undefined) {
      const message = "User Resgiter Successfully";
      log.end();
      return response.success(res, message, user);
    } else {
      log.end();
      return response.seeother(res, user.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const addAddress = async (req, res) => {
  const log = req.context.logger.start(`api:users:addAddress`);
  try {
    const address = await userService.addAddress(req.body, req.context);
    const message = "Address Added Successfully";
    log.end();
    return response.success(res, message, address);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const addressList = async (req, res) => {
  const log = req.context.logger.start(`api:users:getById:${req.params.id}`);
  try {
    const address = await userService.getAddressById(req.params.id, req.context);
    log.end();
    return response.data(res, addressMapper.toSearchModel(address));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const addressUpdate = async (req, res) => {
  const log = req.context.logger.start(`api:users:addressUpdate:${req.params.id}`);
  try {
    const address = await userService.addressUpdate(req.params.id, req.body, req.context);
    log.end();
    return response.data(res, addressMapper.toModel(address));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const getById = async (req, res) => {
  const log = req.context.logger.start(`api:users:getById:${req.params.id}`);
  try {
    const user = await userService.getById(req.params.id, req.context);
    log.end();
    return response.data(res, userMapper.toModel(user));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const list = async (req, res) => {
  const log = req.context.logger.start(`api:users:get`);
  try {
    const users = await userService.get(req.query, req.context);
    let message = users.count ? users.count : 0 + " " + "user Got";
    log.end();
    return response.page(
      message,
      res,
      userMapper.toSearchModel(users),
      Number(req.query.pageNo) || 1,
      Number(req.query.pageSize) || 10,
      users.count
    );
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const login = async (req, res) => {
  const log = req.context.logger.start("api:users:login");
  try {
    const user = await userService.login(req.body, req.context);

    log.end();
    return response.authorized(res, userMapper.toLoginModel(user), user.token);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const update = async (req, res) => {
  const log = req.context.logger.start(`api:users:update:${req.params.id}`);
  try {
    const user = await userService.update(req.params.id, req.body, req.context);
    log.end();
    return response.data(res, userMapper.toModel(user));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const addOrRemoveDeviceToken = async (req, res) => {
  const log = req.context.logger.start(`api:users:addOrRemoveDeviceToken:${req.params.id}`);
  try {
    const user = await userService.addOrRemoveDeviceToken(req.params.id, req.body, req.context);
    const message = "Device Token Updated Successfully";
    log.end();
    return response.success(res, message, user);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const resetPassword = async (req, res) => {
  const log = req.context.logger.start("api:users:resetPassword");
  try {
    const message = await userService.resetPassword(req.body, req.context);
    log.end();
    return response.success(res, message);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const logout = async (req, res) => {
  const log = req.context.logger.start("api:users:logout");
  try {
    const message = await userService.logout(req.body, req.context);
    log.end();
    return response.success(res, message);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const facebookLogin = async (req, res) => {
  const log = req.context.logger.start("api:users:facebookLogin");
  try {
    const user = await userService.facebookLogin(req.body, req.context);

    log.end();
    return response.authorized(res, userMapper.toLoginModel(user), user.token);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const forgotPassword = async (req, res) => {
  const log = req.context.logger.start("api/users/forgotPassword")
  try {
    const passwordChanged = await userService.forgotPassword(req.body, req.context)
    res.message = passwordChanged
    log.end()
    return response.data(res, {})
  } catch (err) {
    log.error(err.message)
    log.end()
    return response.failure(res, err)
  }
}
const otp = async (req, res) => {
  const log = req.context.logger.start(`api/users/otp`)
  // api to send otp on mail
  try {
    let user = await userService.get({ email: req.body.email }, req.context)
    if (!user) {
      log.end()
      return response.failure(res, "The entered email donot match with registered email")
    }
    const message = await userService.otp(user, req.context)
    res.message = "Please check your email to check otp"
    log.end()
    return response.data(res, { otpVerifyToken: message })
  } catch (err) {
    log.error(err.message)
    log.end()
    return response.failure(res, err)
  }
}

const otpVerify = async (req, res) => {
  const log = req.context.logger.start("api/users/otpVerify")

  try {
    const isOtpVerify = await userService.otpVerify(req.query, req.context)
    if (!isOtpVerify.isotpExpires) {
      res.message = isOtpVerify.message
      log.end()
      return response.data(res, {})
    } else {
      log.error(isOtpVerify.message)
      log.end()
      return response.failure(res, isOtpVerify.message)
    }

  } catch (err) {
    log.error(err.message)
    log.end()
    return response.failure(res, err)
  }
}
const search = async (req, res) => {
  const log = req.context.logger.start(`api:users:search`)
  try {
    const user = await userService.search(req.query, req.context)
    log.end()
    return response.data(res, userMapper.toSearchModel(user))
  } catch (err) {
    log.error(err)
    log.end()
    return response.failure(res, err.message)
  }
}
const getCompanyList = async (req, res) => {

  const log = req.context.logger.start(`api:users:getCompanyList`);
  try {
    const companyList = await userService.getCompanyList(req.context);
    log.end();
    return response.data(res, companyList);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const searchCompanyList = async (req, res) => {
  const log = req.context.logger.start(`api:users:searchCompanyList`);
  try {
    const companyList = await userService.searchCompanyList(req.query, req.context);
    log.end();
    return response.data(res, companyList);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const updateCompanyListAddress = async (req, res) => {
  const log = req.context.logger.start(`api:users:updateCompanyListAddress`);
  try {
    const companyList = await userService.updateCompanyListAddress(req.query, req.body, req.context);
    log.end();
    return response.data(res, companyList);
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const status = async (req, res) => {
  const log = req.context.logger.start(`api:users:status:${req.params.id}`);
  try {
    const user = await userService.status(req.params.id, req.body, req.context);
    const message = 'user login status updated successfully';
    log.end();
    return res.status(200).json({
      isSuccess: true,
      statusCode: 200,
      message: message,
    });
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
exports.create = create;
exports.list = list;
exports.login = login;
exports.update = update;
exports.resetPassword = resetPassword;
exports.getById = getById;
exports.logout = logout;
exports.facebookLogin = facebookLogin;
exports.getCompanyList = getCompanyList;
exports.addAddress = addAddress;
exports.addressList = addressList;
exports.addressUpdate = addressUpdate;
exports.forgotPassword = forgotPassword
exports.otp = otp
exports.otpVerify = otpVerify
exports.search = search
exports.searchCompanyList = searchCompanyList;
exports.updateCompanyListAddress = updateCompanyListAddress;
exports.addOrRemoveDeviceToken = addOrRemoveDeviceToken;
exports.status = status;
