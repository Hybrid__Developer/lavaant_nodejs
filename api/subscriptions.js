"use strict";
const subscriptionService = require("../services/subscriptions");
const response = require("../exchange/response");
const subscriptionMapper = require("../mappers/subscriptions");


const buy = async (req, res) => {
  const log = req.context.logger.start(`api:subscription:buy`);
  try {
    const subscription = await subscriptionService.create(req.body, req.context);
    if (subscription.err === null || subscription.err === undefined) {
      const message = "Subscription Added Successfully";
      log.end();
      return response.success(res, message, subscriptionMapper.toModel(subscription));
    } else {
      log.end();
      return response.seeother(res, subscription.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const get = async (req, res) => {
  const log = req.context.logger.start(`api:subscription:get:userId${req.query.id}`);
  try {
    let subscription = await subscriptionService.get('', req.context);
    log.end();
    return response.data(res, subscriptionMapper.toSearchModel(subscription));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};


exports.buy = buy;
exports.get = get;
// exports.getById = getById;

