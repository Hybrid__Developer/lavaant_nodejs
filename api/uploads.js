'use strict'
const uploaderService = require('../services/uploads');
const response = require('../exchange/response');


const imageUploader = async (req, res) => {
    let log = req.context.logger.start(`api:uploads:imageUploader`)
    try {
        const responseData = await uploaderService.imageUploader(req, req.params, req.context)
        res.message = 'File Uploaded Successfully.'
        log.end()
        return response.data(res, responseData)
    } catch (err) {
        log.error(err)
        log.end()
        response.failure(res, err.message)
    }
}


const pdfUploader = async (req, res) => {
    let log = req.context.logger.start(`api:uploads:pdfUploader`)
    try {
        const responseData = await uploaderService.pdfUploader(req, req.params, req.context)
        res.message = 'PDF File Uploaded Successfully.'
        log.end()
        return response.data(res, responseData)
    } catch (err) {
        log.error(err)
        log.end()
        response.failure(res, err.message)
    }
}

const excelUploader = async (req, res) => {
    let log = req.context.logger.start(`api:uploads:excelUploader`)
    try {
        const responseData = await uploaderService.excelUploader(req.body, req.context)
        const message = "excel File Uploaded Successfully";
        log.end()
        return response.data(res, message, responseData)
    } catch (err) {
        log.error(err)
        log.end()
        response.failure(res, err.message)
    }
}


const productExcelUploader = async (req, res) => {
    let log = req.context.logger.start(`api:uploads:productExcelUploader`)
    try {
        const responseData = await uploaderService.productExcelUploader(req.body, req.context)
        const message = "Product excel File Uploaded Successfully";
        log.end()
        return response.data(res, message, responseData)
    } catch (err) {
        log.error(err)
        log.end()
        response.failure(res, err.message)
    }
}

exports.imageUploader = imageUploader;
exports.pdfUploader = pdfUploader;
exports.excelUploader = excelUploader;
exports.productExcelUploader = productExcelUploader;
