"use strict";
const teamService = require("../services/team");
const response = require("../exchange/response");
const teamMapper = require("../mappers/team");


const create = async (req, res) => {
    const log = req.context.logger.start(`api:team:create`);
    try {
        const team = await teamService.create(req.body, req.context);
        const message = "Member added Successfully";
        log.end();
        return response.success(res, message, teamMapper.toModel(team));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


const getMemberList = async (req, res) => {
    const log = req.context.logger.start(`api:team:getMemberList`);
    try {
        const teamMembers = await teamService.getMemberList(req.query, req.context);
        if (teamMembers.err === null || teamMembers.err === undefined) {
            log.end();
            return response.data(res, teamMapper.toSearchModel(teamMembers));
        } else {
            log.end();
            return response.seeother(res, teamMembers.err, {});
        }
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


exports.create = create;
exports.getMemberList = getMemberList;