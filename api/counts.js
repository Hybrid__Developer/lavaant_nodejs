'use strict'
const countService = require('../services/counts')
const response = require('../exchange/response')
const mapper = require('../mappers/counts')

const monthlyUserCount = async (req, res) => {
    try {
        const log = req.context.logger.start("api/counts/monthlyUserCount")
        const usersCount = await countService.userCountByMonth(req.context)
        log.end()
        return response.data(res, mapper.toModel(usersCount))
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.monthlyUserCount = monthlyUserCount