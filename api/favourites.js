"use strict";
const favouriteService = require("../services/favourites");
const response = require("../exchange/response");
const favouriteMapper = require("../mappers/favourites");

const updateFavouriteSubCategories = async (req, res) => {
  const log = req.context.logger.start(`api:favourites:updateFavouriteSubCategories`);
  try {
    const favourite = await favouriteService.update(req.body, 'subCategory', req.context);
    log.end();
    return response.data(res, favouriteMapper.toModel(favourite));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const updateFavouriteStoreList = async (req, res) => {
  const log = req.context.logger.start(`api:favourites:updateFavouriteStoreList`);
  try {
    const favourite = await favouriteService.update(req.body, 'store', req.context);
    log.end();
    return response.data(res, favouriteMapper.toModel(favourite));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const get = async (req, res) => {
  const log = req.context.logger.start(`api:favourites:get`);
  try {
    const favourite = await favouriteService.get(req.context);
    log.end();
    return response.data(res, favouriteMapper.toModel(favourite));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const getFavouriteSubCategories = async (req, res) => {
  const log = req.context.logger.start(`api:favourites:get`);
  try {
    const favourite = await favouriteService.getFavouriteSubCategories(req.query.categoryId, req.context);
    log.end();
    let categories = favouriteMapper.toProductCSearchModel(favourite.subCategory)
    return response.data(res, { categories: categories, favouritesCount: favourite.favouritesCount });
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const getFavouriteStores = async (req, res) => {
  const log = req.context.logger.start(`api:favourites:get`);
  try {
    const favourite = await favouriteService.getFavouriteStores(req.context);
    log.end();
    return response.data(res, favouriteMapper.toStoreSearchModel(favourite));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

exports.updateFavouriteSubCategories = updateFavouriteSubCategories;
exports.getFavouriteSubCategories = getFavouriteSubCategories;
exports.get = get;
exports.updateFavouriteStoreList = updateFavouriteStoreList;
exports.getFavouriteStores = getFavouriteStores;

