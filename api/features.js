"use strict";
const featureService = require("../services/features");
const response = require("../exchange/response");
const featureMapper = require("../mappers/features");


const create = async (req, res) => {
  const log = req.context.logger.start(`api:feature:create`);
  try {
    const feature = await featureService.create(req.body, req.context);
    if (feature.err === null || feature.err === undefined) {
      const message = "feature Added Successfully";
      log.end();
      return response.success(res, message, featureMapper.toModel(feature));
    } else {
      log.end();
      return response.seeother(res, feature.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const get = async (req, res) => {
  const log = req.context.logger.start(`api:feature:getByRole`);
  try {
    let feature = []
    let status= req.query.status !== 'string' && req.query.status !== undefined ? req.query.status : 'active'
    if (req.query.role !== undefined && req.query.role !== null && req.query.role !== '') {
      feature = await featureService.get({
        role: req.query.role,
        status: status
      }, req.context);
    } else {
      feature = await featureService.get({
      status: status
    }, req.context);
    }
    log.end();
    return response.data(res, featureMapper.toSearchModel(feature));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const getById = async (req, res) => {
  const log = req.context.logger.start(`api:feature:getById:${req.params.id}`);
  try {
    const feature = await featureService.getById(req.params.id, req.context);
    log.end();
    return response.data(res, featureMapper.toModel(feature));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const update = async (req, res) => {
  const log = req.context.logger.start(`api:feature:update:${req.params.id}`);
  try {
    const feature = await featureService.update(req.params.id, req.body, req.context);
    log.end();
    if (feature.err === null || feature.err === undefined) {
      let message = "feature Updated Successfully";
      log.end();
      return response.success(res, message, featureMapper.toModel(feature));
    } else {
      log.end();
      return response.seeother(res, feature.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

exports.create = create;
exports.update = update;
exports.get = get;
exports.getById = getById;

