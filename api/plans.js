"use strict";
const planService = require("../services/plans");
const response = require("../exchange/response");
const planMapper = require("../mappers/plans");


const create = async (req, res) => {
  const log = req.context.logger.start(`api:plan:create`);
  try {
    if (req.context.user.role !== "admin") {
      throw new Error('Access denied')
    }
    const plan = await planService.create(req.body, req.context);
    if (plan.err === null || plan.err === undefined) {
      const message = "Plan Added Successfully";
      log.end();
      return response.success(res, message, planMapper.toModel(plan));
    } else {
      log.end();
      return response.seeother(res, plan.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const get = async (req, res) => {
  const log = req.context.logger.start(`api:plan:get:userId${req.query.id}`);
  try {
    let plan = []
    let status = req.query.status !== 'string' && req.query.status !== undefined ? req.query.status : 'active'
    if (req.query.role !== undefined && req.query.role !== null && req.query.role !== '') {
      plan = await planService.get({
        role: req.query.role,
        status: status
      }, req.context);
    } else {
      plan = await planService.get({ status: status }, req.context);
    }
    log.end();
    return response.data(res, planMapper.toSearchModel(plan));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const getById = async (req, res) => {
  const log = req.context.logger.start(`api:plan:getById:${req.params.id}`);
  try {
    const plan = await planService.getById(req.params.id, req.context);
    log.end();
    return response.data(res, planMapper.toModel(plan));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const update = async (req, res) => {
  const log = req.context.logger.start(`api:plan:update:${req.params.id}`);
  try {
    if (req.context.user.role !== "admin") {
      throw new Error('Access denied')
    }
    const plan = await planService.update(req.params.id, req.body, req.context);
    log.end();
    if (plan.err === null || plan.err === undefined) {
      let message = "Plan Updated Successfully";
      log.end();
      return response.success(res, message, planMapper.toModel(plan));
    } else {
      log.end();
      return response.seeother(res, plan.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const updateStatus = async (req, res) => {
  const log = req.context.logger.start(`api:plan:updateStatus:${req.params.id}`);
  try {
    const task = await planService.updateStatus(req.params.id, req.query, req.context);
    log.end();
    let message = "task Updated Successfully";
    log.end();
    return response.success(res, message, planMapper.toModel(task));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const addFeature = async (req, res) => {
  const log = req.context.logger.start(`api:plan:addFeature`);
  try {
    if (req.context.user.role !== "admin") {
      throw new Error('Access denied')
    }
    const feature = await planService.addFeature(req.body, req.context);
    log.end();
    let message = "Feature Added Successfully To Plan";
    log.end();
    return response.success(res, message, planMapper.toModel(feature));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
const removeFeature = async (req, res) => {
  const log = req.context.logger.start(`api:plan:removeFeature:${req.query}`);
  try {
    const feature = await planService.removeFeature(req.query, req.context);
    log.end();
    let message = "Feature Removed Successfully";
    log.end();
    return response.success(res, message, planMapper.toModel(feature));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
exports.create = create;
exports.update = update;
exports.get = get;
exports.getById = getById;
exports.updateStatus = updateStatus;
exports.addFeature = addFeature;
exports.removeFeature = removeFeature;

