"use strict";
const categoryService = require("../services/categories");
const response = require("../exchange/response");
const categoryMapper = require("../mappers/categories");

const create = async (req, res) => {
  const log = req.context.logger.start(`api:category:create`);
  try {
    const category = await categoryService.create(req.body, req.context);
    if (category.err === null || category.err === undefined) {
      const message = "Category Added Successfully";
      log.end();
      return response.success(res, message, categoryMapper.toModel(category));
    } else {
      log.end();
      return response.seeother(res, category.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

const get = async (req, res) => {
  const log = req.context.logger.start(`api:category:get:userId${req.query.id}`);
  try {
    let category = []
    if (req.query.categoryOf !== undefined && req.query.categoryOf !== null && req.query.categoryOf !== '') {
      category = await categoryService.get({
        userId: req.query.id
      }, req.context);
    } else {
      category = await categoryService.get('', req.context);
    }
    log.end();
    return response.data(res, categoryMapper.toSearchModel(category));
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};
// const getById = async (req, res) => {
//   const log = req.context.logger.start(`api:category:getById:${req.params.id}`);
//   try {
//     const category = await categoryService.get(req.params.id, req.context);
//     log.end();
//     return response.data(res, categoryMapper.toModel(category));
//   } catch (err) {
//     log.error(err);
//     log.end();
//     return response.failure(res, err.message);
//   }
// };
const update = async (req, res) => {
  const log = req.context.logger.start(`api:category:update:${req.params.id}`);
  try {
    const category = await categoryService.update(req.params.id, req.body, req.context);
    log.end();
    if (category.err === null || category.err === undefined) {
      let message = "Category Updated Successfully";
      log.end();
      return response.success(res, message, categoryMapper.toModel(category));
    } else {
      log.end();
      return response.seeother(res, category.err, {});
    }
  } catch (err) {
    log.error(err);
    log.end();
    return response.failure(res, err.message);
  }
};

exports.create = create;
exports.update = update;
exports.get = get;
// exports.getById = getById;

