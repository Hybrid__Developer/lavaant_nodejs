"use strict";
const mongoose = require("mongoose");
const phone = mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user"
    },
    type: {
        type: String, default: "area",
        enum: ["area", "shipTo", 'customer', 'company']
    },
    toPhone: [{
        id: { type: String, default: "" },
        contact: { type: String, default: "" },
        sms: { type: String, default: "" },

        _id: false
    }],
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }
});

mongoose.model("Phone", phone);
module.exports = phone;