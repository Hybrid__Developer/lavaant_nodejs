"use strict";
const mongoose = require("mongoose");
const stores = mongoose.Schema({
    logo: { type: String, default: "" },
    name: { type: String, default: "", required: true },
    owner: { type: String, default: "", required: true },
    address: { type: String, default: "", required: true },
    city: { type: String, default: "", required: true },
    state: { type: String, default: "", required: true },
    zipCode: { type: String, default: "" },
    pdf: { type: String, default: "" },
    description: { type: String, default: "" },
    contactName: { type: String, default: "" },
    contactNumber: { type: String, default: "" },
    alternateContactNumber: { type: String, default: "" },
    email: { type: String, default: "", required: true },
    password: { type: String, default: "", required: true },
    area: { type: String, default: "" },
    otp: { type: String, default: "" },
    deviceToken: { type: String, default: "" },
    otpExpires: { type: Date, default: "" },
    otpVerifyToken: { type: String, default: "" },
    status: {
        type: String, default: "active",
        enum: ["active", "inactive"]
    },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }
});

mongoose.model("stores", stores);
module.exports = stores;
