"use strict";
const mongoose = require("mongoose");
const mapAddress = mongoose.Schema({
    potentialCusId: { type: String, default: "", required: true },
    lat: { type: String, default: "", required: true },
    lng: { type: String, default: "", required: true },
    location: { type: String, default: "", required: true },

    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }
});

mongoose.model("mapAddress", mapAddress);
module.exports = mapAddress;