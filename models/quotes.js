'use strict'
var mongoose = require('mongoose')

const quotes = mongoose.Schema({
    title: {
        type: String
    },
    writter: {
        type: String
    },
    status: {
        type: String,
        default: 'active',
        enum: ['active', 'inactive']
    },
    createdOn: { type: Date, default: Date.now }
})
module.exports = quotes;

