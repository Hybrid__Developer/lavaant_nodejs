'use strict'
const mongoose = require('mongoose')

const subCategories = mongoose.Schema({
  name: { type: String, default: "", required: true },
  status: {
    type: String,
    default: 'active',
    enum: ['active', 'inactive']
},
  categoryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "categories"
},
  createdOn: { type: Date, default: Date.now },
  updatedOn: { type: Date, default: Date.now },

});

module.exports = subCategories;