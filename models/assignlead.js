"use strict";
const mongoose = require("mongoose");
const assignlead = mongoose.Schema({
    company: { type: String, default: "", required: true },
    customerName: { type: String, default: "", required: true },
    address: { type: String, default: "", required: true },
    phone: { type: String, default: "", required: true },
    email: { type: String, default: "", required: true },
    note1: { type: String, default: "" },
    note2: { type: String, default: "" },

    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }
});

mongoose.model("Assignlead", assignlead);
module.exports = assignlead;
