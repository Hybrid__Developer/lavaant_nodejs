'use strict'
var mongoose = require('mongoose')

const subscriptions = mongoose.Schema({
    paymentId: { type: String, default: "", required: true },
    plan: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "plans"
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    status: {
        type: String, default: "active",
        enum: ["active", "inactive"]
    },
    expiryDate: { type: Date, default: Date.now },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }
})
module.exports = subscriptions;

