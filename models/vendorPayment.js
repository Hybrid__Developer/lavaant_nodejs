"use strict";
const mongoose = require("mongoose");
const vendorPayment = mongoose.Schema({

    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    planId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "plans"
    },
    plan: { type: String, default: "", },
    expirePlan: { type: Date, default: "" },
    // currentPlan: { type: String, default: "", required: true },
    amount: { type: Number, default: "", },
    // cardNo: { type: Number, default: "", required: true },
    // customerEmail: { type: String, default: "", }
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }

});

mongoose.model("vendorPayment", vendorPayment);
module.exports = vendorPayment;
