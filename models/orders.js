
"use strict";
const mongoose = require("mongoose");

const order = mongoose.Schema({
    orderNo: { type: String, default: "", required: true },
    totalItem: { type: String, default: "", required: true },
    totalAmount: { type: Number, default: "", required: true },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true
    },
    status: {
        type: String, default: "waiting for approval",
        enum: ['waiting for approval', "approved", "packed", "ready to ship", 'onhold', "cancelled","delivered", "rejected"]
    },
    addressId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "address"
    },
    products: [
        {
            quantity: { type: Number, default: "" },
            price: { type: Number, default: "" },
            id: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "product"
            },
            name: { type: String, default: "" },
            subCategory: { type: String, default: "" },
            category: { type: String, default: "" },
            _id: false
        },
    ],
    assignedVendor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user"
    },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }
});
mongoose.model("order", order);
module.exports = order;