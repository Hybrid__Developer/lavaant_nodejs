'use strict'
var mongoose = require('mongoose')

const features = mongoose.Schema({
    name: {
        type: String
    },
    description: {
        type: String
    },
    status: {
        type: String,
        default: 'active',
        enum: ['active', 'inactive']
    },
    role: {
        type: String,
        default: "customer",
        enum: ["customer", "vendor", "store", "admin"]
    },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }

})
module.exports = features;

