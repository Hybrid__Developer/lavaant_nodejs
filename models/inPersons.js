"use strict";
const mongoose = require("mongoose");
const inPersons = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user"
  },
  type: {
    type: String, default: "area",
    enum: ["area", "shipTo", 'customer', 'company']
  },
  toVisit: [{
    id: { type: String, default: "" },
    contact: { type: String, default: "" },
    note1: { type: String, default: "" },
    note2: { type: String, default: "" },
    wRecord: { type: Boolean, default: false },
    recordNotes: { type: String, default: "" },
    recordContact: {
      firstName: { type: String, default: "" },
      lastName: { type: String, default: "" },
      phoneNo: { type: String, default: "" },
    },
    recordTaskReminder: {
      reminderText: { type: String, default: "" },
      reminderNote: { type: String, default: "" },
      fromDate: { type: String, default: "" },
      toDate: { type: String, default: "" },
    },
    _id: false
  }],
  createdOn: { type: Date, default: Date.now },
  updatedOn: { type: Date, default: Date.now }
});

mongoose.model("inPersons", inPersons);
module.exports = inPersons;