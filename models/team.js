"use strict";
const mongoose = require("mongoose");
const team = mongoose.Schema({
    userId: { type: String, default: "", required: true },
    name: { type: String, default: "", required: true },
    title: { type: String, default: "", required: true },
    birthdayDate: { type: String, default: "", required: true },
    hireDate: { type: String, default: "", required: true },
    nickName: { type: String, default: "", required: true },
    phone: { type: String, default: "", required: true },
    sales: { type: String, default: "", required: true },
    anniversaryDate: { type: String, default: "", required: true },


    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }
});

mongoose.model("Team", team);
module.exports = team;