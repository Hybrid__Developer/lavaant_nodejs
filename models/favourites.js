'use strict'
var mongoose = require('mongoose')

const favourites = mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user"
    },
    stores: [{
        id:{
            type: mongoose.Schema.Types.ObjectId,
            ref: "stores"
        },
        _id: false
    }
    ],
    subCategories: [{
        id :{
            type: mongoose.Schema.Types.ObjectId,
            ref: "subCategories"
        },
        _id: false
    }
    ],
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }
})
module.exports = favourites;

