"use strict";
const mongoose = require("mongoose");
const user = mongoose.Schema({
  userId: { type: String, default: "", },
  firstName: { type: String, default: "", },
  lastName: { type: String, default: "", },
  email: { type: String, default: "", },
  password: { type: String, default: "", },
  mobileNo: { type: String, default: "" },
  facebookId: { type: String, default: "" },
  alternateMobileNo: { type: String, default: "" },
  profilePic: { type: String, default: "" },
  pdf: { type: String, default: "" },
  stripeCustomerId: { type: String, default: "none" },
  amount: { type: String, default: "none" },
  currentPlan: { type: String, default: "none" },
  subscriptionId: { type: String, default: "none" },
  address: { type: String, default: "" }, // Address in profile
  anniversary: { type: String, default: "" },
  addressId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "address"
  },
  dob: { type: String, default: "" },
  isShow: {
    type: Boolean,
    default: false
  },
  country: { type: String, default: "" },
  customerGroup: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "categories"
  },
  status: {
    type: String, default: "active",
    enum: ["active", "inactive"]
  },
  workDetails: {
    companyName: { type: String, default: "" },
    companyPhone: { type: String, default: "" },
    companyAddress: { type: String, default: "" },
  },
  role: {
    type: String,
    default: "customer",
    enum: ["customer", "vendor", "store", "admin"]
  },
  otherInformation: {
    likes: { type: String, default: "" },
    dislikes: { type: String, default: "" },
    hobbies: { type: String, default: "" },
    interests: { type: String, default: "" },
    isMarried: { type: Boolean, default: false },
    companionName: { type: String, default: "" },
    hasKids: { type: Boolean, default: false },
    kids: [{
      name: { type: String, default: '' },
      birthdate: { type: String, default: "" },
    }],
  },
  favourites: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "favourites"
  },
  subscription: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "subscriptions"
  },
  otp: { type: String, default: "" },
  deviceToken: { type: String, default: "" },
  otpExpires: { type: Date, default: "" },
  otpVerifyToken: { type: String, default: "" },
  createdBy: { type: String, default: "" },
  createdOn: { type: Date, default: Date.now },
  updatedOn: { type: Date, default: Date.now }
});

mongoose.model("User", user);
module.exports = user;
// module.exports = mongoose.model('User', UserSchema)