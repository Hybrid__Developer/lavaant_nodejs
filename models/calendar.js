"use strict";
const mongoose = require("mongoose");
const calendar = mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user"
    },
    start: { type: String, default: "" },
    name: { type: String, default: "" },
    description: { type: String, default: "" },
    startTime: { type: String, default: "" },
    endTime: { type: String, default: "" },
    duration: { type: String, default: "" },

    reminder: { type: Boolean, default: false },

    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }
});

mongoose.model("calendar", calendar);
module.exports = calendar;

