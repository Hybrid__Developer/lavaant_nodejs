'use strict'
const mongoose = require('mongoose')

const categories = mongoose.Schema({
  name: { type: String, default: "", required: true },
  categoryOf: { type: String, default: "" },
  status: {
    type: String,
    default: 'active',
    enum: ['active', 'inactive']
},
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user"
},
  createdOn: { type: Date, default: Date.now },
  updatedOn: { type: Date, default: Date.now },

});

module.exports = categories;