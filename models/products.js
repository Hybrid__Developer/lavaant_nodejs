"use strict";
const mongoose = require("mongoose");

const products = mongoose.Schema({
    name: { type: String, default: "", required: true },
    description: { type: String, default: "", required: true },
    quantity: { type: Number, default: 0, required: true },
    costPerEach: { type: String, default: "" },
    overAllPrice: { type: String, default: "" },
    image: { type: String, default: "" },
    pdf: { type: String, default: "" },
    note: { type: String, default: "" },
    category: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "categories"
        },
        name: { type: String, default: "" }
    },
    subCategory: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "subCategories"
        },
        name: { type: String, default: "" }
    },
    height: { type: String, default: "" },
    width: { type: String, default: "" },
    weight: { type: String, default: "" },
    rvDescription: { type: String, default: "" },
    length: { type: String, default: "" },
    manufacturer: { type: String, default: "" },
    sku: { type: String, required: true },
    uom: { type: String, default: "" },
    status: {
        type: String, default: "active",
        enum: ["active", "inActive", "out of stock"]
    },
    assignedVendors: [{
        storeId: {
            type: String, default: ""
        }
    }],
    variation: {
        items: [
            {
                type: { type: String },
                price: { type: Number },
            },
        ]
    },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }
});

mongoose.model("products", products);
module.exports = products;
