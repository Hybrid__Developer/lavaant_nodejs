'use strict'
var mongoose = require('mongoose')

const plans = mongoose.Schema({
    title: { type: String, default: "", required: true },
    description: { type: String, default: "", required: true },
    status: {
        type: String, default: "active",
        enum: ["active", "inactive"]
    },
    role: {
        type: String,
        default: "customer",
        enum: ["customer", "vendor", "store", "admin"]
    },
    price: { type: String, default: "", required: true },
    price1: { type: String, default: "" },
    price2: { type: String, default: "" },
    discountedPrice: { type: String, default: "" },
    features: [{
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "features"
        },
        _id: false
    }
    ],
    validity: {
        type: {
            type: String,
            default: "days",
            enum: ["days", "months", "years",]
        },
        timePeriod: {
            type: Number, default: 1,
        }

    },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }
})
module.exports = plans;



