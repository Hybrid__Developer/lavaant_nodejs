'use strict'
const mongoose = require('mongoose')

const tasks = mongoose.Schema({
  details: { type: String, default: "", required: true },
  dueDate: { type: String, default: '' },
  status: {
    type: String,
    default: 'pending',
    enum: ['pending', 'paused', 'completed']
  },
  reminder: {
    type: String,
    default: '1day',
    enum: ['1day', '2day', '1week', '1month']
  },
  fromDate: { type: String, default: "" },
  toDate: { type: String, default: "" },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user"
  },
  categoryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "categories"
  },
  createdOn: { type: Date, default: Date.now },
  taskCompletedOn: { type: Date },
  updatedOn: { type: Date, default: Date.now },

});

module.exports = tasks;