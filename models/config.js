'use strict'
var mongoose = require('mongoose')

const config = mongoose.Schema({
    quoteOfDay: {
        type: String
    },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
    quoteUpdatedOn: { type: Date, default: Date.now }
})
module.exports = config;

