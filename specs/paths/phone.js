module.exports = [
    {
        url: "/create",
        post: {
            summary: "create and update phone schedule list",
            description: "create",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of phone list creation here, type= 'area', 'shipTo', 'customer', 'company' has these values",
                    required: true,
                    schema: {
                        $ref: "#/definitions/addphoneSchedule"
                    }
                }, {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: "/customerList",
        get: {
            summary: "get User List",
            description: "get All Users",
            parameters: [
                {
                    in: "query",
                    name: "userId",
                    description: "user group id user belongs to.",
                    required: true,
                    type: "string"
                },
                {
                    in: "query",
                    name: "type",
                    description: "type can be area, shipTo, customer, company",
                    required: true,
                    type: "string"
                },
                {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: "/phoneAndSmsUpdate",
        put: {
            summary: "add and update Phone calls and sms details",
            description: "update",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of Phone calls and sms",
                    required: true,
                    schema: {
                        $ref: "#/definitions/phoneAndSmsUpdate"
                    }
                }, {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    }



];



