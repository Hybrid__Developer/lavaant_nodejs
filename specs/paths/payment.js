module.exports = [

    {
        url: "/payAmount",
        post: {
            summary: "Pay Amount",
            description: "Pay amount for plan",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of payment amount",
                    required: true,
                    schema: {
                        $ref: "#/definitions/payPayment"
                    }
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },


    {
        url: "/freeTrial",
        post: {
            summary: "Free Trial",
            description: "Free trial for 14 days",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of Free trial plans",
                    required: true,
                    schema: {
                        $ref: "#/definitions/freeTrial"
                    }
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },


    {
        url: "/subscriptionHistory",
        get: {
            summary: "get history of subscription",
            description: "get history of users subscription plans",
            parameters: [
                {
                    in: "query",
                    name: "role",
                    description: "role can be customer or vendor",
                    required: true,
                    type: "string"
                },
                {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: "/unsubscribePlan",
        post: {
            summary: "unsubscribe plan",
            description: "unsubscribe user's plan",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of unsubscribe plan",
                    required: true,
                    schema: {
                        $ref: "#/definitions/unsubscribePlan"
                    }
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    }
]