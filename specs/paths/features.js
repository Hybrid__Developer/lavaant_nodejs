module.exports = [
  {
    url: "/",
    post: {
      summary: "create",
      description: "create feature",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of feature creation.",
          required: true,
          schema: {
            $ref: "#/definitions/createFeature"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    },
    get: {
      summary: "get feature List",
      description: "get All Features",
      parameters: [
        {
          in: "query",
          type: "integer",
          name: "role",
          description: "role= 'customer','vendor','admin','store' default= '' ",
          required: false
        },
        {
          in: "query",
          type: "integer",
          name: "status",
          description: "status= 'active','inactive'",
          default:'active',
          required: false
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }

      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/{id}",
    put: {
      summary: "Update",
      description: "update feature",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "feature id",
          required: true,
          type: "string"
        },
        {
          in: "body",
          name: "body",
          description: "Model of feature update(status will be 'active','inactive'",
          required: true,
          schema: {
            $ref: "#/definitions/updateFeature"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/getById/{id}",
    get: {
      summary: "get feature",
      description: "get feature detail by Id",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "featureId",
          required: true,
          type: "string"
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
];
