module.exports = [
  {
    url: "/",
    post: {
      summary: "create",
      description: "create quote",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of quote creation. 'fill quoteOf if there is any specific type of quote. (for ex. quoteOf: 'task')",
          required: true,
          schema: {
            $ref: "#/definitions/quoteCreate"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    },
    get: {
      summary: "get quote of the day",
      description: "get quote of the day",
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
];
