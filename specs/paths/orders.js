module.exports = [
    {
        url: "/placeOrder",
        post: {
            summary: "place Order",
            description: "place Order",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of place Order creation",
                    required: true,
                    schema: {
                        $ref: "#/definitions/orderCreate"
                    }
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/list",
        get: {
            summary: "get order List",
            description: "get all order",
            parameters: [
                // {
                //     in: "query",
                //     type: "integer",
                //     name: "pageNo",
                //     description: "pageNo",
                //     required: true
                // },
                // {
                //     in: "query",
                //     type: "integer",
                //     name: "pageSize",
                //     description: "pageSize",
                //     required: true
                // },
                {
                    in: "query",
                    type: "string",
                    name: "userId",
                    description: "userId",
                    required: true
                },
                {
                    in: "query",
                    type: "string",
                    name: "sortByName",
                    default: 'desc',
                    description: "sortByName",
                    required: false
                },
                {
                    in: "query",
                    type: "string",
                    name: "sortByDate",
                    default: 'desc',
                    description: "sortByName",
                    required: false
                },
                {
                    in: "query",
                    type: "string",
                    name: "fromDate",
                    default: new Date().toISOString().substring(0, 10),
                    description: "fromDate",
                    required: false
                },
                {
                    in: "query",
                    type: "string",
                    name: "toDate",
                    default: new Date().toISOString().substring(0, 10),
                    description: "toDate",
                    required: false
                },
                {
                    in: "query",
                    type: "array",
                    name: "subCategories",
                    description: "need a array of subCategoiers like paint,nails",
                    required: false
                },
                {
                    in: "query",
                    type: "array",
                    name: "products",
                    description: "need a array of productNAME like testing,pins",
                    required: false
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/byOrderNo/{orderNo}",
        get: {
            summary: "get order detail by No",
            description: "get order detail by No",
            parameters: [
                {
                    in: "path",
                    type: "string",
                    name: "orderNo",
                    description: "orderNo",
                    required: true
                }],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: "/getByStatus",
        get: {
            summary: "get order detail by Status",
            description: "get order detail by Status",
            parameters: [
                {
                    in: "query",
                    type: "string",
                    name: "status",
                    description: "status= new/current/past",
                    required: true,
                    default: 'new'
                }, {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    }, {
        url: "/updateStatus/{id}",
        put: {
            summary: "Update order status",
            description: "update order status to approved/rejected , ('packed', 'ready to ship', 'onhold','delivered')",
            parameters: [
                {
                    in: "path",
                    name: "id",
                    description: "order id",
                    required: true,
                    type: "string"
                },
                {
                    in: "query",
                    name: "status",
                    description: "order status would be 'approved'/'rejected'etc..",
                    required: true,
                    type: "string"
                },
                {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],

            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
];
