module.exports = [

    {
        url: "/buySubscription",
        post: {
            summary: "Buy subscription",
            description: "pay for plans",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of buy subscription",
                    required: true,
                    schema: {
                        $ref: "#/definitions/buySubscription"
                    }
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },



    {
        url: "/unsubscribePlan",
        post: {
            summary: "unsubscribe plan",
            description: "unsubscribe user's plan",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of unsubscribe plan",
                    required: true,
                    schema: {
                        $ref: "#/definitions/unsubscribePlan"
                    }
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    }

]