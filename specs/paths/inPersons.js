module.exports = [
  {
    url: "/create",
    post: {
      summary: "create and update inperson schedule list",
      description: "create",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of inperson list creation here, type= 'area', 'shipTo', 'customer', 'company' has these values",
          required: true,
          schema: {
            $ref: "#/definitions/addInPersonSchedule"
          }
        }, {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },

  {
    url: "/customerList",
    get: {
      summary: "get User List",
      description: "get All Users",
      parameters: [
        {
          in: "query",
          name: "userId",
          description: "user group id user belongs to.",
          required: true,
          type: "string"
        },
        {
          in: "query",
          name: "type",
          description: "type can be area, shipTo, customer, company",
          required: true,
          type: "string"
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  }
  ,

  {
    url: "/inPersonVisitorUpdate",
    put: {
      summary: "add and update inPerson visitor details",
      description: "update",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of add and update inPerson visitor details",
          required: true,
          schema: {
            $ref: "#/definitions/InPersonVisitorUpdate"
          }
        }, {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },

  {
    url: "/scheduledCustomerList",
    get: {
      summary: "get User's schedule List according date wise",
      description: "get User's schedule List according date wise",
      parameters: [
        {
          in: "query",
          name: "userId",
          description: "userId",
          type: "string",
          required: true,
        },
        {
          in: "query",
          name: "fromDate",
          description: "Date belongs From date",
          type: "string"
        },
        {
          in: "query",
          name: "toDate",
          description: "Date belongs To date",
          type: "string"
        },
        {
          in: "query",
          name: "type",
          description: "type = area/shipTo/customer/company/all",
          type: "string"
        },
        {
          in: "query",
          name: "getByType",
          description: "get visitors by week/month/year",
          type: "string"
        },
        {
          in: "query",
          name: "area",
          description: "area belongs to address area",
          type: "string"
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },

  // {
  //   url: "/inPersonVisitorsWMY",
  //   get: {
  //     summary: "get inPerson visitors for week/month/year",
  //     description: "get inPerson visitors for week/month/year",
  //     parameters: [
  //       {
  //         in: "query",
  //         name: "userId",
  //         description: "userId",
  //         type: "string",
  //         required: true,
  //       },
  //       {
  //         in: "query",
  //         name: "getByType",
  //         description: "get visitors by week/month/year",
  //         type: "string",
  //         default: 'week',
  //         required: true,
  //       },
  //       {
  //         in: "query",
  //         name: "type",
  //         description: "type = area/shipTo/customer/company/all",
  //         default: 'all',
  //         required: true,
  //         type: "string"
  //       },
  //       {
  //         in: "header",
  //         name: "x-access-token",
  //         description: "token to access api",
  //         required: true,
  //         type: "string"
  //       }
  //     ],
  //     responses: {
  //       default: {
  //         description: "Unexpected error",
  //         schema: {
  //           $ref: "#/definitions/Error"
  //         }
  //       }
  //     }
  //   }
  // },

  {
    url: "/search",
    get: {
      summary: "search User",
      description: "search User by email/name",
      parameters: [
        {
          in: "query",
          type: "string",
          name: "searchString",
          description: "enter email or name",
          required: true
        },
        {
          in: "query",
          name: "userId",
          description: "user id of currently logging user",
          required: true,
          type: "string"
        },
        {
          in: "query",
          name: "type",
          description: "type can be area, shipTo, customer, company",
          required: true,
          type: "string"
        },

        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },


];



