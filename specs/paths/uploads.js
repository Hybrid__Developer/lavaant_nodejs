module.exports = [
    {
        url: "/image/{uploadImageOf}/{id}",
        put: {
            summary: 'Image upload',
            description: 'upload',
            parameters: [{
                in: 'path',
                name: 'uploadImageOf',
                description: '"user", "product", "store"',
                required: true,
                default: 'user',
                type: 'string'
            }, {
                "name": "file",
                "in": "formData",
                "description": 'please choose any "user", "product", "store" image according to uploadImage type',
                "required": false,
                "type": "file"
            }, {
                "name": "id",
                "in": "path",
                "description": 'please enter id according to uploadImageOf i.e if uploadImageOf = "user" then id = userId',
                "required": true,
                "type": "string"
            }, {
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },


    {
        url: "/pdf/{uploadPdfOf}/{id}",
        put: {
            summary: 'pdf upload',
            description: 'upload',
            parameters: [{
                in: 'path',
                name: 'uploadPdfOf',
                description: '"user", "product", "store"',
                required: true,
                default: 'product',
                type: 'string'
            }, {
                "name": "file",
                "in": "formData",
                "description": 'please choose any "user", "product", "store" pdf according to uploadPdf type',
                "required": false,
                "type": "file"
            }, {
                "name": "id",
                "in": "path",
                "description": 'please enter id according to uploadPdfOf i.e if uploadPdfOf = "user" then id = userId',
                "required": true,
                "type": "string"
            }, {
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },


    {
        url: "/excelUploader",
        post: {
            summary: "user excel upload",
            description: "upload",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of upload user excel",
                    required: true,
                    schema: {
                        $ref: "#/definitions/userExcel"
                    }
                }, {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: "/productExcelUploader",
        post: {
            summary: "product excel upload",
            description: "upload",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of upload product excel",
                    required: true,
                    schema: {
                        $ref: "#/definitions/productExcel"
                    }
                }, {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
]
