module.exports = [
  {
    url: "/event/create",
    post: {
      summary: "Add event",
      description: "create",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Add event with name, Description, Date and time",
          required: true,
          schema: {
            $ref: "#/definitions/addCalendarEvent"
          }
        }, {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },

  {
    url: "/event/update",
    put: {
      summary: "update event",
      description: "update",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "update calendar events",
          required: true,
          schema: {
            $ref: "#/definitions/calendarEventUpdate"
          }
        }, {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },

  {
    url: "/getEventsByUserId",
    get: {
      summary: "get all events of logging user",
      description: "get events list of logging user by Id",
      parameters: [
        {
          in: "query",
          name: "userId",
          description: "userId",
          type: "string",
          required: true,
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },

]