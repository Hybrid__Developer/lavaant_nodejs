module.exports = [
    {
        url: "/member/add",
        post: {
            summary: "create",
            description: "create",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of add team member",
                    required: true,
                    schema: {
                        $ref: "#/definitions/addMember"
                    }
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },


    {
        url: "/getMemberList",
        get: {
            summary: "get team members List",
            description: "get team members List",
            parameters: [
                {
                    in: "query",
                    name: "userId",
                    description: "user id of currently logging user",
                    required: true,
                    type: "string"
                },
                {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },


];
