module.exports = [
    {
        url: "/potentialCustomer/add",
        post: {
            summary: "create",
            description: "create",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of add potential customer",
                    required: true,
                    schema: {
                        $ref: "#/definitions/addPotentialCustomer"
                    }
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: "/mapAddress/add",
        post: {
            summary: "add map address",
            description: "add map address",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of add map address",
                    required: true,
                    schema: {
                        $ref: "#/definitions/addMapAddress"
                    }
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: "/potentialCustomer/list",
        get: {
            summary: "Potential Customers List",
            description: "get potential customers List",
            parameters: [
                {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: "/deletePotentialCustomer/{id}",
        delete: {
            summary: "delete potential customer",
            description: "delete potential customer by Id",
            parameters: [{
                in: "path",
                name: "id",
                description: "potential customer Id",
                required: true,
                type: "string"
            },
            {
                in: "header",
                name: "x-access-token",
                description: "token to access api",
                required: true,
                type: "string"
            }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: "/mapAddressUpdate/{id}",
        put: {
            summary: "mapAddressUpdate ",
            description: "update map address",
            parameters: [
                {
                    in: "path",
                    name: "id",
                    description: "customer id",
                    required: true,
                    type: "string"
                },
                {
                    in: "body",
                    name: "body",
                    description: "Model of map address update",
                    required: true,
                    schema: {
                        $ref: "#/definitions/mapAddressUpdate"
                    }
                },
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: "/mapAddressList",
        get: {
            summary: "Potential Customers address List",
            description: "get potential customers address List",
            parameters: [
                {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
];



