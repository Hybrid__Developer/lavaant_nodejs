module.exports = [
  {
    url: "/subCategories",
    put: {
      summary: "Update favorite subCategories",
      description: "user favorite subCategories update",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of user favorite subCategories update",
          required: true,
          schema: {
            $ref: "#/definitions/favouriteSubCategories"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    },
        get: {
          summary: "get favourite subcategories",
          description: "get favourite subcategories",
          parameters: [
            {
              in: "query",
              name: "categoryId",
              description: "user group id user belongs to.",
              required: true,
              type: "string"
            },
            {
              in: "header",
              name: "x-access-token",
              description: "token to access api",
              required: true,
              type: "string"
            }
          ],
    
          responses: {
            default: {
              description: "Unexpected error",
              schema: {
                $ref: "#/definitions/Error"
              }
            }
          }
        }
  },
  {
    url: "/",
    get: {
      summary: "get favourite",
      description: "get favourite",
      parameters: [
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/stores",
    put: {
      summary: "Update favorite stores/company/vendor list",
      description: "user favorite vendors update",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of user favorite stores/vendors update",
          required: true,
          schema: {
            $ref: "#/definitions/favouriteVendors"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    },
    get: {
      summary: "get favourite Stores",
      description: "get favourite Stores",
      parameters: [
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
   
  },
];
