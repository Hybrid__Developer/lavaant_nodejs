module.exports = [
  {
    url: "/",
    post: {
      summary: "create",
      description: "create",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of store",
          required: true,
          schema: {
            $ref: "#/definitions/storeCreate"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/update/{id}",
    put: {
      summary: "Update",
      description: "update user details",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "user id",
          required: true,
          type: "string"
        },
        {
          in: "body",
          name: "body",
          description: "Model of user update",
          required: true,
          schema: {
            $ref: "#/definitions/storeUpdate"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/updateStatus/{id}",
    put: {
      summary: "Update store status",
      description: "update store status active/inactive.",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "store id",
          required: true,
          type: "string"
        },
        {
          in: "query",
          name: "status",
          description: "store status 'active/inactive'",
          required: true,
          type: "string"
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/list",
    get: {
      summary: "get Stores List",
      description: "get All Stores",
      parameters: [
        {
          in: "query",
          type: "string",
          name: "status",
          description: "status active/inactive",
          default: 'active',
          required: false
        },

      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/getById/{id}",
    get: {
      summary: "get store",
      description: "get store detail by Id",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "storeId",
          required: true,
          type: "string"
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/search",
    get: {
      summary: "search store",
      description: "search store by email/name",
      parameters: [
        {
          in: "query",
          type: "string",
          name: "searchString",
          description: "enter email or name",
          required: true
        },
        {
          in: "query",
          type: "string",
          name: "status",
          description: "status default status= 'active'",
          default: 'active',
          required: false
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  }
];
