module.exports = [
  {
    url: "/",
    post: {
      summary: "create",
      description: "create subscription plan",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of subscription plan creation. validity type will be :'days', 'months', 'years'",
          required: true,
          schema: {
            $ref: "#/definitions/createPlan"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    },
    get: {
      summary: "get plans List",
      description: "get plans by status and role or all",
      parameters: [
        {
          in: "query",
          type: "string",
          name: "role",
          description: "role= customer, vendor, admin, store",
          required: false
        },
        {
          in: "query",
          type: "string",
          name: "status",
          description: "active/inactive",
          required: false
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }

      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/{id}",
    put: {
      summary: "Update",
      description: "update subscription plan",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "plan id",
          required: true,
          type: "string"
        },
        {
          in: "body",
          name: "body",
          description: "Model of plan update",
          required: true,
          schema: {
            $ref: "#/definitions/updatePlan"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/getById/{id}",
    get: {
      summary: "get plan",
      description: "get plan detail by Id",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "plan Id",
          required: true,
          type: "string"
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/updateStatus/{id}",
    put: {
      summary: "Update plan status",
      description: "update plan status active/inactive.",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "plan id",
          required: true,
          type: "string"
        },
        {
          in: "query",
          name: "status",
          description: "task status 'active/inactive'",
          required: true,
          type: "string"
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/features/add",
    post: {
      summary: "Add plan feature",
      description: "Add plan feature",
      parameters: [
       {
          in: "body",
          name: "body",
          description: "Model of add features to subscription plan.",
          required: true,
          schema: {
            $ref: "#/definitions/createPlanFeatures"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/features/remove",
    put: {
      summary: "Remove plan feature",
      description: "Remove plan feature",
      parameters: [
        {
          in: "query",
          name: "featureId",
          description: "featureId",
          required: true,
          type: "string"
        },
        {
          in: "query",
          name: "planId",
          description: "planId",
          required: true,
          type: "string"
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
];
