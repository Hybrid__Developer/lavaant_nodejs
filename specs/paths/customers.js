module.exports = [
    {
        url: "/add",
        post: {
            summary: "create",
            description: "create",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of customer creation",
                    required: true,
                    schema: {
                        $ref: "#/definitions/customerCreate"
                    }
                },
                {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/ListByUserId",
        get: {
            summary: "get customers",
            description: "get customers by user Id",
            parameters: [
                {
                    in: "query",
                    type: "string",
                    name: "userId",
                    description: "user id",
                    default: false,
                    required: true
                },
                {
                    in: "query",
                    type: "integer",
                    name: "pageNo",
                    description: "pageNo",
                    required: false
                },
                {
                    in: "query",
                    type: "integer",
                    name: "pageSize",
                    description: "pageSize",
                    required: false
                },

            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/update",
        put: {
            summary: "update",
            description: "update",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of update customer",
                    required: true,
                    schema: {
                        $ref: "#/definitions/customerUpdate"
                    }
                },
                {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: "/delete",
        post: {
            summary: "delete",
            description: "delete",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of update customer",
                    required: true,
                    schema: {
                        $ref: "#/definitions/customerDelete"
                    }
                },
                {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

    {
        url: "/customerExcelUploader",
        post: {
            summary: "user excel upload",
            description: "upload",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of upload user excel",
                    required: true,
                    schema: {
                        $ref: "#/definitions/customerExcel"
                    }
                }, {
                    in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    }

]    