module.exports = [
  {
    url: "/",
    post: {
      summary: "create",
      description: "create category",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of category creation.",
          required: true,
          schema: {
            $ref: "#/definitions/createSubCategory"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/{id}",
    put: {
      summary: "Update",
      description: "update category",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "category id",
          required: true,
          type: "string"
        },
        {
          in: "body",
          name: "body",
          description: "Model of category update",
          required: true,
          schema: {
            $ref: "#/definitions/updateCategory"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/getByCategory",
    get: {
      summary: "get subCategory List",
      description: "get All subCategory by Category id or all",
      parameters: [
        {
          in: "query",
          type: "string",
          name: "id",
          description: "categoryId or 'all' to get all subcategories",
          required: true
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }

      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  }
];
