module.exports = [
  {
    url: "/",
    post: {
      summary: "buy subscription",
      description: "buy subscription",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of buy subscription.",
          required: true,
          schema: {
            $ref: "#/definitions/buySubscription"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    },
    get: {
      summary: "get subscription List",
      description: "get All subscriptions",
      parameters: [
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }

      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  }
];
