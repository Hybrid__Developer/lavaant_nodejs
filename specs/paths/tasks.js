module.exports = [{
        url: "/",
        post: {
            summary: "create",
            description: "create task",
            parameters: [{ in: "body",
                    name: "body",
                    description: "Model of task creation, here reminder have values='1day', '2day', '1week', '1month'",
                    required: true,
                    schema: {
                        $ref: "#/definitions/createTask"
                    }
                },
                { in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        },
        get: {
            summary: "get task List",
            description: "get All tasks by category",
            parameters: [{ in: "query",
                    type: "string",
                    name: "category",
                    description: "fill categoryId if you want task by category",
                    default: 'all',
                    required: true
                },
                { in: "query",
                    type: "string",
                    name: "user",
                    description: "userId",
                    required: true
                },
                { in: "query",
                    type: "string",
                    name: "sortByName",
                    default: '',
                    description: "sortByName will sort by details and has value asce/desc",
                    required: false
                },
                { in: "query",
                    type: "string",
                    name: "sortByDate",
                    default: '',
                    description: "sortByDate will sort by created date and has value asce/desc",
                    required: false
                },
                { in: "query",
                    type: "string",
                    name: "sortByCompletedFirst",
                    default: '',
                    description: "sortByCompletedFirst will sort by completedDate + status and has value asce/desc",
                    required: false
                },
                { in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }

            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/update/{id}",
        put: {
            summary: "Update",
            description: "update task details",
            parameters: [{ in: "path",
                    name: "id",
                    description: "task id",
                    required: true,
                    type: "string"
                },
                { in: "body",
                    name: "body",
                    description: "Model of user update",
                    required: true,
                    schema: {
                        $ref: "#/definitions/createTask"
                    }
                },
                { in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],

            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/updateStatus/{id}",
        put: {
            summary: "Update task status",
            description: "update task status to done.",
            parameters: [{ in: "path",
                    name: "id",
                    description: "task id",
                    required: true,
                    type: "string"
                },
                { in: "query",
                    name: "status",
                    description: "task status would be 'paused'/'completed'",
                    required: true,
                    type: "string"
                },
                { in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],

            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/delete/{id}",
        delete: {
            summary: "delete task",
            description: "delete task by Id",
            parameters: [{ in: "path",
                    name: "id",
                    description: "taskId",
                    required: true,
                    type: "string"
                },
                { in: "header",
                    name: "x-access-token",
                    description: "token to access api",
                    required: true,
                    type: "string"
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/list",
        get: {
            summary: "get task List",
            description: "get all tasks of user",
            parameters: [{ in: "query",
                    type: "string",
                    name: "userId",
                    description: "userId",
                    required: true
                },
                { in: "query",
                    type: "string",
                    name: "sortByName",
                    default: 'desc',
                    description: "sortByName",
                    required: false
                },
                { in: "query",
                    type: "string",
                    name: "sortByDate",
                    default: 'desc',
                    description: "sortByName",
                    required: false
                },
                { in: "query",
                    type: "string",
                    name: "fromDate",
                    default: new Date().toISOString().substring(0, 10),
                    description: "fromDate",
                    required: false
                },
                { in: "query",
                    type: "string",
                    name: "toDate",
                    default: new Date().toISOString().substring(0, 10),
                    description: "toDate",
                    required: false
                },
                { in: "query",
                    type: "array",
                    name: "categories",
                    description: "need categories id seprated by ',' ex: 123,456,6",
                    required: false
                }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
];