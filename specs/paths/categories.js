module.exports = [
  {
    url: "/",
    post: {
      summary: "create",
      description: "create category",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of category creation. 'fill categoryOf if there is any specific type of category. (for ex. categoryOf: 'task')",
          required: true,
          schema: {
            $ref: "#/definitions/createCategory"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    },
    get: {
      summary: "get Category List",
      description: "get All Categories",
      parameters: [
        {
          in: "query",
          type: "string",
          name: "categoryOf",
          description: "fill categoryOf if there is any specific type of category. (for ex. categoryOf: 'task') otherwise empty",
          required: false
        },
        {
          in: "query",
          type: "integer",
          name: "id",
          description: "fill userId if categoryOf has any type specified",
          required: false
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }

      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/{id}",
    put: {
      summary: "Update",
      description: "update category",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "category id",
          required: true,
          type: "string"
        },
        {
          in: "body",
          name: "body",
          description: "Model of category update",
          required: true,
          schema: {
            $ref: "#/definitions/updateCategory"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  }
];
