module.exports = [
  {
    url: "/register",
    post: {
      summary: "create",
      description: "create",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of user creation (here, role= 'customer','vendor','admin'",
          required: true,
          schema: {
            $ref: "#/definitions/userCreate"
          }
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/list",
    get: {
      summary: "get User List",
      description: "get All Users",
      parameters: [
        // {
        //   in: "query",
        //   type: "integer",
        //   name: "paging",
        //   description: "paging",
        //   default: false,
        //   required: false
        // },
        {
          in: "query",
          type: "integer",
          name: "pageNo",
          description: "pageNo",
          required: false
        },
        {
          in: "query",
          type: "integer",
          name: "pageSize",
          description: "pageSize",
          required: false
        },
        {
          in: "query",
          type: "string",
          name: "role",
          description: "role = customer/vendor/all",
          default: 'all',
          required: true
        },

      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/search",
    get: {
      summary: "search User",
      description: "search User by email/name and role",
      parameters: [
        {
          in: "query",
          type: "string",
          name: "searchString",
          description: "enter email or name",
          required: true
        },
        {
          in: "query",
          type: "string",
          name: "sortByName",
          default: '',
          description: "sortByName will sort by username and has value asce/desc",
          required: false
        },
        {
          in: "query",
          type: "string",
          name: "sortByShipTo",
          default: '',
          description: "sortByShipTo will sort by address and has value asce/desc",
          required: false
        }, {
          in: "query",
          type: "string",
          name: "sortByCompany",
          default: '',
          description: "sortByCompany will sort by companyname and has value asce/desc",
          required: false
        }, {
          in: "query",
          type: "string",
          name: "sortByArea",
          default: '',
          description: "sortByArea will sort by area and has value asce/desc",
          required: false
        },
        {
          in: "query",
          type: "string",
          name: "role",
          description: "role default role= 'customer'",
          default: 'customer',
          required: false
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/update/{id}",
    put: {
      summary: "Update",
      description: "update user details",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "user id",
          required: true,
          type: "string"
        },
        {
          in: "body",
          name: "body",
          description: "Model of user update",
          required: true,
          schema: {
            $ref: "#/definitions/userUpdate"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/deviceToken/addOrRemove/{id}",
    put: {
      summary: "add Or Remove device Token",
      description: "add Or Remove device Token / fcm token",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "userId",
          required: true,
          type: "string"
        },
        {
          in: "body",
          name: "body",
          description: "Model of user device token update",
          required: true,
          schema: {
            $ref: "#/definitions/userDeviceToken"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/getById/{id}",
    get: {
      summary: "get user",
      description: "get user detail by Id",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "userId",
          required: true,
          type: "string"
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/login",
    post: {
      summary: "Login user",
      description: "user login into system and get its token to access apis",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of login user",
          required: true,
          schema: {
            $ref: "#/definitions/Login"
          }
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },

  {
    url: "/facebook/login",
    post: {
      summary: "Login user with facebook",
      description: "user login into system with facebook and get its token to access apis",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of login user with facebook",
          required: true,
          schema: {
            $ref: "#/definitions/fbLogin"
          }
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/resetPassword",
    post: {
      summary: "Reset Password",
      description: "reset Password",
      parameters: [
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        },
        {
          in: "body",
          name: "body",
          description: "Model of resetPassword user",
          required: true,
          schema: {
            $ref: "#/definitions/resetPassword"
          }
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },

  {
    url: "/addAddress",
    post: {
      summary: "add",
      description: "add-address",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of addAdress creation",
          required: true,
          schema: {
            $ref: "#/definitions/addAddress"
          }
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/addressList/{id}",
    get: {
      summary: "addressList",
      description: "get address list  by userId",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "userId",
          required: true,
          type: "string"
        },
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/addressUpdate/{id}",
    put: {
      summary: "addressUpdate ",
      description: "update address details",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "address id",
          required: true,
          type: "string"
        },
        {
          in: "body",
          name: "body",
          description: "Model of address update",
          required: true,
          schema: {
            $ref: "#/definitions/addressUpdate"
          }
        },
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: '/otp',
    put: {
      summary: 'Send otp',
      description: 'Send otp through registered email',
      parameters: [{
        in: 'body',
        name: 'body',
        description: 'Model of User creation',
        required: true,
        schema: {
          $ref: '#/definitions/otp'
        }
      }],
      responses: {
        default: {
          description: 'Unexpected error',
          schema: {
            $ref: '#/definitions/Error'
          }
        }
      }
    }
  },
  {
    url: '/forgotPassword',
    put: {
      summary: 'forgotPassword',
      description: 'forgotPassword',
      parameters: [{
        in: 'body',
        name: 'body',
        description: 'Model of User creation',
        required: true,
        schema: {
          $ref: '#/definitions/forgotPassword'
        }
      }],
      responses: {
        default: {
          description: 'Unexpected error',
          schema: {
            $ref: '#/definitions/Error'
          }
        }
      }
    }
  },
  {
    url: '/otp/verify',
    put: {
      summary: 'Otp Verification',
      description: 'otp verification',
      parameters: [{
        in: 'query',
        name: 'otp',
        description: 'otp',
        required: true,
        type: 'string'
      }, {
        in: 'query',
        name: 'otpVerifyToken',
        description: 'otpVerifyToken',
        required: true,
        type: 'string'
      }],
      responses: {
        default: {
          description: 'Unexpected error',
          schema: {
            $ref: '#/definitions/Error'
          }
        }
      }
    }
  },
  {
    url: "/getCompanyList",
    get: {
      summary: "getCompanyList and related users",
      description: "getCompanyList and users list relates to that company",
      parameters: [
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/searchCompany",
    get: {
      summary: "searchCompany",
      description: "searchCompany",
      parameters: [
        {
          in: 'query',
          name: 'companyName',
          description: 'companyName',
          required: true,
          type: 'string'
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/update/companyAdderessList",
    put: {
      summary: "update companyAdderessList",
      description: "update companyAdderess and related users company address",
      parameters: [
        {
          in: 'query',
          name: 'companyName',
          description: 'companyName, initial company name getting from get and search companyaddress res',
          required: true,
          type: 'string'
        }, {
          in: 'body',
          name: 'body',
          description: 'Model of User creation',
          required: true,
          schema: {
            $ref: '#/definitions/updateCompanyAddressList'
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
  {
    url: "/status/{id}",
    put: {
      summary: "User active or inactive",
      description: "user login status active or inactive",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "user id",
          required: true,
          type: "string"
        },
        {
          in: "body",
          name: "body",
          description: "Model of user update",
          required: true,
          schema: {
            $ref: "#/definitions/loginStatusUpdate"
          }
        },
        {
          in: "header",
          name: "x-access-token",
          description: "token to access api",
          required: true,
          type: "string"
        }
      ],

      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error"
          }
        }
      }
    }
  },
];
