module.exports = [
    // {
    //     url: '/{count}',
    //     get: {
    //         summary: 'get users',
    //         description: 'get users count (male,female,premium,ios,android or all)',
    //         parameters: [{
    //             in: 'path',
    //             name: 'count',
    //             description: ' pass (male,female,premium,ios,android) and default value is "all"',
    //             required: true,
    //             default: 'all'
    //         }, {
    //             in: 'query',
    //             name: 'by',
    //             description: 'by (city,country) and default value is "all"',
    //             required: true,
    //             default: 'all'
    //         },
    //         {
    //             in: 'query',
    //             name: 'name',
    //             description: 'name of "city/country"',
    //             required: false,
    //             default: ''
    //         }, {
    //             in: 'header',
    //             name: 'x-access-token',
    //             description: 'token to access api',
    //             required: true,
    //             type: 'string'
    //         }],
    //         responses: {
    //             default: {
    //                 description: 'Unexpected error',
    //                 schema: {
    //                     $ref: '#/definitions/Error'
    //                 }
    //             }
    //         }
    //     },

    // },
    {
        url: '/monthly/users',
        get: {
            summary: 'get monthly count of vendors/customers of current year',
            description: 'get User count by month',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }
]
