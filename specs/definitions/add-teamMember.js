module.exports = [
    {
        name: "addMember",
        properties: {
            userId: { type: "string", default: "" },
            name: { type: "string", default: "" },
            title: { type: "string", default: "" },
            birthdayDate: { type: "string", default: "" },
            hireDate: { type: "string", default: "" },
            nickName: { type: "string", default: "" },
            phone: { type: "string", default: "" },
            sales: { type: "string", default: "" },
            anniversaryDate: { type: "string", default: "" }
        }
    }
];


