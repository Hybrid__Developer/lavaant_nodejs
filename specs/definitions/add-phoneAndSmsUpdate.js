module.exports = [
    {
        name: "phoneAndSmsUpdate",
        properties: {
            userId: { type: "string", default: "" },
            customerId: {
                type: "string",
                default: "",
            },
            type: {
                type: "string",
                default: "",
            },
            contact: {
                type: "string",
                default: "",
            },
            sms: {
                type: "string",
                default: "",
            },
        }
    }
];
