module.exports = [
  {
    name: "createPlan",
    properties: {
      title: { type: "string", default: "" },
      description: { type: "string", default: "" },
      role: {
        type: "string",
        default: "customer",
        enum: ["customer", "vendor", "admin"]
      },
      price: { type: "string", default: "" },
      price1: { type: "string", default: "" },
      price2: { type: "string", default: "" },
      discountedPrice: { type: "string", default: "" },
      validity: {
        type: "object",
        properties: {
          type: {
            type: "string",
            default: "days",
            enum: ["days", "month", "year",]
          },
          timePeriod: { type: "number", default: 1 },
        }
      },
      features: {
        type: 'array',
        items: {
          type: 'array',
          properties: {
            id: {
              type: "string",
              default: "",
            },
          }
        }
      },
    }
  }

];
