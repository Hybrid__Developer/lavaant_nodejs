module.exports = [
    {
        name: "unsubscribePlan",
        properties: {
            userId: {
                type: "string"
            },
            subscriptionId: {
                type: "string"
            }
        }
    }
];
