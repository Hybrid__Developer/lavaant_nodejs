module.exports = [
  {
    name: "addInPersonSchedule",
    properties: {
      userId: { type: "string", default: "" },
      type: {
        type: "string", default: "area",
        enum: ["area", "shipTo", 'customer', 'company']
      },
      toVisit: {
        type: 'array',
        items: {
          type: 'array',
          properties: {
            id: {
              type: "string",
              default: "",
            },
          },

        }
      }
    }
  }

];
