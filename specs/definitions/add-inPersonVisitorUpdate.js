module.exports = [
    {
        name: "InPersonVisitorUpdate",
        properties: {
            userId: { type: "string", default: "" },
            customerId: {
                type: "string",
                default: "",
            },
            type: {
                type: "string",
                default: "",
            },
            contact: {
                type: "string",
                default: "",
            },
            note1: {
                type: "string",
                default: "",
            },
            note2: {
                type: "string",
                default: "",
            },
            recordNotes: {
                type: "string",
                default: "",
            },
            firstName: {
                type: "string",
                default: "",
            },
            lastName: {
                type: "string",
                default: "",
            },
            phoneNo: {
                type: "string",
                default: "",
            },
            recordTaskReminder: {
                type: 'object',
                properties: {
                    reminderText: {
                        type: "string",
                        default: "",
                    },
                    reminderNote: {
                        type: "string",
                        default: "",
                    },
                    fromDate: {
                        type: "string",
                        default: "",
                    },
                    toDate: {
                        type: "string",
                        default: "",
                    },
                },
            }
        }
    }
];
