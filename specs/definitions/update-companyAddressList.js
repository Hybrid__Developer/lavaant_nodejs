module.exports = [
  {
    name: "updateCompanyAddressList",
    properties: {
      companyName: { type: "string", default: "" },
      companyPhone: { type: "string", default: "" },
      companyAddress: { type: "string", default: "" }
    }
  }
];
