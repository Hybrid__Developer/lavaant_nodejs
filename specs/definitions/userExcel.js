module.exports = [
    {
        name: "userExcel",
        properties: {
            uploadExcelOf: {
                type: 'string',
            },
            excel: {
                type: 'array',
                items: {
                    type: 'array',
                    properties: {
                        firstName: {
                            type: "string",
                            default: "",
                        },
                        lastName: {
                            type: "string",
                            default: "",
                        },
                        email: {
                            type: "string",
                            default: "",
                        },
                        mobileNo: {
                            type: "string",
                            default: "",
                        },
                        password: {
                            type: "string",
                            default: "",
                        },
                        role: {
                            type: "string",
                            default: "",
                        },
                        dob: {
                            type: "string",
                            default: "",
                        },
                        anniversary: {
                            type: "string",
                            default: "",
                        },
                    },
                }
            }
        }
    }

];


