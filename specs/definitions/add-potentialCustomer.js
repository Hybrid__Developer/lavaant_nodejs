module.exports = [
    {
        name: "addPotentialCustomer",
        properties: {
            company: { type: "string", default: "" },
            customerName: { type: "string", default: "" },
            address: { type: "string", default: "" },
            phone: { type: "string", default: "" },
            email: { type: "string", default: "" },
            note1: { type: "string", default: "" },
            note2: { type: "string", default: "" }
        }
    }
];


