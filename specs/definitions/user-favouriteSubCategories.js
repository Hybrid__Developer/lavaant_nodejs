module.exports = [
  {
    name: "favouriteSubCategories",
    properties: {
      favouriteSubCategories: {
        type: 'array',
        items: {
            type: 'array',
            properties: {
              id: {
                type: "string"
              }
            }
        }
    }
    }
  }
];
