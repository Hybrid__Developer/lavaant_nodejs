module.exports = [
  {
    name: "buySubscription",
    properties: {
      paymentId: { type: "string", default: "" },
      plan: { type: "string", default: "" },
      status: {
        type: "string",
        default: "active",
        enum: ["active", "inactive"]
      },
      user: { type: "string", default: "" }
    }
  }

];
