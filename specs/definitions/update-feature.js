
module.exports = [
    {
        name: "updateFeature",
        properties: {
            name: { type: "string", default: "" },
            description: { type: "string", default: "" },
            role: { type: "string", default: "" },
            status: { type: "string", default: "" },
        }
    }

];
