module.exports = [{
    name: 'forgotPassword',
    properties: {
        newPassword: {
            type: 'string'
        },
        otpVerifyToken: {
            type: 'string'
        }
    }
}]