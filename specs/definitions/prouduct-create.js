module.exports = [
    {
        name: "productCreate",
        properties: {
            name: {
                type: "string"
            },
            description: {
                type: "string"
            },
            quantity: {
                type: "number"
            },
            costPerEach: {
                type: "string"
            },
            overAllPrice: {
                type: "string"
            },
            note: {
                type: "string"
            },
            category: {
                type: "string"
            },
            image: {
                type: "string",
            },
            height: {
                type: "string"
            },
            width: {
                type: "string"
            },
            weight: {
                type: "string"
            },
            length: {
                type: "string"
            },
            subCategory: {
                type: "string"
            },
            rvDescription: {
                type: "string"
            },
            manufacturer: {
                type: "string"
            },
            sku: {
                type: "string"
            },
            uom: {
                type: "string"
            },
            status: {
                type: "string",
                enum: ["active", "deactive", "out of stock"]
            },
            assignedVendors: {
                type: 'array',
                items: {
                    type: 'array',
                    properties: {
                        storeId: { type: "string", default: "" },
                    }
                }
            },
            variation: {
                properties: {
                    items: {
                        type: 'array',
                        items: {
                            type: 'array',
                            properties: {
                                type: { type: "string", default: "" },
                                price: { type: "number", default: 0 },
                            }
                        }
                    }
                }

            }

        }
    }
];
