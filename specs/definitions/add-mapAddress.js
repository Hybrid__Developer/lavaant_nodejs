module.exports = [
    {
        name: "addMapAddress",
        properties: {
            potentialCusId: { type: "string", default: "" },
            lat: { type: "string", default: "" },
            lng: { type: "string", default: "" },
            location: { type: "string", default: "" },
        }
    }
];