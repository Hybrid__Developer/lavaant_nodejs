module.exports = [
    {
        name: "addphoneSchedule",
        properties: {
            userId: { type: "string", default: "" },
            type: {
                type: "string", default: "area",
                enum: ["area", "shipTo", 'customer', 'company']
            },
            toPhone: {
                type: 'array',
                items: {
                    type: 'array',
                    properties: {
                        id: {
                            type: "string",
                            default: "",
                        }
                    },

                }
            }
        }
    }

];
