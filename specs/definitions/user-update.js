module.exports = [
  {
    name: "userUpdate",
    properties: {
      firstName: {
        type: "string"
      },
      lastName: {
        type: "string"
      },
      alternateMobileNo: {
        type: "string"
      },
      mobileNo: {
        type: "string"
      },
      // country: {
      //   type: "string"
      // },
      // address: {
      //   type: "string"
      // },
      dob: {
        type: "string"
      },
      status: {
        type: "string"
      },
      anniversary: {
        type: "string"
      },
      customerGroup: {
        type: "string"
      },

      profiePic: {
        type: "file"
      },
      role: {
        type: "string",
        default: "customer",
        enum: ["customer", "vendor", "admin"]
      },
      workDetails: {
        type: "object",
        properties: {
          companyName: { type: "string", default: "" },
          companyPhone: { type: "string", default: "" },
          companyAddress: { type: "string", default: "" }
        }
      },
      otherInformation: {
        type: "object",
        properties: {
          hobbies: { type: "string", default: "" },
          interests: { type: "string", default: "" },
          isMarried: { type: "boolean", default: false },
          companionName: { type: "string", default: "" },
          anniversary: { type: "string", default: "" },
          kids: {
            type: 'array',
            items: {
                type: 'array',
                properties: {
                  name: {
                    type: "string"
                  },
                  birthdate: {
                    type: "string"
                  },
                }
            }
        }
        }
      }

    }
  }
];
