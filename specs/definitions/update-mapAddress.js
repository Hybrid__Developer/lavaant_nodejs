module.exports = [
    {
        name: "mapAddressUpdate",
        properties: {
            lat: { type: "string", default: "" },
            lng: { type: "string", default: "" },
            location: { type: "string", default: "" },
        }
    }
];
