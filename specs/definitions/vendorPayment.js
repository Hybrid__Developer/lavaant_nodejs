module.exports = [
    {
        name: "buySubscription",
        properties: {
            userId: {
                type: "string",
            },
            userName: {
                type: "string"
            },
            amount: { type: "number", default: "" },

            planId: {
                type: "string"
            },
            plan: {
                type: "string"
            },
            customerEmail: {
                type: "string",
            },
            stripeToken: {
                type: "string"
            }
        }
    }
];