module.exports = [
    {
        name: "productExcel",
        properties: {
            excel: {
                type: 'array',
                items: {
                    type: 'array',
                    properties: {
                        name: {
                            type: "string"
                        },
                        description: {
                            type: "string"
                        },
                        quantity: {
                            type: "number"
                        },
                        costPerEach: {
                            type: "string"
                        },
                        overAllPrice: {
                            type: "string"
                        },
                        note: {
                            type: "string"
                        },
                        category: {
                            type: "string"
                        },
                        height: {
                            type: "string"
                        },
                        width: {
                            type: "string"
                        },
                        weight: {
                            type: "string"
                        },
                        length: {
                            type: "string"
                        },
                        subCategory: {
                            type: "string"
                        },
                        rvDescription: {
                            type: "string"
                        },
                        manufacturer: {
                            type: "string"
                        },
                        sku: {
                            type: "string"
                        },
                        uom: {
                            type: "string"
                        },
                    },
                }
            }
        }
    }
];

