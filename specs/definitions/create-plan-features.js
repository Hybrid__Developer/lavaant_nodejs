module.exports = [
  {
    name: "createPlanFeatures",
    properties: {
      planId: { type: "string", default: "" },
      features: {
        type: 'array',
        items: {
            type: 'array',
            properties: {
          id: {
            type: "string",
            default: "",
          },
        }
        }
      },
    }
  }

];
