module.exports = [
    {
        name: "calendarEventUpdate",
        properties: {
            eventId: {
                type: "string",
                default: "",
            },
            start: {
                type: "string",
                default: "",
            },
            time: {
                type: "string",
                default: "",
            },
            durationHours: {
                type: "string",
                default: "",
            },
            durationMinutes: {
                type: "string",
                default: "",
            },
            name: {
                type: "string",
                default: "",
            },
            description: {
                type: "string",
                default: "",
            },
            reminder: {
                type: "boolean",
                default: "",
            }
        }
    }
];
