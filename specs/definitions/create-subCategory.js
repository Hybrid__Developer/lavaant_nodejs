module.exports = [
    {
        name: "createSubCategory",
        properties: {
            name: { type: "string", default: "" },
            categoryId: { type: "string", default: "" },
        }
    }

];
