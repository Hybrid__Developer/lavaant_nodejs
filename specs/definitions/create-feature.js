module.exports = [
    {
        name: "createFeature",
        properties: {
            name: { type: "string", default: "" },
            description: { type: "string", default: "" },
            role: { type: "string", default: "" },
        }
    }

];
