module.exports = [

    {
        name: "selectCompany",
        properties: {
            workDetails: {
                type: "object",
                properties: {
                    companyName: { type: "string", default: "" },
                    companySelected: {
                        type: "boolean", default: ""
                    },
                }
            },
        }
    }
];
