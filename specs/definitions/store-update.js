module.exports = [

  {
    name: "storeUpdate",
    properties: {
      name: {
        type: "string"
      },
      owner: {
        type: "string"
      },
      address: {
        type: "string"
      },
      city: {
        type: "string"
      },
      state: {
        type: "string"
      },
      zipCode: {
        type: "string"
      },
      description: {
        type: "string"
      },
      contactName: {
        type: "string"
      },
      contactNumber: {
        type: "string"
      },
      alternateContactNumber: {
        type: "string"
      },
      // email: {
      //   type: "string"
      // },
      // password: {
      //   type: "string"
      // },
      area: {
        type: "string"
      }
    }
  }
];
