module.exports = [
    {
        name: "createTask",
        properties: {
            details: { type: "string", default: "" },
            reminder: { type: "string", default: "" },
            dueDate: { type: "string", default: "" },
            categoryId: { type: "string", default: "" },
        }
    }

];
