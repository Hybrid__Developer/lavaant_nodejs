module.exports = [
  {
    name: "updatePlan",
    properties: {
      title: { type: "string", default: "" },
      description: { type: "string", default: "" },
      price: { type: "string", default: "" },
      discountedPrice: { type: "string", default: "" },
      validity: {
        type: "object",
        properties: {
          type: {
            type: "string",
            default: "days",
            enum: ["days", "month", "year",]
          },
          timePeriod: { type: "number", default: 1 },
        }
      },
      features: {
        type: 'array',
        items: {
            type: 'array',
            properties: {
          id: {
            type: "string",
            default: "",
          },
        }
        }
      },
    }
  }

];
