module.exports = [
    {
        name: "loginStatusUpdate",
        properties: {
            status: {
                type: "string",
                enum: ['active', 'inactive']

            }
        }
    }
]
