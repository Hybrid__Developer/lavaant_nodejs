"use strict";
const response = require("../exchange/response");

const create = (req, res, next) => {
  const log = req.context.logger.start("validators:subCategory:create");

  if (!req.body) {
    log.end();
    return response.failure(res, "body is required");
  }
  if (req.body.categoryId === "string" || req.body.categoryId === undefined) {
    log.end();
    return response.failure(res, "CategoryId is required");
  }
  log.end();
  return next();
};

exports.create = create;
