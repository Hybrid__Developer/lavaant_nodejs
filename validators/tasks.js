"use strict";
const response = require("../exchange/response");

const create = (req, res, next) => {
  const log = req.context.logger.start("validators:tasks:create");

  if (!req.body) {
    log.end();
    return response.failure(res, "body is required");
  }
  if (req.body.categoryId === "string" || req.body.categoryId === undefined) {
    log.end();
    return response.failure(res, "CategoryId is required");
  }
  if (req.body.reminder === "" || req.body.reminder === "string" || req.body.reminder === undefined) {
    log.end();
    return response.failure(res, "reminder is required");
  }

  log.end();
  return next();
};
const get = (req, res, next) => {
  const log = req.context.logger.start("validators:tasks:get");

  if (!req.query && !req.query.user) {
    log.end();
    return response.failure(res, "user is required");
  }
  
  if (!req.query.category) {
    log.end();
    return response.failure(res, "category is required");
  }
  log.end();
  return next();
};
exports.create = create;
exports.get = get;
