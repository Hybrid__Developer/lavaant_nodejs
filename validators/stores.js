"use strict";
const response = require("../exchange/response");

const create = (req, res, next) => {
  const log = req.context.logger.start("validators:users:create");

  if (!req.body) {
    log.end();
    return response.failure(res, "body is equired");
  }
  if (!req.body.name) {
    log.end();
    return response.failure(res, "name is required");
  }
  if (!req.body.email) {
    log.end();
    return response.failure(res, "email is required");
  }
  if (!req.body.contactNumber) {
    log.end();
    return response.failure(res, "contactNumber is required");
  }
  if (!req.body.address) {
    log.end();
    return response.failure(res, "address is required");
  }
  log.end();
  return next();
};

const getById = (req, res, next) => {
  const log = req.context.logger.start("validators:users:getById");

  if (!req.params && !req.params.id) {
    log.end();
    return response.failure(res, "id is required");
  }
  log.end();
  return next();
};

const update = (req, res, next) => {
  const log = req.context.logger.start("validators:user:update");

  if (!req.body) {
    log.end();
    return response.failure(res, "body is required");
  }
  if (!req.params.id) {
    log.end();
    return response.failure(res, "user id is required");
  }

  log.end();
  return next();
};


exports.getById = getById;
exports.create = create;
exports.update = update;
