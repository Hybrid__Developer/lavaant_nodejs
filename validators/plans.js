"use strict";
const response = require("../exchange/response");

const create = (req, res, next) => {
  const log = req.context.logger.start("validators:plans:create");

  if (!req.body) {
    log.end();
    return response.failure(res, "body is required");
  }
  if (req.body.title === "string" || req.body.title === undefined) {
    log.end();
    return response.failure(res, "title is required");
  }
  if (req.body.description === "string" || req.body.description === undefined) {
    log.end();
    return response.failure(res, "description is required");
  }
  if (req.body.role === "string" || req.body.role === undefined) {
    log.end();
    return response.failure(res, "role is required");
  }
  if (req.body.price === "string" || req.body.price === undefined) {
    log.end();
    return response.failure(res, "price is required");
  }

  log.end();
  return next();
};

exports.create = create;
