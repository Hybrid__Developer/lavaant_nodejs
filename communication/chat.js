let socketio = require('socket.io');
var mongoose = require('mongoose');
require('../models/room.js');
require('../models/chats.js');

var roomModel = mongoose.model('Room');

var usernames = {};
var rooms = [];



// Socket setup
module.exports.sockets = function (http) {

    io = socketio.listen(http);
    //setting chat route
    // var ioChat = io.of('/chat');

    io.on('connection', (socket) => {
        console.log('made socket connection, user is connected', socket.id);


        // create room
        socket.on('createroom', function (data) {
            let id = '5f928bb2d2803b3a4574535a';

            let roo = db.room.findById(id, function (err, data) {
                console.log('data-room', data);
            });
            // console.log('data create room', data)
            newRoom = new roomModel({
                name1: 'raj',
                name2: 'amrit',
            });

            newRoom.save(function (err, newResult) {
                if (!err) {
                    // console.log('room is created');
                } else {
                    // console.log('error in creation room');
                }
            })
            // var new_room = ("" + Math.random()).substring(2, 7);
            // rooms.push(new_room);
            // data.room = new_room;
            // socket.emit('updatechat', 'SERVER', 'Your room is ready, invite someone using this ID:' + new_room);
            // socket.emit('roomcreated', data);
        });

        // add user
        socket.on('adduser', function (data) {
            var username = data.username;
            var room = data.room;

            if (rooms.indexOf(room) != -1) {
                socket.username = username;
                socket.room = room;
                usernames[username] = username;
                socket.join(room);
                socket.emit('updatechat', 'SERVER', 'You are connected. Start chatting');
                socket.broadcast.to(room).emit('updatechat', 'SERVER', username + ' has connected to this room');
            } else {
                socket.emit('updatechat', 'SERVER', 'Please enter valid code.');
            }
        });

        // Handle chat event
        socket.on('message', (msg) => {
            console.log('data ==chat>>>>>>>', msg);
            socket.broadcast.emit('message-broadcast', msg);
            // io.sockets.emit('message', data);
        });
        socket.on('chat-message', (msg) => {
            console.log('data ==chat-message    >>>>>>>', msg);
            socket.broadcast.emit('message-broadcast', msg);
            // io.sockets.emit('message', data);
        });
        // socket.on('typing', function (data) {
        //     socket.broadcast.emit('typing', data)
        // })
    });
}